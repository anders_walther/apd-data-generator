package dk.s4.hl7.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.hl7.v3.POCDMT000040ClinicalDocument;
import org.junit.Assume;
import org.junit.BeforeClass;
import org.junit.Test;

import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;

public class PerfTest {
  private static volatile Object lock = new Object();
  private static volatile JAXBContext jaxbContext;
  private static final int COUNT = 500;

  private static JAXBContext getOrCreateJAXbContext() {
    try {
      if (jaxbContext == null) {
        synchronized (lock) {
          if (jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(POCDMT000040ClinicalDocument.class);
          }
        }
      }
      return jaxbContext;
    } catch (JAXBException e1) {
      throw new RuntimeException(e1.getMessage(), e1);
    }
  }

  @BeforeClass
  public static void before() {
    Assume.assumeNotNull(System.getProperty("perf_test"));
  }

  @Test
  public void phmrCodec() throws Exception {
    PHMRXmlCodec codec = new PHMRXmlCodec();
    String phmr = readFile();
    PHMRDocument document;
    long duration = 0;
    for (int i = 0; i < 10; i++) {
      document = codec.decode(phmr);
      document.hashCode();
    }
    for (int i = 0; i < COUNT; i++) {
      long start = System.nanoTime();
      document = codec.decode(phmr);
      long stop = System.nanoTime();
      document.hashCode();
      duration += stop - start;
      //      System.out.println("PHMR Codec duration: " + ((stop - start) / 100000));
    }
    System.out.println("PHMR duration: " + duration / 1000000);
  }

  @Test
  public void JAXBTest() throws Exception {
    String phmr = readFile();
    long duration = 0;
    getOrCreateJAXbContext().createUnmarshaller();
    for (int i = 0; i < 10; i++) {
      Unmarshaller unmarshaller = getOrCreateJAXbContext().createUnmarshaller();
      POCDMT000040ClinicalDocument obj = ((JAXBElement<POCDMT000040ClinicalDocument>) unmarshaller
          .unmarshal(new StringReader(phmr))).getValue();
      obj.hashCode();
    }
    for (int i = 0; i < COUNT; i++) {
      long start = System.nanoTime();
      Unmarshaller unmarshaller = getOrCreateJAXbContext().createUnmarshaller();
      POCDMT000040ClinicalDocument obj = ((JAXBElement<POCDMT000040ClinicalDocument>) unmarshaller
          .unmarshal(new StringReader(phmr))).getValue();
      long stop = System.nanoTime();
      obj.hashCode();
      duration += stop - start;
      //      System.out.println("JAXB duration: " + ((stop - start) / 100000));
    }
    System.out.println("JAXB duration: " + duration / 1000000);
  }

  @Test
  public void writePhmrCodec() throws Exception {
    PHMRXmlCodec codec = new PHMRXmlCodec(null);
    String phmr = readFile();
    long duration = 0;
    PHMRDocument phmrDocument = codec.decode(phmr);
    StringWriter writer = new StringWriter(58000);
    for (int i = 0; i < COUNT; i++) {
      long start = System.nanoTime();
      codec.serialize(phmrDocument, writer);
      long stop = System.nanoTime();
      writer.getBuffer().setLength(0);
      duration += stop - start;
      //      System.out.println("PHMR write duration: " + ((stop - start) / 100000));
    }
    System.out.println("PHMR write duration: " + duration / 1000000);
  }

  @Test
  public void writeJAXBTest() throws Exception {
    String phmr = readFile();
    long duration = 0;
    getOrCreateJAXbContext().createUnmarshaller();
    Unmarshaller unmarshaller = getOrCreateJAXbContext().createUnmarshaller();
    JAXBElement<POCDMT000040ClinicalDocument> obj = ((JAXBElement<POCDMT000040ClinicalDocument>) unmarshaller
        .unmarshal(new StringReader(phmr)));
    StringWriter writer = new StringWriter(8096);
    for (int i = 0; i < COUNT; i++) {
      long start = System.nanoTime();
      Marshaller marshaller = getOrCreateJAXbContext().createMarshaller();
      marshaller.marshal(obj, writer);
      long stop = System.nanoTime();
      writer.getBuffer().setLength(0);
      duration += stop - start;
      //            System.out.println("JAXB write duration: " + ((stop - start) / 100000));
    }
    System.out.println("JAXB write duration: " + duration / 1000000);
  }

  private String readFile() throws Exception {
    //    File file = new File(this.getClass().getClassLoader().getResource("phmr/PhmrDataFileFor0211223989_0.xml").toURI());
    File file = new File(
        this.getClass().getClassLoader().getResource("phmr/PHMR_KOL_Example_1_MaTIS-MODIFIED.xml").toURI());
    byte[] encoded = new byte[(int) file.length()];
    InputStream is = new FileInputStream(file);
    is.read(encoded, 0, (int) file.length());
    is.close();
    return new String(encoded, "UTF-8");
  }
}
