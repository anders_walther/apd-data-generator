package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.assertFalse;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.APDXmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.encode.apd.SetupMedcomExample;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;

public final class TestXmlToApd extends BaseDecodeTest implements ConcurrencyTestCase {

  private APDXmlCodec codec = new APDXmlCodec();

  @Before
  public void before() {
    setCodec(new APDXmlCodec());
  }

  public void setCodec(APDXmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testApdXMLConverter() {
    try {
      File file = new File(this.getClass().getClassLoader().getResource("apd/DK-APD_Example_1.xml").toURI());
      byte[] encoded = new byte[(int) file.length()];
      InputStream is = new FileInputStream(file);
      is.read(encoded, 0, (int) file.length());
      is.close();
      String XML = new String(encoded, "UTF-8");
      AppointmentDocument a = decode(codec, XML);
      a.getAuthor();
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testPHMRXmlConverterMedcomExample1() throws Exception {
    encodeDecodeAndCompare(codec, SetupMedcomExample.createBaseAppointmentDocument());
  }

  @Override
  public void runTest() throws Exception {
    testPHMRXmlConverterMedcomExample1();
  }

}
