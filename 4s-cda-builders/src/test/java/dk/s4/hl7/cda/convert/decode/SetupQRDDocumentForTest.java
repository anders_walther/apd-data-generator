package dk.s4.hl7.cda.convert.decode;

import java.util.Date;
import java.util.UUID;

import dk.s4.hl7.cda.codes.BasicType;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Participant.ROLE_TYPE;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.SectionInformation;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse.QRDAnalogSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse.QRDDiscreteSliderResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse.QRDMultipleChoiceResponseBuilder;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse.QRDTextResponseBuilder;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

/**
 * Helper methods to create Numeric example
 * 
 * @author Frank Jacobsen, Systematic
 *
 */
public class SetupQRDDocumentForTest {

  public static QRDDocument defineAsCDA() {
    QRDDocument defineQRDDocumentHeaderInformation = defineQRDDocumentHeaderInformation();
    addAdditionalResponses(defineQRDDocumentHeaderInformation);
    addExtraParticipants(defineQRDDocumentHeaderInformation);
    defineQRDDocumentHeaderInformation.getSections().add(0,
        new Section<QRDResponse>(new SectionInformation("vejledning", "vejledning")));
    return defineQRDDocumentHeaderInformation;
  }

  /** Define a CDA for Numeric example */
  public static QRDDocument defineQRDDocumentHeaderInformation() {

    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    // Setup Anders Andersen as authenticator
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data
    // of a CDA.
    QRDDocument qrdDocument = new QRDDocument(MedCom.createId(UUID.randomUUID().toString()));
    qrdDocument.setLanguageCode("da-DK");
    qrdDocument.setTitle("KOL spørgeskema");

    // 1.1 Populate with time and version info
    qrdDocument.setDocumentVersion("2358344", 1);
    qrdDocument.setEffectiveTime(documentCreationTime);

    // 1.2 Populate the document with patient information
    Patient nancy = Setup.defineNancyAsFullPersonIdentity();
    qrdDocument.setPatient(nancy);

    // 1.3 Populate with Author, Custodian, and Authenticator
    // Setup Svendborg sygehus Hjertemedicinsk B as organization
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    qrdDocument.setCustodian(custodianOrganization);

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .build();

    qrdDocument.setAuthor(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(documentCreationTime)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    Date at1000onJan13 = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    qrdDocument.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setTime(at1000onJan13)
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(organization)
        .build());

    // 1.4 Define the service period
    Date from = DateUtil.makeDanishDateTime(2014, 0, 6, 8, 2, 0);
    Date to = DateUtil.makeDanishDateTime(2014, 0, 10, 8, 15, 0);
    qrdDocument.setDocumentationTimeInterval(from, to);

    qrdDocument.setQuestionnaireType(
        new CodedValue("KCQCOP-10", "1.2.208.999.9.9", "Kansas City COPD Questionnaire", "PRO Spørgeskematyper"));
    //null, null)); //
    return qrdDocument;
  }

  private static void addExtraParticipants(QRDDocument cdaInner) {
    PersonIdentity andersAndersen = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();
    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("310541000016007")
        .setName("Odense Universitetshospital - Svendborg Sygehus")
        .setAddress(Setup.defineHjerteMedicinskAfdAddress())
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    for (int i = 0; i < 2; i++) {
      cdaInner.addInformationRecipients(new ParticipantBuilder()
          .setTelecomList(custodianOrganization.getTelecomList())
          .setSOR("31054100001600" + i)
          .setPersonIdentity(andersAndersen)
          .setOrganizationIdentity(custodianOrganization)
          .build());
    }

    cdaInner.setDataEnterer(new ParticipantBuilder()
        .setAddress(custodianOrganization.getAddress())
        .setSOR(custodianOrganization.getIdValue())
        .setTelecomList(custodianOrganization.getTelecomList())
        .setPersonIdentity(andersAndersen)
        .setOrganizationIdentity(custodianOrganization)
        .build());

    for (int i = 0; i < 2; i++) {
      cdaInner.addParticipant(new ParticipantBuilder()
          .setRoleType(ROLE_TYPE.CAREGIVER)
          .setTelecomList(custodianOrganization.getTelecomList())
          .setSOR("31054100001600" + i)
          .setPersonIdentity(andersAndersen)
          .setOrganizationIdentity(custodianOrganization)
          .build());
    }
  }

  private static void addAdditionalResponses(QRDDocument qrdDocument) {
    Section<QRDResponse> section = new Section<QRDResponse>("Indledning", "Testing.");

    QRDResponse qRDResponseBuild = new QRDNumericResponse.QRDNumericResponseBuilder()
        .setInterval("1", "100", IntervalType.IVL_INT)
        .setValue("2014", BasicType.INT)
        .setCodeValue(new CodedValue("q4", "Continua-Q-QID", "Some-Display-Name", "Some-CodeSystem-Name"))
        .setId(new IDBuilder().setRoot("IDroot").setExtension("idExtension").setAuthorityName("AuthorityName").build())
        .setQuestion("ob2")
        .build();

    // External reference
    qRDResponseBuild
        .addReference(new ReferenceBuilder(Reference.DocumentIdReferencesUse.XDS_UNIQUE_ID_REFERENCE.getReferencesUse(),
            MedCom.createId("externalDocumentReference"), Loinc.createQFDDTypeCode()).build());
    // External reference with observation
    qRDResponseBuild
        .addReference(new ReferenceBuilder(Reference.DocumentIdReferencesUse.XDS_UNIQUE_ID_REFERENCE.getReferencesUse(),
            MedCom.createId("externalDocumentReference"), Loinc.createPHMRTypeCode())
                .externalObservation(MedCom.createId("externalObservationReference"))
                .build());

    section.addQuestionnaireEntity(qRDResponseBuild);

    section.addQuestionnaireEntity(new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("code1", "codeSystem1", "displayName1", "codeSystemName1"))
        .setId(MedCom.createId("extension1"))
        .setInterval("0", "120", "2")
        .setQuestion("question1")
        .setValue("10", "INT")
        .build());

    section.addQuestionnaireEntity(new QRDAnalogSliderResponseBuilder()
        .setCodeValue(new CodedValue("code2", "codeSystem2", "displayName2", "codeSystemName2"))
        .setId(MedCom.createId("extension2"))
        .setInterval("100", "200", "4")
        .setQuestion("qeustion2")
        .setValue("20", "INT")
        .build());

    section.addQuestionnaireEntity(new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("code1", "codeSystem1", "displayName1", "codeSystemName1"))
        .setId(MedCom.createId("extension1"))
        .setMinimum(1)
        .setQuestion("question1")
        .setAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .build());

    section.addQuestionnaireEntity(new QRDDiscreteSliderResponseBuilder()
        .setCodeValue(new CodedValue("code2", "codeSystem2", "displayName2", "codeSystemName2"))
        .setId(MedCom.createId("extension2"))
        .setMinimum(1)
        .setQuestion("question2")
        .setAnswer("answerCode2", "answerCodeSystem2", "answerDisplayName2", "answerCodeSystemName2")
        .build());

    qrdDocument.addSection(section);

    section = new Section<QRDResponse>("testingTitle2", "testingText2");

    section.addQuestionnaireEntity(new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code1", "codeSystem1", "displayName1", "codeSystemName1"))
        .setId(MedCom.createId("extension1"))
        .setInterval(1, 2)
        .setQuestion("question1")
        .addAnswer("answerCode1", "answerCodeSystem1", "answerDisplayName1", "answerCodeSystemName1")
        .addAnswer("answerCode2", "answerCodeSystem2", "answerDisplayName2", "answerCodeSystemName2")
        .build());

    section.addQuestionnaireEntity(new QRDMultipleChoiceResponseBuilder()
        .setCodeValue(new CodedValue("code2", "codeSystem2", "displayName2", "codeSystemName2"))
        .setId(MedCom.createId("extension2"))
        .setInterval(3, 5)
        .setQuestion("question2")
        .addAnswer("answerCode3", "answerCodeSystem3", "answerDisplayName3", "answerCodeSystemName3")
        .addAnswer("answerCode4", "answerCodeSystem4", "answerDisplayName4", "answerCodeSystemName4")
        .build());

    section.addQuestionnaireEntity(new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("code1", "codeSystem1", "displayName1", "codeSystemName1"))
        .setId(MedCom.createId("extension1"))
        .setQuestion("question1")
        .setText("answer1")
        .build());

    section.addQuestionnaireEntity(new QRDTextResponseBuilder()
        .setCodeValue(new CodedValue("code2", "codeSystem2", "displayName2", "codeSystemName2"))
        .setId(MedCom.createId("extension2"))
        .setQuestion("question2")
        .setText("answer2")
        .build());

    qrdDocument.addSection(section);
  }
}
