package dk.s4.hl7.cda.convert.decode;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.convert.encode.phmr.SetupMedcomExample1;
import dk.s4.hl7.cda.convert.encode.phmr.SetupMedcomKOLExample1;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;

public final class TestXmlToPhmr extends BaseDecodeTest implements ConcurrencyTestCase {
  private PHMRXmlCodec codec = new PHMRXmlCodec();

  @Before
  public void before() {
    setCodec(new PHMRXmlCodec());
  }

  public void setCodec(PHMRXmlCodec codec) {
    this.codec = codec;
  }

  @Test
  public void testPHMRXmlConverterMedcomExample1() throws Exception {
    encodeDecodeAndCompare(codec, SetupMedcomExample1.defineAsCDA());
  }

  @Test
  public void testPHMRXmlConvertermedcomKOLExample1() {
    encodeDecodeAndCompare(codec, SetupMedcomKOLExample1.defineAsCDA());
  }

  @Test
  public void testPHMRXMLConverterOldNancy() {
    try {
      File file = new File(this.getClass().getClassLoader().getResource("phmr/PHMR_OLD_NANCY.xml").toURI());
      byte[] encoded = new byte[(int) file.length()];
      InputStream is = new FileInputStream(file);
      is.read(encoded, 0, (int) file.length());
      is.close();
      String phmrAsXML = new String(encoded, "UTF-8");
      decode(codec, phmrAsXML);
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Test
  public void testPHMRXMLConverterMeasurementVariations() {
    try {
      File file = new File(this.getClass().getClassLoader().getResource("phmr/PhmrMeasurementVariations.xml").toURI());
      byte[] encoded = new byte[(int) file.length()];
      InputStream is = new FileInputStream(file);
      is.read(encoded, 0, (int) file.length());
      is.close();
      String phmrAsXML = new String(encoded, "UTF-8");
      System.out.println(phmrAsXML);
      PHMRDocument phmr = decode(codec, phmrAsXML);
      assertTrue(phmr.getVitalSigns().size() > 0);
      Iterator<Measurement> iterator = phmr.getVitalSigns().iterator();
      int countBySingleValue, countByLowOpen, countByHighOpen;
      countBySingleValue = countByLowOpen = countByHighOpen = 0;
      while (iterator.hasNext()) {
        Measurement m = iterator.next();
        assertTrue(m.getValue() != null || m.getValueInterval() != null);
        if (m.getValue() != null) {
          countBySingleValue++;
        } else if (m.getValueInterval() != null && m.getValueInterval().isHighValueUnknown()) {
          countByHighOpen++;
        } else if (m.getValueInterval() != null && m.getValueInterval().isLowValueUnknown()) {
          countByLowOpen++;
        } else {
          fail("Unexpected measurement");
        }
      }
      assertEquals(4, countBySingleValue);
      assertEquals(1, countByLowOpen);
      assertEquals(1, countByHighOpen);
    } catch (IOException e) {
      assertFalse("Something went wrong while decoding xml: " + e.getMessage(), true);
    } catch (URISyntaxException e) {
      assertFalse("Couldn't load xml file: " + e.getMessage(), true);
    }
  }

  @Override
  public void runTest() throws Exception {
    testPHMRXmlConvertermedcomKOLExample1();
  }
}
