package dk.s4.hl7.cda.convert.encode.qrd;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.QRDXmlCodec;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.qrd.QRDDocument;

/**
 * Validate the output from our own QRD builder with the example from MedCom.
 *
 * @author Frank Jacobsen, Systematic
 *
 */
public final class TestQRDExamples extends BaseEncodeTest<QRDDocument> implements ConcurrencyTestCase {

  public TestQRDExamples() {
    super("src/test/resources/qrd");
  }

  @Before
  public void before() {
    setCodec(new QRDXmlCodec());
  }

  @Test
  public void shouldMatchExpectedValueEx1() throws Exception {
    performTest("QRD_KOL_Example_1_MaTIS.xml", SetupQrdKolExample1.defineAsCDA());
  }

  @Test
  public void testAnalogSlider() throws Exception {
    performGeneratedTest("generatedAnalogSliderTest.xml", new SetupQRDAnalogSliderExample().createDocument());
  }

  @Test
  public void testDiscreteSlider() throws Exception {
    performGeneratedTest("generatedDiscreteSliderTest.xml", new SetupQRDDiscreteSliderExample().createDocument());
  }

  @Test
  public void testMultipleChoice() throws Exception {
    performGeneratedTest("generatedMultipleChoiceTest.xml", new SetupQRDMultipleChoiceExample().createDocument());
  }

  @Test
  public void testNoResponse() throws Exception {
    performGeneratedTest("generatedNoSections.xml", new SetupQRDNoResponseExample().createDocument());
  }

  @Test
  public void testNumeric() throws Exception {
    performGeneratedTest("generatedNumericTest.xml", new SetupQRDNumericExample().createDocument());
  }

  @Test
  public void testText() throws Exception {
    performGeneratedTest("generatedTextTest.xml", new SetupQRDTextExample().createDocument());
  }

  @Test
  public void testQuestionnaireType() throws Exception {
    performGeneratedTest("generatedWithQuestionnaireType.xml", new SetupQRDQuestionnaireTypeExample().createDocument());
  }

  @Override
  public void runTest() throws Exception {
    shouldMatchExpectedValueEx1();
  }
}
