package dk.s4.hl7.cda.convert.encode.phmr;

import java.util.Calendar;
import java.util.Date;

import dk.s4.hl7.cda.codes.DAK;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.DataInputContext;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.Patient.Gender;
import dk.s4.hl7.cda.model.Patient.PatientBuilder;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.Setup;
import dk.s4.hl7.cda.model.util.DateUtil;

public class SetupMedcomExample4 {

  /** Define a CDA for the Medcom example 4. */
  public static PHMRDocument defineAsCDA() {
    PHMRDocument cda = defineAsCDAWithoutMeasurements();
    return defineMeasurements(cda);
  }

  public static PHMRDocument defineAsCDAWithoutMeasurements() {
    // Define the 'time'
    Date documentCreationTime = DateUtil.makeDanishDateTime(2013, 01, 17, 9, 15, 0);

    // 1. Create a PHMR document as a "Green CDA", that is,
    // a data structure containing only the dynamic data of a CDA.
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("437f8b60-9563-11e3-a5e2-0800200c9a66")
        .setRoot(MedCom.ROOT_OID)
        .build();
    PHMRDocument cda = new PHMRDocument(idHeader);

    cda.setLanguageCode("da-DK");

    // 1.1 Populate with time and version info
    cda.setEffectiveTime(documentCreationTime);

    PersonIdentity authorPerson = new PersonIdentity.PersonBuilder("Madsen")
        .addGivenName("M")
        .setPrefix("Læge")
        .build();

    // 1.2 Populate the document with patient information (from base)
    Patient janus = new PatientBuilder("Berggren")
        .setGender(Gender.Male)
        .setBirthTime(1948, Calendar.JUNE, 26)
        .addGivenName("Janus")
        .setSSN("2606481234")
        .setAddress(Setup.defineNancyAddress())
        .addTelecom(AddressData.Use.HomeAddress, "tel", "65123456")
        .addTelecom(AddressData.Use.WorkPlace, "mailto", "jab@udkantsdanmark.dk")
        .build();

    cda.setTitle("Hjemmemonitorering for " + janus.getIdValue());

    cda.setPatient(janus);

    // 1.3 Populate with Author, Custodian, and Authenticator
    OrganizationIdentity custodian = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("77668685")
        .setName("Odense Universitetshospital, Odense")
        .setAddress(new AddressData.AddressBuilder("5000", "Odense C")
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .addAddressLine("Lungemedicinsk afdeling J")
            .addAddressLine("Sdr. Boulevard 29,")
            .addAddressLine("Indgang 87-88")
            .build())
        .build();

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("77668685")
        .setName("Odense Universitetshospital, Odense")
        .build();

    PersonIdentity authenticator = new PersonIdentity.PersonBuilder("Olsen")
        .addGivenName("Lars")
        .setPrefix("Læge")
        .build();

    cda.setAuthor(new ParticipantBuilder()
        .setAddress(custodian.getAddress())
        .setSOR(custodian.getIdValue())
        .setTime(documentCreationTime)
        .setPersonIdentity(authorPerson)
        .setOrganizationIdentity(organization)
        .build());
    cda.setCustodian(custodian);
    cda.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(custodian.getAddress())
        .setSOR(custodian.getIdValue())
        .setTime(documentCreationTime)
        .setPersonIdentity(authenticator)
        .setOrganizationIdentity(organization)
        .build());

    // 1.4 Define the service period
    Date fromTime = DateUtil.makeDanishDateTime(2013, 1, 11, 9, 11, 0);
    Date toTime = DateUtil.makeDanishDateTime(2013, 1, 16, 11, 14, 0);
    cda.setDocumentationTimeInterval(fromTime, toTime);

    // 1.5 Add measuring equipment
    MedicalEquipment e1 = new MedicalEquipment.MedicalEquipmentBuilder()
        .setMedicalDeviceCode("MCI00005")
        .setMedicalDeviceDisplayName("Lung Monitor")
        .setManufacturerModelName("Manufacturer: Vitalograph / Model: Lung Monitor Bluetooth")
        .setSoftwareName("SerialNr: N/I / SW Rev. N/I")
        .build();

    cda.addMedicalEquipment(e1);
    return cda;
  }

  public static PHMRDocument defineMeasurements(PHMRDocument cda) {
    // 1.6 Add measurements (observations)

    // Example 2: Use the helper methods to easily create measurements
    // for commonly used telemedical measurements, here examplified
    // by weight. Note - no codes, displaynames, nor UCUM units are
    // given.

    ID id = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension("437f8b60-9563-11e3-a5e2-0800200c9a66")
        .setRoot(MedCom.ROOT_OID)
        .build();

    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.Electronically,
        DataInputContext.PerformerType.Citizen);

    cda.addVitalSign(NPU.createSaturation(".92", DateUtil.makeDanishDateTime(2013, 1, 11, 9, 11, 0), context, id));
    cda.addVitalSign(NPU.createSaturation(".91", DateUtil.makeDanishDateTime(2013, 1, 12, 8, 45, 0), context, id));
    cda.addVitalSign(NPU.createSaturation(".94", DateUtil.makeDanishDateTime(2013, 1, 13, 9, 15, 0), context, id));
    cda.addVitalSign(NPU.createSaturation(".95", DateUtil.makeDanishDateTime(2013, 1, 14, 9, 45, 0), context, id));
    cda.addVitalSign(NPU.createSaturation(".90", DateUtil.makeDanishDateTime(2013, 1, 15, 7, 1, 0), context, id));
    cda.addVitalSign(NPU.createSaturation(".94", DateUtil.makeDanishDateTime(2013, 1, 16, 11, 10, 0), context, id));

    // use DAK-E code system
    cda.addResult(DAK.createFVC("2.50", DateUtil.makeDanishDateTime(2013, 1, 11, 9, 14, 0), context, id));
    cda.addResult(DAK.createFVC("2.40", DateUtil.makeDanishDateTime(2013, 1, 12, 8, 50, 0), context, id));
    cda.addResult(DAK.createFVC("2.90", DateUtil.makeDanishDateTime(2013, 1, 13, 9, 21, 0), context, id));
    cda.addResult(DAK.createFVC("3.11", DateUtil.makeDanishDateTime(2013, 1, 14, 9, 50, 0), context, id));
    cda.addResult(DAK.createFVC("2.25", DateUtil.makeDanishDateTime(2013, 1, 15, 7, 10, 0), context, id));
    cda.addResult(DAK.createFVC("3.00", DateUtil.makeDanishDateTime(2013, 1, 16, 11, 14, 0), context, id));

    return cda;
  }
}
