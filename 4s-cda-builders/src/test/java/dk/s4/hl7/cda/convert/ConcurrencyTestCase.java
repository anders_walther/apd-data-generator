package dk.s4.hl7.cda.convert;

public interface ConcurrencyTestCase {
  void runTest() throws Exception;
}
