package dk.s4.hl7.cda.convert.encode.apd;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.convert.APDXmlCodec;
import dk.s4.hl7.cda.convert.ConcurrencyTestCase;
import dk.s4.hl7.cda.convert.encode.BaseEncodeTest;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;

/**
 * Validate the output from our own appointment builder with the example from MedCom.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public final class TestAgainstMedComExamples extends BaseEncodeTest<AppointmentDocument>
    implements ConcurrencyTestCase {
  public TestAgainstMedComExamples() {
    super("src/test/resources/apd/");
  }

  @Before
  public void before() {
    setCodec(new APDXmlCodec());
  }

  @Test
  public void shouldMatchExpectedValueEx1() throws Exception {
    performTest("DK-APD_Example_1.xml", SetupMedcomExample.createBaseAppointmentDocument());
  }

  @Override
  public void runTest() throws Exception {
    shouldMatchExpectedValueEx1();
  }
}