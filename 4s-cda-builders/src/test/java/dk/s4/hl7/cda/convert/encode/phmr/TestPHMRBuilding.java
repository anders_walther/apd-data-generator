package dk.s4.hl7.cda.convert.encode.phmr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.Patient.Gender;
import dk.s4.hl7.cda.model.Patient.PatientBuilder;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PhysicalQuantityInterval;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.cda.model.testutil.HelperMethods;
import dk.s4.hl7.cda.model.util.DateUtil;

/**
 * Test to drive implementation of a GreenCDA like Clinical document conforming
 * to the Danish PHMR standard.
 *
 * Note, this is not a systematic testing effort, but a TDD effort to get the
 * basic abstractions into place. Other tests will validate the exact output
 * with examples from MedCom.
 *
 * @author Henrik Baerbak Christensen, Aarhus University
 * @author Lars Chr. Duus Hausmann, Silverbullet A/S
 */
public final class TestPHMRBuilding {

  private Document phmrAsXML;
  private String asString;
  private PHMRXmlCodec converter = new PHMRXmlCodec();

  @Before
  public void setup() throws ParserConfigurationException, SAXException, IOException, JAXBException {
    // Step 1. Define the Medcom EX1 CDA
    PHMRDocument cda = SetupMedcomExample1.defineAsCDA();

    // Tweak some fields of the contents to ensure proper testing

    // A) Set effective time to 10:11:12 instead
    cda.setEffectiveTime(DateUtil.makeDanishDateTime(2014, 0, 13, 10, 11, 12));

    // B) Setup 'fake' afdeling X to make a difference in the XML that we can
    // spot
    OrganizationIdentity authenticatorOrgIdentity = new OrganizationIdentity.OrganizationBuilder()
        .setSOR("88878685")
        .setName("Odense Universitetshospital - Svendborg Sygehus X")
        .build();

    AddressData organizationAddress = new AddressData.AddressBuilder("5700", "Svendborg")
        .addAddressLine("Hjertemedicinsk afdeling X")
        .addAddressLine("Valdemarsgade 53x")
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();

    // Setup Anders Andersen as authenticator
    PersonIdentity authenticatorIdentity = new PersonIdentity.PersonBuilder("Andersen").addGivenName("Anders").build();

    Date at1000onJan13 = DateUtil.makeDanishDateTime(2014, 0, 13, 10, 0, 0);

    cda.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(organizationAddress)
        .addTelecom(Use.WorkPlace, "tel", "55555555")
        .setSOR("88878685")
        .setTime(at1000onJan13)
        .setPersonIdentity(authenticatorIdentity)
        .setOrganizationIdentity(authenticatorOrgIdentity)
        .build());

    // 2. use the PHMRBuilder to create the XML representation
    asString = createPHMRFromCDA(cda);

    // System.out.println(asString);
  }

  /* Smoke test the header */
  @Test
  public void shouldValidateHeader() {
    assertNotNull("The phmr document is null.", phmrAsXML);

    assertTrue("PHMR has not the DK templateID", asString.contains("<templateId root=\"1.2.208.184.11.1\"/>"));

    String uuid = HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("extension", 0, "id",
        "ClinicalDocument", phmrAsXML);

    try {
      UUID.fromString(uuid);
    } catch (IllegalArgumentException e) {
      Assert.fail("PHMR has not the proper id: " + uuid);
    }
    assertTrue("typeId / root is incorrect", asString.contains("root=\"2.16.840.1.113883.1.3\""));

    assertTrue("effectiveTime is missing or incorrectly formatted",
        asString.contains("<effectiveTime value=\"20140113101112+0100\"/>"));

    assertTrue("confidentialityCode missing or wrong",
        asString.contains("<confidentialityCode code=\"N\" codeSystem=\"2.16.840.1.113883.5.25\"/>"));

    assertTrue("language code missing or wrong", asString.contains("<languageCode code=\"da-DK\"/>"));
  }

  @Test
  public void shouldValidateRecordTarget() {
    assertTrue("recordTarget missing or wrong",
        asString.contains("<recordTarget typeCode=\"RCT\" contextControlCode=\"OP\">"));
    assertTrue("patientRole missing or wrong", asString.contains("<patientRole classCode=\"PAT\">"));

    assertEquals("CPR missing in patientRole", "2512484916", HelperMethods
        .getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("extension", 0, "id", "patientRole", phmrAsXML));

    assertTrue("name tag missing or wrong", asString.contains("<name>"));

    assertTrue("Nancy missing or wrong", asString.contains("<given>Nancy</given>"));
    assertTrue("Ann missing or wrong", asString.contains("<given>Ann</given>"));
    assertTrue("Berggren missing or wrong", asString.contains("<family>Berggren</family>"));

    assertTrue("Gender missing or wrong",
        asString.contains("<administrativeGenderCode code=\"F\" codeSystem=\"2.16.840.1.113883.5.1\"/>"));

    assertTrue("Birthtime missing or wrong", asString.contains("<birthTime value=\"19481225000000+0000\"/>"));

    assertTrue("addr missing or wrong", asString.contains("<addr use=\"H\">"));
    assertTrue("Streetline 1 missing or wrong",
        asString.contains("<streetAddressLine>Skovvejen 12</streetAddressLine>"));
    assertTrue("Streetline 2 missing or wrong", asString.contains("<streetAddressLine>Landet</streetAddressLine>"));

    assertTrue("postalCode missing or wrong", asString.contains("<postalCode>5700</postalCode>"));
    assertTrue("city missing or wrong", asString.contains("<city>Svendborg</city>"));

    assertTrue("telecom 1 missing or wrong", asString.contains("<telecom value=\"tel:65123456\" use=\"H\"/>"));
    assertTrue("telecom 2 missing or wrong",
        asString.contains("<telecom value=\"mailto:nab@udkantsdanmark.dk\" use=\"WP\"/>"));

    // assertTrue("confidentialityCode missing or wrong",
    // asString.contains(""));
    // assertTrue("confidentialityCode missing or wrong",
    // asString.contains(""));
  }

  @Test
  public void shouldValidateAuthor() {
    assertTrue("author missing or wrong", asString.contains("<author typeCode=\"AUT\" contextControlCode=\"OP\">"));
    // Further validation in the 'compare xml' test case.
  }

  @Test
  public void shouldValidateCustodian() {
    assertTrue("custodian missing or wrong", asString.contains("<custodian typeCode=\"CST\">"));
    assertTrue("assignedCustodian missing or wrong", asString.contains("<assignedCustodian classCode=\"ASSIGNED\">"));
    assertTrue("representedCustodian missing or wrong",
        asString.contains("<representedCustodianOrganization classCode=\"ORG\" determinerCode=\"INSTANCE\">"));

    assertTrue("id is missing or wrong",
        asString.contains("<id root=\"1.2.208.176.1.1\" extension=\"88878685\" assigningAuthorityName=\"SOR\"/>"));

    assertTrue("name is missing or wrong",
        asString.contains("<name>Odense Universitetshospital - Svendborg Sygehus</name>"));

    assertTrue("telecom is missing or wrong", asString.contains("<telecom value=\"tel:65112233\" use=\"WP\"/>"));

    assertTrue("custodian addr is missing or wrong",
        asString.contains("<streetAddressLine>Hjertemedicinsk afdeling B</streetAddressLine>"));
  }

  @Test
  public void shouldValidateLegalAuthenticator() {
    assertTrue("legalAuthenticator missing or wrong",
        asString.contains("<legalAuthenticator typeCode=\"LA\" contextControlCode=\"OP\">"));

    assertTrue("authentication time missing or wrong", asString.contains("<time value=\"20140113100000+0100\"/>"));

    assertTrue("signatureCode missing or wrong", asString.contains("<signatureCode nullFlavor=\"NI\"/>"));

    assertTrue("assignedEntity missing or wrong", asString.contains("<assignedEntity classCode=\"ASSIGNED\">"));

    assertTrue("assignedEntity address wrong or missing",
        asString.contains("<streetAddressLine>Hjertemedicinsk afdeling X</streetAddressLine>"));

    assertTrue("assignedEntity telecom wrong or missing",
        asString.contains("<telecom value=\"tel:55555555\" use=\"WP\"/>"));

    assertTrue("assignedEntity assignedPerson wrong or missing",
        asString.contains("<assignedPerson classCode=\"PSN\" determinerCode=\"INSTANCE\">"));

    assertTrue("assignedEntity assignedPersion given name wrong or missing",
        asString.contains("<given>Anders</given>"));
    assertTrue("assignedEntity assignedPersion family name wrong or missing",
        asString.contains("<family>Andersen</family>"));

    assertTrue("legalAuthenticator representedOrganization wrong or missing",
        asString.contains("<representedOrganization classCode=\"ORG\" determinerCode=\"INSTANCE\">"));
    assertTrue("legalAuthenticator representedOrganization name wrong or missing",
        asString.contains("<name>Odense Universitetshospital - Svendborg Sygehus X</name>"));
  }

  @Test
  public void shouldValidateDocumentationOf() {
    assertTrue("documentationOf wrong or missing", asString.contains("<documentationOf typeCode=\"DOC\">"));
    assertTrue("documentationOf serviceEvent wrong or missing",
        asString.contains("<serviceEvent classCode=\"MPROT\" moodCode=\"EVN\">"));
    assertTrue("documentationOf low wrong or missing", asString.contains("<low value=\"20140106080200+0100\"/>"));
    assertTrue("documentationOf high wrong or missing", asString.contains("<high value=\"20140110081500+0100\"/>"));

  }

  @Test
  @Ignore
  public void shouldValidateWeightMeasurements() {
    assertTrue("Measurement section missing",
        asString.contains("<component typeCode=\"COMP\" contextConductionInd=\"true\">"));

    // validate weight no 1 correctly stored
    assertEquals(NPU.DRY_BODY_WEIGHT_CODE, HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("code",
        0, "translation", "code", phmrAsXML));
    assertEquals(NPU.CODESYSTEM_OID, HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("codeSystem",
        0, "code", "observation", phmrAsXML));
    assertEquals("Legeme masse; Pt", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("displayName",
        0, "code", "observation", phmrAsXML));
    assertEquals("NPU terminologien", HelperMethods
        .getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("codeSystemName", 0, "code", "observation", phmrAsXML));
    assertEquals("kg", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("unit", 0, "value",
        "observation", phmrAsXML));
    assertEquals("77.5", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("value", 0, "value",
        "observation", phmrAsXML));

    // validate weight no 2 correctly stored
    assertEquals(NPU.DRY_BODY_WEIGHT_CODE, HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("code",
        1, "code", "observation", phmrAsXML));
    assertEquals(NPU.CODESYSTEM_OID, HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("codeSystem",
        1, "code", "observation", phmrAsXML));
    assertEquals("Legeme masse; Pt", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("displayName",
        1, "code", "observation", phmrAsXML));
    assertEquals("NPU terminologien", HelperMethods
        .getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("codeSystemName", 1, "code", "observation", phmrAsXML));
    assertEquals("kg", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("unit", 1, "value",
        "observation", phmrAsXML));
    assertEquals("77.0", HelperMethods.getValueOfAttrNamedInNodeIndexNamedEnclosedInNodeInDoc("value", 1, "value",
        "observation", phmrAsXML));
  }

  @Test
  public void shouldValidateMedicalEquipment() {
    assertTrue("Medical equipment template 1 missing or wrong",
        asString.contains("<templateId root=\"2.16.840.1.113883.10.20.1.7\"/>"));
    assertTrue("Medical equipment template 2 missing or wrong",
        asString.contains("<templateId root=\"1.2.208.184.11.1\"/>"));

    assertTrue("Medical equipment manufacturer missing or wrong",
        asString.contains("<manufacturerModelName>Manufacturer: AD Company / Model: 6121ABT1</manufacturerModelName>"));
    assertTrue("Medical equipment softwarename missing or wrong",
        asString.contains("<softwareName>SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711</softwareName>"));

    assertTrue("Medical equipment code missing or wrong",
        asString.contains("<code code=\"NI\" codeSystem=\"2.16.840.1.113883.6.24\" codeSystemName=\"MDC Dynamic\">"));

    assertTrue("Medical equipment translation missing or wrong", asString.contains(
        "<translation code=\"MCI00001\" codeSystem=\"1.2.208.184.100.3\" displayName=\"Weight\" codeSystemName=\"MedCom Instrument Codes\"/>"));
  }

  @Test
  public void shouldHandleMissingAdressOrTelecom() throws Exception {
    PHMRDocument cda = SetupMedcomExample1.defineAsCDA();
    Patient patientNoAdr = new PatientBuilder("Berggren")
        .addGivenName("Nancy")
        .setBirthTime(1948, Calendar.DECEMBER, 25)
        .setGender(Gender.Female)
        .setSSN("2512481234")
        .build();

    cda.setPatient(patientNoAdr);

    asString = createPHMRFromCDA(cda);
  }

  @Test
  public void shouldHandleMissingPatientNames() throws Exception {
    PHMRDocument cda = SetupMedcomExample1.defineAsCDA();
    Patient patientNoNames = new PatientBuilder(null /* no family name */)
        .addGivenName(null /* no given name */)
        .setBirthTime(1948, Calendar.DECEMBER, 25)
        .setGender(Gender.Female)
        .setSSN("2512481234")
        .build();

    cda.setPatient(patientNoNames);

    asString = createPHMRFromCDA(cda);
    assertTrue(asString.contains("<given nullFlavor=\"NI"));
    assertTrue(asString.contains("<family nullFlavor=\"NI"));
  }

  @Test
  public void shouldHandleMissingMedicalEquipment() {
    PHMRDocument cda = SetupMedcomExample1.defineAsCDAWitoutMedicalEquipment();
    String encode = converter.encode(cda);
    assertTrue("Medical equipment template 1 missing or wrong",
        encode.contains("<templateId root=\"2.16.840.1.113883.10.20.1.7\"/>"));
    assertTrue("Medical equipment template 2 missing or wrong",
        encode.contains("<templateId root=\"1.2.208.184.11.1\"/>"));
    assertTrue("Medical equipment template, no empty text element",
        encode.contains("<text>No Medical Equipment</text>"));
  }

  @Test
  public void shouldHandleMeasurementWithLowOpenInterval() {
    PHMRDocument cda = SetupMedcomExample4.defineAsCDAWithoutMeasurements();
    cda.addResult(new Measurement.MeasurementBuilder()
        .setPhysicalQuantity(
            new PhysicalQuantityInterval.PhysicalQuantityIntervalBuilder()
                .setUnit("L")
                .setLowValueUnknown()
                .setHighValue("200", true /*inclusive*/)
                .build(),
            "L", new CodedValue("code", "codeSystem", "displayName"))
        .build());
    String encode = converter.encode(cda);
    assertTrue("Low interval not unknown", encode.contains("<low nullFlavor=\"UNK\""));
    assertTrue("High interval unknown", !encode.contains("<high nullFlavor"));
  }

  @Test
  public void shouldHandleMeasurementWithHighOpenInterval() {
    PHMRDocument cda = SetupMedcomExample4.defineAsCDAWithoutMeasurements();
    cda.addResult(new Measurement.MeasurementBuilder()
        .setPhysicalQuantity(
            new PhysicalQuantityInterval.PhysicalQuantityIntervalBuilder()
                .setUnit("L")
                .setLowValue("2", false /*not inclusive*/)
                .setHighValueUnknown()
                .build(),
            "L", new CodedValue("code", "codeSystem", "displayName"))
        .build());
    String encode = converter.encode(cda);
    assertTrue("High interval not unknown", encode.contains("<high nullFlavor=\"UNK\""));
    assertTrue("Low interval unknown", !encode.contains("<low nullFlavor"));
  }

  private String createPHMRFromCDA(PHMRDocument cda)
      throws ParserConfigurationException, SAXException, IOException, JAXBException {
    String xmltext = converter.encode(cda);
    phmrAsXML = HelperMethods.parseXMLStringToDOM(xmltext, true);
    return converter.encode(cda);
  }
}
