package dk.s4.hl7.cda.convert.encode;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;

import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.Difference;
import org.custommonkey.xmlunit.DifferenceListener;
import org.custommonkey.xmlunit.XMLUnit;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.testutil.HelperMethods;

public class BaseEncodeTest<E extends ClinicalDocument> {
  private Codec<E, String> xmlCodec;
  private File parentFolder;

  public BaseEncodeTest(String parentFolderPath) {
    this.parentFolder = new File(parentFolderPath);
  }

  public void setCodec(Codec<E, String> xmlCodec) {
    this.xmlCodec = xmlCodec;
  }

  public String encodeDocument(E cda) throws ParserConfigurationException, SAXException, IOException {
    return xmlCodec.encode(cda);
  }

  protected void performGeneratedTest(String generatedXmlFile, E cda)
      throws ParserConfigurationException, SAXException, IOException, JAXBException {
    String document = encodeDocument(cda);
    writeToTestFile(document, generatedXmlFile);
    compareGeneratedAndFile(document, cda.getId().getExtension(), generatedXmlFile);
  }

  protected void performTest(String xmlFile, E cda)
      throws ParserConfigurationException, SAXException, IOException, JAXBException {
    String document = encodeDocument(cda);
    compareGeneratedAndFile(document, cda.getId().getExtension(), xmlFile);
  }

  /**
   * When using this please follow these guidelines:
   * 1) Run mvn verify and note which tests fail.
   * 2) Use mvn verify -Dgenerate_testfiles. Tests pass
   * 3) Review new test documents where tests failed before. Is the content as
   * expected
   * 4) Validate newly generated documents using CDA Validator:
   * http://hl7-jkiddo.rhcloud.com/app/#/view_direct_input
   * 
   * @param documentContent
   * @param outputFileName
   */
  public void writeToTestFile(String documentContent, String outputFileName) {
    if (System.getProperty("generate_testfiles") != null) {
      try {
        File file = createOutputFile(outputFileName);
        Writer writer = new OutputStreamWriter(new FileOutputStream(file), "UTF-8");
        writer.write(documentContent);
        writer.flush();
        writer.close();
      } catch (IOException e) {
        throw new RuntimeException(e);
      }
    }
  }

  private File createOutputFile(String outputFileName) throws IOException {
    File outputFile = new File(parentFolder, outputFileName);
    if (outputFile.exists()) {
      outputFile.delete();
    }
    outputFile.createNewFile();
    return outputFile;
  }

  public void compareGeneratedAndFile(String cda, String uuid, String filename)
      throws ParserConfigurationException, SAXException, IOException, JAXBException {
    // Load the MedCom example
    // this requires the XML files to have an UTF-8 BOM
    Document expected = HelperMethods.parseXMLStringToDOM(new File(parentFolder, filename), true);

    // Parse XML to dom and add stylesheet header in xml
    Document computed = HelperMethods.parseXMLStringToDOM(cda, true);
    HelperMethods.addStyleSheetHeader(computed);

    assertNotNull(computed);
    // Use XMLDfiff for comparison, ignore differences in whitespace
    XMLUnit.setIgnoreWhitespace(true);
    XMLUnit.setIgnoreComments(true);
    XMLUnit.setIgnoreAttributeOrder(true);
    Diff xmlDiff = new Diff(expected, computed);
    StringBuffer assertMessage = new StringBuffer(), detailedMessage = new StringBuffer();
    xmlDiff.overrideDifferenceListener(new DebugDifferenceListener(assertMessage, detailedMessage));

    boolean similar = xmlDiff.similar();

    // For debugging, you can output all the differences!
    if (!similar) {
      String computedXml = HelperMethods.convertXMLDocumentToString(computed);
      String expectedXml = HelperMethods.convertXMLDocumentToString(expected);

      System.out.println("------------- Detailed Message --------------");
      System.out.print(HelperMethods.indentLines(detailedMessage.toString()));
      System.out.println("------------- Expected XML ------------------");
      System.out.print(HelperMethods.indentLines(expectedXml));
      System.out.println("------------- Computed XML ------------------");
      System.out.print(HelperMethods.indentLines(computedXml));
      System.out.println("------------- End of details ----------------");
    }

    assertTrue(assertMessage.toString(), similar);
  }

  private static class DebugDifferenceListener implements DifferenceListener {
    private StringBuffer assertMessage;
    private StringBuffer detailedMessage;

    DebugDifferenceListener(StringBuffer assertMessage, StringBuffer detailedMessage) {
      this.assertMessage = assertMessage;
      this.detailedMessage = detailedMessage;
    }

    @Override
    public int differenceFound(Difference diff) {
      detailedMessage.append(diff.toString()).append("\n");
      assertMessage.append(diff.getDescription()).append("\n");
      return RETURN_ACCEPT_DIFFERENCE;
    }

    @Override
    public void skippedComparison(Node node1, Node node2) {
    }
  }
}
