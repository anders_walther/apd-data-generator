package dk.s4.hl7.cda.model.phmr;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Calendar;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.codes.UCUM;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.DataInputContext;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.Patient.Gender;
import dk.s4.hl7.cda.model.Patient.PatientBuilder;
import dk.s4.hl7.cda.model.phmr.Measurement.Status;
import dk.s4.hl7.cda.model.util.DateUtil;

/**
 * Test driving the use of EffectiveJava's builder pattern for constructing core
 * model objects.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 *
 */
public class TestModelBuilders {

  private Patient person;
  private AddressData addr;
  private OrganizationIdentity org;

  private Date time1 = DateUtil.makeDanishDateTime(2014, 0, 6, 8, 2, 0);

  @Before
  public void setUp() throws Exception {
    addr = new AddressData.AddressBuilder("8200", "Aarhus N")
        .addAddressLine("Aabogade 34")
        .addAddressLine("Bygn: Hopper 218")
        .setCountry("Denmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();
  }

  @Test
  public void shouldBuildProperPersonObject() {
    person = new PatientBuilder("Andersen")
        .addGivenName("Jonas")
        .addGivenName("Christoffer")
        .setSSN("0101031753")
        .setGender(Gender.Male)
        .setBirthTime(1994, Calendar.OCTOBER, 17)
        .setAddress(addr)
        .addTelecom(AddressData.Use.HomeAddress, "tel", "65123456")
        .addTelecom(AddressData.Use.WorkPlace, "mailto", "nab@udkantsdanmark.dk")
        .setPrefix("Hjertelæge")
        .build();

    String asString = person.toString();

    assertTrue("Missing family name", asString.contains("Andersen"));
    assertTrue("Missing first name 1", asString.contains("Jonas"));
    assertTrue("Missing first name 2", asString.contains("Christoffer"));
    assertTrue("Missing Gender", asString.contains("G: Male"));
    assertTrue("Missing birth time", asString.contains("Birth: Mon Oct 17"));
    assertTrue("Missing birth time", asString.contains("1994"));
    assertTrue("Missing ID", asString.contains("ID: 0101031753"));

    assertTrue("Missing prefix", asString.contains("Hjertelæge"));

    assertTrue("Missing telecom 1", asString.contains("HomeAddress/tel:65123456"));
    assertTrue("Missing telecom 2", asString.contains("WorkPlace/mailto:nab@udkantsdanmark.dk"));

    assertTrue("Missing postal code", asString.contains("8200"));
  }

  @Test
  public void shouldBuildProperAddressObject() {

    String asString = addr.toString();
    // System.out.println(asString);

    assertTrue("Missing postalcode", asString.contains("8200"));
    assertTrue("Missing city", asString.contains("Aarhus N"));

    assertTrue("Missing adr line 1", asString.contains("Aabogade 34"));
    assertTrue("Missing adr line 2", asString.contains("Hopper 218"));

    assertTrue("Missing country", asString.contains("Denmark"));

    assertTrue("Missing address use", asString.contains("(Use: WorkPlace)"));
  }

  @Test
  public void shouldBuildProperOrganziationIdentity() {
    org = // new OrganizationIdentity("1789",
        // "Sct Brutus anstalt for kriminelle mindreårige", null, addr);
        new OrganizationIdentity.OrganizationBuilder()
            .setSOR("1789")
            .setName("Sct Brutus anstalt for kriminelle mindreårige")
            .setAddress(addr)
            .addTelecom(Use.WorkPlace, "tel", "86868686")
            .build();

    String asString = org.toString();
    // System.out.println(asString);

    assertTrue("Missing Org OID", asString.contains("1789"));
    assertTrue("Missing Org Name", asString.contains("Sct Brutus anstalt for kriminelle mindreårige"));
    assertTrue("Missing telecom", asString.contains("tel:86868686"));

    // just smoke test that address is present
    assertTrue("Missing postalcode", asString.contains("8200"));
  }

  @Test
  public void shouldBuildBasicMeasurement() {
    Measurement m = new Measurement.MeasurementBuilder(time1, Measurement.Status.COMPLETED)
        .setPhysicalQuantity("70.6", UCUM.kg, NPU.DRY_BODY_WEIGHT_CODE, NPU.DRY_BODY_WEIGHT_DISPLAYNAME)
        .build();

    assertEquals(NPU.CODESYSTEM_OID, m.getCodeSystem());
    assertEquals(NPU.DISPLAYNAME, m.getCodeSystemName());

    assertEquals(Status.COMPLETED, m.getStatus());
    assertEquals(time1, m.getTimestamp());
    assertEquals("70.6", m.getValue());
    assertEquals(UCUM.kg, m.getUnit());
    assertEquals(NPU.DRY_BODY_WEIGHT_CODE, m.getCode());
    assertEquals(NPU.DRY_BODY_WEIGHT_DISPLAYNAME, m.getDisplayName());

    assertEquals("Measurement: 70.6 kg (Legeme masse; Pt)", m.toString());
  }

  @Test
  public void shouldBuildContextMeasurement() {
    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.Electronically,
        DataInputContext.PerformerType.Citizen);

    Measurement m = new Measurement.MeasurementBuilder(time1, Measurement.Status.COMPLETED)
        .setPhysicalQuantity("70.6", UCUM.kg, NPU.DRY_BODY_WEIGHT_CODE, NPU.DRY_BODY_WEIGHT_DISPLAYNAME)
        .setContext(context)
        .build();

    assertEquals("70.6", m.getValue());

    assertEquals(DataInputContext.PerformerType.Citizen, m.getDataInputContext().getMeasurementPerformerCode());
    assertEquals(DataInputContext.ProvisionMethod.Electronically,
        m.getDataInputContext().getMeasurementProvisionMethodCode());

    assertEquals(Measurement.Type.PHYSICAL_QUANTITY, m.getType());

    assertEquals("Measurement: 70.6 kg (Legeme masse; Pt) [Citizen,Electronically]", m.toString());
  }

  @Test
  public void shouldBuildAlternativeCodingSystemMeasurement() {
    final String LOINC_OID = "2.16.840.1.113883.6.1";
    Measurement m = new Measurement.MeasurementBuilder(time1, Measurement.Status.NULLIFIED)
        .useAlternativeCodingSystem(LOINC_OID, "LOINC")
        .setPhysicalQuantity("3.4", UCUM.L, "19868-9", "FVC")
        .build();

    assertEquals(LOINC_OID, m.getCodeSystem());
    assertEquals("LOINC", m.getCodeSystemName());

    assertEquals(Status.NULLIFIED, m.getStatus());
    assertEquals(time1, m.getTimestamp());
    assertEquals("3.4", m.getValue());
    assertEquals(UCUM.L, m.getUnit());
  }

  @Test
  public void shouldBuildURLtoMediaMeasurement() {
    Measurement ctg = new Measurement.MeasurementBuilder(time1, Measurement.Status.COMPLETED)
        .setObservationMediaReference("484ff720-8f2c-11e3-baa8-0800200c9a66",
            "ctg_obs/484ff720-8f2c-11e3-baa8-0800200c9a66.png")
        .build();

    assertEquals("484ff720-8f2c-11e3-baa8-0800200c9a66", ctg.getReferenceId());
    assertEquals("ctg_obs/484ff720-8f2c-11e3-baa8-0800200c9a66.png", ctg.getReference());

    assertEquals(Measurement.Type.OBSERVATION_MEDIA, ctg.getType());

    assertEquals("Measurement: ctg_obs/484ff720-8f2c-11e3-baa8-0800200c9a66.png ", ctg.toString());
  }

  @Test
  public void shouldBuildEquipment() {
    MedicalEquipment equipment = new MedicalEquipment.MedicalEquipmentBuilder()
        .setMedicalDeviceCode("EPQ12225")
        .setMedicalDeviceDisplayName("Weight")
        .setManufacturerModelName("Manufacturer: AD Company / Model: 6121ABT1")
        .setSoftwareName("SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711")
        .build();

    assertEquals("EPQ12225", equipment.getMedicalDeviceCode());
    assertEquals("Weight", equipment.getMedicalDeviceDisplayName());
    assertEquals("Manufacturer: AD Company / Model: 6121ABT1", equipment.getManufacturerModelName());
    assertEquals("SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711", equipment.getSoftwareName());

    assertEquals(
        "Medical Equipment: EPQ12225, Weight, Manufacturer: AD Company / Model: 6121ABT1, SerialNr: 6121ABT1-987 Rev. 3 / SW Rev. 20144711",
        equipment.toString());
  }

  @Test(expected = RuntimeException.class)
  public void shouldThrowExceptionInCaseMixedMeasurement() {
    @SuppressWarnings("unused")
    // You cannot have both an observation media AND a physical quantity in a
    // single measurement
    Measurement wrong = new Measurement.MeasurementBuilder(time1, Measurement.Status.COMPLETED)
        .setObservationMediaReference("484ff720-8f2c-11e3-baa8-0800200c9a66",
            "ctg_obs/484ff720-8f2c-11e3-baa8-0800200c9a66.png")
        .setPhysicalQuantity("70.6", UCUM.kg, NPU.DRY_BODY_WEIGHT_CODE, NPU.DRY_BODY_WEIGHT_DISPLAYNAME)
        .build();
  }

  @Test(expected = RuntimeException.class)
  public void shouldThrowExceptionInCaseOfNoContents() {
    @SuppressWarnings("unused")
    // You MUST have either physical quantity OR an observation media
    Measurement wrong = new Measurement.MeasurementBuilder(time1, Measurement.Status.COMPLETED).build();
  }

  @Test
  public void shouldBuildMedicalEquipment() {
    MedicalEquipment me = new MedicalEquipment.MedicalEquipmentBuilder()
        .setMedicalDeviceCode("ADeviceCode")
        .setMedicalDeviceDisplayName("MedLab Super 2000")
        .setManufacturerModelName("ModelX")
        .setSoftwareName("Rev 2.1")
        .build();

    assertEquals("ADeviceCode", me.getMedicalDeviceCode());
    assertEquals("MedLab Super 2000", me.getMedicalDeviceDisplayName());
    assertEquals("ModelX", me.getManufacturerModelName());
    assertEquals("Rev 2.1", me.getSoftwareName());
  }

  @Test
  public void shouldValidateDuringBuildMedicalEquipment() {
    @SuppressWarnings("unused")
    MedicalEquipment me = null;
    try {
      me = new MedicalEquipment.MedicalEquipmentBuilder()
          .setMedicalDeviceDisplayName("MedLab Super 2000")
          .setMedicalDeviceCode("ADeviceCode")
          .setSoftwareName("Rev 2.1")
          .build();
      fail("A missing model name was not caught during validation.");
    } catch (RuntimeException e) {
    }

    try {
      me = new MedicalEquipment.MedicalEquipmentBuilder()
          .setMedicalDeviceCode("ADeviceCode")
          .setMedicalDeviceDisplayName("MedLab Super 2000")
          .setManufacturerModelName("ModelX")
          .build();
      fail("A missing software name not caught during validation.");
    } catch (RuntimeException e) {
    }
  }
}
