package dk.s4.hl7.util.xml;

import java.util.Stack;

import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import dk.s4.hl7.util.xml.hashing.HashableCharSequence;
import dk.s4.hl7.util.xml.hashing.HashableStringBuilder;

/**
 * Wraps an XMLStreamReader and keeps track of the current XPath while stepping
 * through the xml.
 * 
 * Note that the ideal way of implementing this class would be to implement the
 * XMLStreamReader interface and direct calls to xmlStreamReader instance
 * (Decorator pattern). But this is a tedious task :)
 */
public class XmlStreamWrapper {
  private static final int EXPECTED_MAX_XPATH_STRING_LENGTH = 512;

  private XMLStreamReader xmlStreamReader;
  private HashableStringBuilder currentXPath;
  private Stack<Integer> startIndexStack;
  private int[] currentStartEndIndexPair;
  private int previousEventType;
  private String previousToPop;
  private boolean skipChildrenOnNext;

  public XmlStreamWrapper(XMLStreamReader xmlStreamReader) {
    this.xmlStreamReader = xmlStreamReader;
    this.currentXPath = new HashableStringBuilder(EXPECTED_MAX_XPATH_STRING_LENGTH);
    this.previousToPop = "";
    this.skipChildrenOnNext = false;
    this.currentStartEndIndexPair = new int[2];
    this.startIndexStack = new Stack<Integer>();
    startIndexStack.ensureCapacity(50);
  }

  public int next() throws XMLStreamException {
    if (skipChildrenOnNext) {
      skipChildrenOnNext = false;
      return skipElementChildrenNext();
    }
    return normalNext();
  }

  private int normalNext() throws XMLStreamException {
    switch (xmlStreamReader.next()) {
    case XMLStreamConstants.START_ELEMENT:
      startIndexStack.push(xmlStreamReader.getLocation().getCharacterOffset());
      elementPop(previousToPop);
      previousToPop = "";
      elementPush(xmlStreamReader.getLocalName());
      break;
    case XMLStreamConstants.END_ELEMENT:
      currentStartEndIndexPair[0] = startIndexStack.pop();
      currentStartEndIndexPair[1] = xmlStreamReader.getLocation().getCharacterOffset();
      elementPop(previousToPop);
      previousToPop = xmlStreamReader.getLocalName();
      break;
    default:
      break;
    }

    previousEventType = xmlStreamReader.getEventType();
    return xmlStreamReader.getEventType();
  }

  public int[] getCurrentStartEndIndexPair() {
    return currentStartEndIndexPair;
  }

  public void skipChildrenOnNext() {
    if (xmlStreamReader.getEventType() == XMLStreamConstants.START_ELEMENT) {
      skipChildrenOnNext = true;
    }
  }

  private int skipElementChildrenNext() throws XMLStreamException {
    if (xmlStreamReader.getEventType() == XMLStreamConstants.START_ELEMENT) {
      skipChildrenOnNext = false;
      String targetEndElement = currentXPath.toString();
      boolean skippingChildren = true;
      while (skippingChildren) {
        switch (normalNext()) {
        case XMLStreamConstants.END_ELEMENT:
          if (currentXPath.equalsIgnoreCase(targetEndElement)) {
            skippingChildren = false;
          }
          break;
        default:
          break;
        }
        previousEventType = xmlStreamReader.getEventType();

      }
    }
    return xmlStreamReader.getEventType();
  }

  public boolean isCurrentStartElement() {
    return xmlStreamReader.getEventType() == XMLStreamConstants.START_ELEMENT;
  }

  public boolean isCurrentEndElement() {
    return xmlStreamReader.getEventType() == XMLStreamConstants.END_ELEMENT;
  }

  /**
   * Will return the current complete xpath to the current element.
   * 
   * Note that the value is lowercase - Use equalsIgnoreCase on return value
   */
  public HashableCharSequence getCurrentXPath() {
    // Update if cursor has been moved - Will do on reader.getElementText
    if (previousEventType != xmlStreamReader.getEventType() && isCurrentEndElement()) {
      elementPop(xmlStreamReader.getLocalName());
      previousEventType = xmlStreamReader.getEventType();
    }
    return currentXPath;
  }

  public boolean hasNext() throws XMLStreamException {
    return xmlStreamReader.hasNext();
  }

  /**
   * Reference to internal XMLStreamReader.
   * 
   * Use next on XmlStreamWrapper instead of the XMLStreamReader returnered here
   * when stepping through the xml. Otherwise the currentXPath won't get updated
   * 
   * 
   * 
   * @return
   */
  public XMLStreamReader getXMLStreamReader() {
    return xmlStreamReader;
  }

  private void elementPop(String elementName) {
    if (elementName != null && !elementName.isEmpty()) {
      // -1 is '/' sign
      currentXPath.setLength(currentXPath.length() - elementName.length() - 1);
    }
  }

  private void elementPush(String elementName) {
    currentXPath.append('/').append(elementName.toLowerCase());
  }

  public boolean isEmpty() {
    return currentXPath.length() == 0;
  }

  public void close() {
    if (xmlStreamReader != null) {
      try {
        xmlStreamReader.close();
      } catch (XMLStreamException e) {
        // Ignore
      }
    }
  }

  public String getLocalName() {
    return xmlStreamReader.getLocalName();
  }

  public String getAttributeValue(String namespaceURI, String attributeName) {
    return xmlStreamReader.getAttributeValue(namespaceURI, attributeName);
  }

  public String getElementText() throws XMLStreamException {
    return xmlStreamReader.getElementText();
  }
}
