package dk.s4.hl7.util.xml;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;

/**
 * This class does the equivalent to the following XPath example: /root/childA/@attributeA = "value" If the expression
 * matches the list of XmlHandlers will be added to XmlMapping. The XmlHandlers must have XPath expressions that are
 * children to the attribute matching expression eg.: /root/childA/childB
 */
public class ElementAttributeFinder implements XmlHandler {
  private List<AttributeMatcher> attributeMatchers;
  private String targetElementName;
  private String completeXPath;
  private AttributeMatcher currentMatcher;

  public ElementAttributeFinder(String completeXPath, String targetElementName) {
    this.targetElementName = targetElementName;
    this.completeXPath = completeXPath;
    this.attributeMatchers = new ArrayList<AttributeMatcher>();
    this.currentMatcher = null;
  }

  public void addAttributeMatchers(String attributeName, String attributeValue, XmlHandler... xmlHandlers) {
    attributeMatchers.add(new AttributeMatcher(attributeName, attributeValue, xmlHandlers));
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), targetElementName)) {
      changeCurrentMatcher(null, xmlMapping);
      for (AttributeMatcher attributeMatcher : attributeMatchers) {
        if (attributeMatcher.matchAttribute(xmlElement)) {
          changeCurrentMatcher(attributeMatcher, xmlMapping);
          attributeMatcher.addHandlers(xmlMapping);
        }
      }
    }
  }

  private void changeCurrentMatcher(AttributeMatcher attributeMatcher, XmlMapping xmlMapping) {
    if (currentMatcher != null) {
      currentMatcher.removeHandlers(xmlMapping);
    }
    currentMatcher = attributeMatcher;
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Not used
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(completeXPath, this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove(completeXPath);
  }
}
