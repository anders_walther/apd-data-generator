/**
 * The MIT License
 *
 * Original work sponsored and donated by National Board of e-Health (NSI), Denmark
 * (http://www.nsi.dk)
 *
 * Copyright (C) 2016 National Board of e-Health (NSI), Denmark (http://www.nsi.dk)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is furnished to do
 * so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package dk.s4.hl7.util.xml;

import java.io.Reader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;

public class XmlElementFinder {
  private volatile static XMLInputFactory xmlInputFactory;
  private List<XmlHandler> currentHandlers;

  public XmlElementFinder() {
    createOrReuseXMLInputFactory();
    currentHandlers = new ArrayList<XmlHandler>();
  }

  private void createOrReuseXMLInputFactory() throws FactoryConfigurationError {
    if (xmlInputFactory == null) {
      synchronized (this) {
        if (xmlInputFactory == null) {
          xmlInputFactory = XMLInputFactory.newFactory();
          xmlInputFactory.setProperty("javax.xml.stream.isCoalescing", true);
        }
      }
    }
  }

  public void findElements(String xml, XmlMapping xmlMapping) throws XMLStreamException {
    findElements(new StringReader(xml), xmlMapping);
  }

  public void findElements(Reader reader, XmlMapping xmlMapping) throws XMLStreamException {
    XmlRawTextReader rawTextReader = new XmlRawTextReader(reader);
    XmlStreamWrapper xmlStreamWrapper = new XmlStreamWrapper(xmlInputFactory.createXMLStreamReader(rawTextReader));
    XMLElement xmlElement = new XMLElement(xmlStreamWrapper, rawTextReader);
    currentHandlers = new ArrayList<XmlHandler>();
    try {
      while (xmlStreamWrapper.hasNext()) {
        xmlStreamWrapper.next();
        if (xmlStreamWrapper.isCurrentStartElement()) {
          xmlElement.setXPath(xmlStreamWrapper.getCurrentXPath());
          XmlHandler handler = xmlMapping.get(xmlStreamWrapper.getCurrentXPath());
          if (handler != null) {
            if (handler.includeChildren()) {
              currentHandlers.add(handler);
            } else {
              handler.handleElementStart(xmlMapping, xmlElement);
            }
          }
          for (int i = 0; i < currentHandlers.size(); i++) {
            currentHandlers.get(i).handleElementStart(xmlMapping, xmlElement);
          }
        }
        // no else-if as handlers may have moved the cursor to 'end element'
        // in the same iteration by calling XMLStreamReader.getElementText
        if (xmlStreamWrapper.isCurrentEndElement()) {
          xmlElement.setXPath(xmlStreamWrapper.getCurrentXPath());
          XmlHandler handler = xmlMapping.get(xmlElement.getXPath());
          if (handler != null) {
            handler.handleElementEnd(xmlMapping, xmlElement);
            currentHandlers.remove(handler);
          }
          for (XmlHandler xmlHandler : currentHandlers) {
            xmlHandler.handleElementEnd(xmlMapping, xmlElement);
          }
        }
      }
    } finally {
      xmlStreamWrapper.close();
    }
  }
}
