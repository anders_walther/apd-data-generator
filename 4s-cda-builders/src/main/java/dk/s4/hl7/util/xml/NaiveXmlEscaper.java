package dk.s4.hl7.util.xml;

import java.io.IOException;

public class NaiveXmlEscaper {
  /**
   * Performs naive escaping of xml.
   */
  public static void escapeAndInsert(Appendable output, final String text) throws IOException {
    int startIndex = 0;
    int endIndex = 0;
    for (int i = 0; i < text.length(); i++) {
      char c = text.charAt(i);
      switch (c) {
      case '<':
        output.append(text, startIndex, endIndex);
        output.append("&lt;");
        endIndex = startIndex = i + 1;
        break;
      case '>':
        output.append(text, startIndex, endIndex);
        output.append("&gt;");
        endIndex = startIndex = i + 1;
        break;
      case '\"':
        output.append(text, startIndex, endIndex);
        output.append("&quot;");
        endIndex = startIndex = i + 1;
        break;
      case '&':
        output.append(text, startIndex, endIndex);
        output.append("&amp;");
        endIndex = startIndex = i + 1;
        break;
      case '\'':
        output.append(text, startIndex, endIndex);
        output.append("&apos;");
        endIndex = startIndex = i + 1;
        break;
      default:
        endIndex++;
      }
    }
    if (startIndex == 0 && endIndex == text.length()) {
      output.append(text);
    } else {
      output.append(text, startIndex, endIndex);
    }
  }
}
