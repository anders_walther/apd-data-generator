package dk.s4.hl7.util.xml;

import javax.xml.stream.XMLStreamException;

import dk.s4.hl7.util.xml.hashing.HashableCharSequence;

public class XMLElement {
  private XmlStreamWrapper xmlStreamWrapper;
  private HashableCharSequence xpath;
  private String elementValue;
  private XmlRawTextReader rawTextReader;

  public XMLElement(XmlStreamWrapper xmlStreamWrapper, XmlRawTextReader rawTextReader) {
    this.xmlStreamWrapper = xmlStreamWrapper;
    this.rawTextReader = rawTextReader;
  }

  public XMLElement(XmlStreamWrapper xmlStreamWrapper) {
    this(xmlStreamWrapper, null);
  }

  public void setXPath(HashableCharSequence xpath) {
    this.xpath = xpath;
    elementValue = null;
  }

  public HashableCharSequence getXPath() {
    return xpath;
  }

  public String getElementName() {
    return xmlStreamWrapper.getLocalName();
  }

  public String getAttributeValue(String attributeName) {
    if (xmlStreamWrapper.isCurrentStartElement()) {
      return xmlStreamWrapper.getAttributeValue(null, attributeName);
    }
    return null;
  }

  public void startCaptureRawText() {
    if (rawTextReader != null) {
      rawTextReader.continuesCapture();
      xmlStreamWrapper.skipChildrenOnNext();
    }
  }

  /**
   * NB: Only use this after all attributes have been read - Calling this will
   * move the XML parser forward
   * 
   */
  public String getElementValue() {
    if (elementValue == null) {
      if (xmlStreamWrapper.isCurrentEndElement()) {
        int[] startEndIndices = xmlStreamWrapper.getCurrentStartEndIndexPair();
        elementValue = rawTextReader.readRawText(startEndIndices[0], startEndIndices[1]);
      } else {
        try {
          elementValue = xmlStreamWrapper.getElementText();
        } catch (XMLStreamException e) {
          // Element has children and does not have a value/text
          return null;
        }
      }
    }
    return elementValue;
  }
}
