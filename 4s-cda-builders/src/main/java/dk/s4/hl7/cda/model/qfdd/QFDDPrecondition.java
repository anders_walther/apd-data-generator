package dk.s4.hl7.cda.model.qfdd;

import dk.s4.hl7.cda.convert.decode.CdaBuilderException;
import dk.s4.hl7.cda.model.CodedValue;

/**
 * General precondition for a QFDD question. There are two usages:
 * 
 * 1) The precondition is valid within a range of values set by minimum and
 * maximum. (Numeric question)
 * 2) the precondition is valid for an answer option (multiple choice and derivatives)
 */
public class QFDDPrecondition {
  private QFDDCriterion criterion;
  private QFDDPreconditionGroup preconditionGroup;
  private CodedValue conjunctionCode;
  private boolean negationInd;

  private QFDDPrecondition(QFDDPreconditionBuilder builder) {
    this.criterion = builder.criterion;
    this.preconditionGroup = builder.preconditionGroup;
    this.conjunctionCode = builder.conjunctionCode;
    this.negationInd = builder.negationInd;
  }

  public boolean isSimple() {
    return criterion != null && preconditionGroup == null && conjunctionCode == null && !negationInd;
  }

  public QFDDCriterion getCriterion() {
    return criterion;
  }

  public QFDDPreconditionGroup getPreconditionGroup() {
    return preconditionGroup;
  }

  public CodedValue getConjunctionCode() {
    return conjunctionCode;
  }

  public boolean isNegationInd() {
    return negationInd;
  }

  public static class QFDDPreconditionBuilder {
    private QFDDCriterion criterion;
    private QFDDPreconditionGroup preconditionGroup;
    private CodedValue conjunctionCode;
    private boolean negationInd;

    public QFDDPreconditionBuilder(QFDDCriterion criterion) {
      this.criterion = criterion;
    }

    public QFDDPreconditionBuilder(QFDDPreconditionGroup preconditionGroup) {
      this.preconditionGroup = preconditionGroup;
    }

    public QFDDPreconditionBuilder setConjunctionCode(CodedValue conjunctionCode) {
      this.conjunctionCode = conjunctionCode;
      return this;
    }

    public QFDDPreconditionBuilder setNegationInd(boolean negationInd) {
      this.negationInd = negationInd;
      return this;
    }

    public QFDDPrecondition build() {
      checkMutualExclusive();
      return new QFDDPrecondition(this);
    }

    private void checkMutualExclusive() {
      if (criterion == null) {
        if (preconditionGroup == null) {
          throw new CdaBuilderException("Precondition must have a criterion or an precondition group");
        }
      } else if (preconditionGroup != null) {
        throw new CdaBuilderException("Precondition cannot have both of a criterion and an precondition group");
      }
    }
  }
}
