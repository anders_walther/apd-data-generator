package dk.s4.hl7.cda.convert.decode;

import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.util.xml.XmlHandler;

/**
 * CDA specific handler interface that ensures that a handler is able to add the
 * data it has collected to a GreenCDA model document
 */
public interface CDAXmlHandler<E extends ClinicalDocument> extends XmlHandler {
  void addDataToDocument(E clinicalDocument);
}
