package dk.s4.hl7.cda.convert.base;

/**
 * The Converter<S, T> interface is used for creating classes that are capable of 
 * converting from a source type (S) to a target type (T).
 * 
 * 
 * @see https://www.dartlang.org/articles/libraries/converters-and-codecs
 * 
 * @author Frank Jacobsen Systematic
 */

public interface Converter<S, T> {
  T convert(S source);
}
