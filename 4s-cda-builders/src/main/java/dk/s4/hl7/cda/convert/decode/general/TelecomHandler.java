package dk.s4.hl7.cda.convert.decode.general;

import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.Telecom;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class TelecomHandler extends BaseXmlHandler {
  private static final String TELECOM_ELEMENT_NAME = "telecom";
  private List<Telecom> telecoms;

  public TelecomHandler(String xpathToParentElement) {
    telecoms = new ArrayList<Telecom>();
    addPath(xpathToParentElement + "/" + TELECOM_ELEMENT_NAME);
  }

  public List<Telecom> getTelecoms() {
    return telecoms;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), TELECOM_ELEMENT_NAME)) {
      telecoms.add(ConvertXmlUtil.createTelecom(xmlElement));
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Ignore
  }

  public void clear() {
    telecoms = new ArrayList<Telecom>();
  }
}
