package dk.s4.hl7.cda.convert;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.codes.DAK;
import dk.s4.hl7.cda.codes.EquipmentTypes;
import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.convert.encode.ClinicalDocumentConverter;
import dk.s4.hl7.cda.convert.encode.pattern.ReferenceRangePattern;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Comment;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.Measurement.Status;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

/**
 * An implementation of the Converter that can build an XML Document following
 * the Danish PHMR standard.
 * 
 * @author Henrik Baerbak Christensen, Aarhus University
 * @author Lars Christian Duus Hausmann, Silverbullet A/S
 * @author Frank Jacobsen Systematic A/S
 * @author Lars Storm Systematic A/S
 * @author Erik Nielsen, Systematic A/S
 */

public class PHMRXmlConverter extends ClinicalDocumentConverter<PHMRDocument> {
  private Logger logger;
  private static final ReferenceRangePattern referenceRangePattern = new ReferenceRangePattern();
  private static final CodedValue NULL_FLAVOURED_SNOMED_CT = new CodedValue(null, NPU.TRANSLATION_SNOMED_CT_OID, null,
      NPU.TRANSLATION_SNOMED_CT_NAME);

  public PHMRXmlConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    super(xmlPrettyPrinter);
    this.logger = LoggerFactory.getLogger(PHMRXmlConverter.class);
  }

  public PHMRXmlConverter() {
    this(new XmlPrettyPrinter());
  }

  private void buildVitalSignsSection(List<Measurement> measurements, String text, XmlStreamBuilder xmlBuilder)
      throws IOException {
    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, HL7.PHMR_VITAL_SIGNS_FIRST_ID, HL7.PHMR_VITAL_SIGNS_SECOND_ID,
        HL7.PHMR_ENTRY_ID);
    BuildUtil.buildCode(Loinc.SECTION_VITAL_SIGNS_CODE, Loinc.OID, xmlBuilder);
    xmlBuilder.element("title").value("Vital Signs").elementEnd();

    if (!measurements.isEmpty()) {
      if (text != null) {
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping(text).elementEnd();
      } else {
        xmlBuilder.element("text").value("Vital Signs").elementEnd();
      }
      for (Measurement measurement : measurements) {
        buildMeasurementObservation(measurement, xmlBuilder);
      }
    } else {
      xmlBuilder.element("text").value("No Vital Signs").elementEnd();
    }

    xmlBuilder.elementEnd(); // end section
    xmlBuilder.elementEnd(); // end component
  }

  private void buildResultsSection(List<Measurement> measurements, String text, XmlStreamBuilder xmlBuilder)
      throws IOException {
    logger.debug("Creating observations from " + measurements.size() + " measurements");
    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, HL7.PHMR_RESULTS_FIRST_ID, HL7.PHMR_RESULTS_SECOND_ID,
        HL7.PHMR_RESULTS_THIRD_ID);
    BuildUtil.buildCode(Loinc.SECTION_RESULTS_CODE, Loinc.OID, xmlBuilder);
    xmlBuilder.element("title").value("Results").elementEnd();

    if (!measurements.isEmpty()) {
      if (text != null) {
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping(text).elementEnd();
      } else {
        xmlBuilder.element("text").value("Results").elementEnd();
      }
      for (Measurement measurement : measurements) {
        buildMeasurementObservation(measurement, xmlBuilder);
      }
    } else {
      xmlBuilder.element("text").value("No Results").elementEnd();
    }

    xmlBuilder.elementEnd(); // end section
    xmlBuilder.elementEnd(); // end component
  }

  /**
   * Create instrument section from list of instruments
   * 
   * @param instruments
   *          The list to create instrument from
   * @param text
   * @return Section of instruments
   */
  private void buildMedicalEquipmentSection(List<MedicalEquipment> instruments, String text,
      XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, HL7.PHMR_MEDICAL_EQUIPMENT_FIRST_ID, HL7.PHMR_MEDICAL_EQUIPMENT_SECOND_ID,
        HL7.PHMR_MEDICAL_EQUIPMENT_THIRD_ID);
    BuildUtil.buildCode(Loinc.SECTION_MEDICAL_CODE, Loinc.OID, xmlBuilder);
    xmlBuilder.element("title").value("Medical Equipment").elementEnd();

    if (!instruments.isEmpty()) {
      if (text != null) {
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping(text).elementEnd();
      } else {
        xmlBuilder.element("text").value("Medical Equipment").elementEnd();
      }
      HashSet<String> medicalHs = new HashSet<String>();
      for (MedicalEquipment medicalEquipment : instruments) {
        if (!medicalHs.contains(medicalEquipment.getMedicalDeviceCode())) {
          medicalHs.add(medicalEquipment.getMedicalDeviceCode());
          createEntryFromMedicalEquipment(medicalEquipment, xmlBuilder);
        }
      }
    } else {
      xmlBuilder.element("text").value("No Medical Equipment").elementEnd();
    }

    xmlBuilder.elementEnd(); // end section
    xmlBuilder.elementEnd(); // end component
  }

  /**
   * Creates PHMR device entry section from Instrument object
   * 
   * @param medicalEquipment
   *          The Instrument to use
   * @param medicalHs
   * @return
   */
  private void createEntryFromMedicalEquipment(MedicalEquipment medicalEquipment, XmlStreamBuilder xmlBuilder)
      throws IOException {
    xmlBuilder.element("entry").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("organizer").attribute("classCode", "CLUSTER").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, HL7.DEVICE_DEFINITION_ORGANIZER);
    buildStatusCode(Status.COMPLETED, xmlBuilder);

    xmlBuilder.element("participant").attribute("typeCode", "SBJ").attribute("contextControlCode", "OP");
    xmlBuilder.element("participantRole").attribute("classCode", "MANU");
    BuildUtil.buildTemplateIds(xmlBuilder, HL7.PRODUCT_INSTANCE_TEMPLATE_OID, HL7.PHMR_PRODUCT_INSTANCE);
    BuildUtil.buildId(EquipmentTypes.getEquipmentTypeOID(medicalEquipment.getSerialNumberType()),
        medicalEquipment.getSerialNumberType(), medicalEquipment.getSerialNumber(), xmlBuilder);
    xmlBuilder.element("playingDevice").attribute("classCode", "DEV").attribute("determinerCode", "INSTANCE");
    if (medicalEquipment.getMedicalDeviceCode() != null) {
      BuildUtil.buildNullFlavorableCodeTranslated(
          new CodedValue( // code
              "NI", NPU.TRANSLATION_MDC_OID, null /* no display name */, NPU.TRANSLATION_MDC_NAME),
          new CodedValue( // translation
              medicalEquipment.getMedicalDeviceCode(), MedCom.MEDICAL_DEVICE_OID,
              medicalEquipment.getMedicalDeviceDisplayName(), MedCom.MEDICAL_DISPLAYNAME),
          xmlBuilder);
    }
    xmlBuilder.element("manufacturerModelName").value(medicalEquipment.getManufacturerModelName()).elementEnd();
    xmlBuilder.element("softwareName").value(medicalEquipment.getSoftwareName()).elementEnd();

    xmlBuilder.elementEnd(); // end playingDevice
    xmlBuilder.elementEnd(); // end participantRole
    xmlBuilder.elementEnd(); // end participant
    xmlBuilder.elementEnd(); // end organizer
    xmlBuilder.elementEnd(); // end entry
  }

  // === Private helper methods

  private void buildMeasurementObservation(Measurement measurement, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("entry").attribute("typeCode", "COMP").attribute("contextConductionInd", "true").addElement(
        xmlBuilder.element("organizer").attribute("classCode", "CLUSTER").attribute("moodCode", "EVN"));

    BuildUtil.buildTemplateIds(xmlBuilder, HL7.OBSERVATION_ORGANIZER);
    buildStatusCode(measurement.getStatus(), xmlBuilder);
    buildEffectiveTime(measurement, xmlBuilder);

    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");

    if (measurement.getType() == Measurement.Type.PHYSICAL_QUANTITY) {
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.OBSERVATION, HL7.PHMR_NUMERIC_OBSERVATION);
      BuildUtil.buildId(measurement.getId(), xmlBuilder);
      buildMeasurementCode(measurement, xmlBuilder);
      buildStatusCode(measurement.getStatus(), xmlBuilder);
      buildValueAsPQ(measurement, xmlBuilder);
      buildDataInputContext(measurement, xmlBuilder);
    } else if (measurement.getType() == Measurement.Type.OBSERVATION_MEDIA) {
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.OBSERVATION);
      xmlBuilder.element("entryRelationship").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
      xmlBuilder.element("observationMedia").attribute("classCode", "OBS").attribute("moodCode", "EVN").attribute("ID",
          "UrlToMedia");
      BuildUtil.buildId(measurement.getId(), xmlBuilder);
      xmlBuilder.element("value");
      xmlBuilder.element("reference").attribute("value", measurement.getReference()).elementShortEnd();
      xmlBuilder.elementEnd(); // end value

      xmlBuilder.elementEnd(); // end observationMedia
      xmlBuilder.elementEnd(); // end entryRelationship
    }

    if (measurement.hasComment()) {
      buildComment(measurement.getComment(), xmlBuilder);
    }
    externalReferencePattern.build(measurement.getReferences(), xmlBuilder);
    referenceRangePattern.build(measurement.getObservationRanges(), xmlBuilder);
    xmlBuilder.elementEnd(); // end observation
    xmlBuilder.elementEnd(); // end component
    xmlBuilder.elementEnd(); // end organizer
    xmlBuilder.elementEnd(); // end entry
  }

  private void buildDataInputContext(Measurement measurement, XmlStreamBuilder xmlBuilder) throws IOException {
    if (measurement.getDataInputContext() == null) {
      BuildUtil.buildNullFlavor("methodCode", xmlBuilder);
      return;
    }
    if (measurement.getDataInputContext().getMeasurementPerformerCode() != null) {
      buildMethodCode(getPerformerMethodCode(measurement), MedCom.MESSAGECODE_OID,
          getPerformerMethodDisplayName(measurement), MedCom.MESSAGECODE_DISPLAYNAME, xmlBuilder);
    }
    if (measurement.getDataInputContext().getMeasurementProvisionMethodCode() != null) {
      buildMethodCode(getProvisionMethodCode(measurement), MedCom.MESSAGECODE_OID,
          getProvisionMethodDisplayName(measurement), MedCom.MESSAGECODE_DISPLAYNAME, xmlBuilder);
    }
    if (measurement.getDataInputContext().getOrganizationIdentity() != null
        && measurement.getDataInputContext().getProvisionUserType() != null) {
      xmlBuilder.element("participant").attribute("typeCode", "ENT").attribute("contextControlCode", "OP");
      xmlBuilder.element("participantRole").attribute("classCode",
          measurement.getDataInputContext().getProvisionUserType());
      buildSORId(measurement.getDataInputContext().getOrganizationIdentity(), xmlBuilder);
      xmlBuilder.elementEnd(); // participant
      xmlBuilder.elementEnd(); // participantRole
    }
  }

  private void buildMeasurementCode(Measurement measurement, XmlStreamBuilder xmlBuilder) throws IOException {
    CodedValue code = new CodedValue(measurement.getCode(), measurement.getCodeSystem(), measurement.getDisplayName(),
        measurement.getCodeSystemName());
    CodedValue translated = (measurement.getTranslatedCode() != null && measurement.getTranslatedCodeSystem() != null)
        ? new CodedValue(measurement.getTranslatedCode(), measurement.getTranslatedCodeSystem(),
            measurement.getTranslatedDisplayName(), measurement.getTranslatedCodeSystemName())
        : null;
    if (measurement.getCode() != null && (NPU.CODESYSTEM_OID.equals(measurement.getCodeSystem())
        || DAK.CODESYSTEM_OID.equals(measurement.getCodeSystem()))) {
      // Use NPU as translation
      BuildUtil.buildNullFlavorableCodeTranslated(NULL_FLAVOURED_SNOMED_CT, code, xmlBuilder);
    } else if (measurement.getCode() == null) {
      BuildUtil.buildNullFlavorableCodeTranslated(NULL_FLAVOURED_SNOMED_CT, translated, xmlBuilder);
    } else {
      BuildUtil.buildNullFlavorableCodeTranslated(code, translated, xmlBuilder);
    }
  }

  private void buildComment(Comment comment, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("entryRelationship").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("act").attribute("classCode", "ACT").attribute("moodCode", "EVN");

    BuildUtil.buildCode(Loinc.COMMENT_CODEDVALUE, xmlBuilder);
    buildText(comment.getText(), xmlBuilder);
    buildAuthorSection(null, comment.getAuthor(), xmlBuilder);

    xmlBuilder.elementEnd(); // end act
    xmlBuilder.elementEnd(); // end entryRelationship
  }

  private void buildText(String text, XmlStreamBuilder xmlBuilder) throws IOException {
    if (text != null && !text.trim().isEmpty()) {
      xmlBuilder.element("text").value(text).elementEnd();
    }
  }

  private static void buildStatusCode(Status status, XmlStreamBuilder xmlBuilder) throws IOException {
    if (status == Status.COMPLETED) {
      xmlBuilder.element("statusCode").attribute("code", "completed").elementShortEnd();
    } else if (status == Status.NULLIFIED) {
      xmlBuilder.element("statusCode").attribute("code", "nullified").elementShortEnd();
    }
  }

  private static String getProvisionMethodCode(Measurement measurement) {
    switch (measurement.getDataInputContext().getMeasurementProvisionMethodCode()) {
    case Electronically:
      return MedCom.TRANSFERRED_ELECTRONICALLY;
    case TypedByCitizen:
      return MedCom.TYPED_BY_CITIZEN;
    case TypedByCitizenRelative:
      return MedCom.TYPED_BY_CITIZEN_RELATIVE;
    case TypedByHealthcareProfessional:
      return MedCom.TYPED_BY_HEALTHCAREPROFESSIONAL;
    case TypedByCareGiver:
      return MedCom.TYPED_BY_CAREGIVER;
    default:
      throw new IllegalStateException("Invalid Context provision method code"
          + measurement.getDataInputContext().getMeasurementProvisionMethodCode().toString());
    }
  }

  private static String getProvisionMethodDisplayName(Measurement measurement) {
    switch (measurement.getDataInputContext().getMeasurementProvisionMethodCode()) {
    case Electronically:
      return MedCom.TRANSFERRED_ELECTRONICALLY_DISPLAYNAME;
    case TypedByCitizen:
      return MedCom.TYPED_BY_CITIZEN_DISPLAYNAME;
    case TypedByCitizenRelative:
      return MedCom.TYPED_BY_CITIZEN_RELATIVE_DISPLAYNAME;
    case TypedByHealthcareProfessional:
      return MedCom.TYPED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME;
    case TypedByCareGiver:
      return MedCom.TYPED_BY_CAREGIVER_DISPLAYNAME;
    default:
      throw new IllegalStateException("Invalid Context provision method code"
          + measurement.getDataInputContext().getMeasurementProvisionMethodCode().toString());
    }
  }

  private static String getPerformerMethodCode(Measurement measurement) {
    switch (measurement.getDataInputContext().getMeasurementPerformerCode()) {
    case Citizen:
      return MedCom.PERFORMED_BY_CITIZEN;
    case HealthcareProfessional:
      return MedCom.PERFORMED_BY_HEALTHCAREPROFESSIONAL;
    case CareGiver:
      return MedCom.PERFORMED_BY_CAREGIVER;
    default:
      throw new IllegalStateException("Invalid Context performer code"
          + measurement.getDataInputContext().getMeasurementPerformerCode().toString());
    }
  }

  private static String getPerformerMethodDisplayName(Measurement measurement) {
    switch (measurement.getDataInputContext().getMeasurementPerformerCode()) {
    case Citizen:
      return MedCom.PERFORMED_BY_CITIZEN_DISPLAYNAME;
    case HealthcareProfessional:
      return MedCom.PERFORMED_BY_HEALTHCAREPROFESSIONAL_DISPLAYNAME;
    case CareGiver:
      return MedCom.PERFORMED_BY_CAREGIVER_DISPLAYNAME;
    default:
      throw new IllegalStateException("Invalid Context performer code"
          + measurement.getDataInputContext().getMeasurementPerformerCode().toString());
    }
  }

  public String convert(PHMRDocument source) {
    int nofObservations = source.getVitalSigns().size() + source.getResults().size()
        + source.getMedicalEquipments().size();
    StringBuilder builder = new StringBuilder(8000 + (nofObservations * 1500));
    serialize(source, builder);
    return builder.toString();
  }

  @Override
  protected void buildDocumentationOf(PHMRDocument source, XmlStreamBuilder xmlBuilder) throws IOException {
    Collection<CodedValue> codedValues = getUniqueMeasurementCodes((PHMRDocument) source);

    xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
    xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");
    xmlBuilder.element("effectiveTime");

    if (source.getServiceStartTime() != null) {
      xmlBuilder
          .element("low")
          .attribute("value", dateTimeformatter.format(source.getServiceStartTime()))
          .elementShortEnd();
    }
    if (source.getServiceStopTime() != null) {
      xmlBuilder
          .element("high")
          .attribute("value", dateTimeformatter.format(source.getServiceStopTime()))
          .elementShortEnd();
    }
    xmlBuilder.elementEnd(); // end effectiveTime
    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf

    for (CodedValue codedValue : codedValues) {
      xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
      xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");
      BuildUtil.buildCode(codedValue.getCode(), codedValue.getCodeSystem(), codedValue.getDisplayName(), xmlBuilder);
      xmlBuilder.elementEnd(); // end serviceEvent
      xmlBuilder.elementEnd(); // end documentationOf
    }
  }

  private Collection<CodedValue> getUniqueMeasurementCodes(PHMRDocument source) {
    Set<String> codes = new HashSet<String>();
    List<CodedValue> codedValues = new ArrayList<CodedValue>(10);
    for (Measurement measurement : source.getVitalSigns()) {
      addObservationCodeAndTranslation(measurement, codes, codedValues);
    }
    for (Measurement measurement : source.getResults()) {
      addObservationCodeAndTranslation(measurement, codes, codedValues);
    }
    return codedValues;
  }

  private void addObservationCodeAndTranslation(Measurement measurement, Set<String> codes,
      List<CodedValue> codedValues) {
    if (measurement.getCode() != null && !codes.contains(measurement.getCode())) {
      codes.add(measurement.getCode());
      codedValues.add(new CodedValue(measurement.getCode(), measurement.getCodeSystem(), measurement.getDisplayName(),
          measurement.getCodeSystemName()));
    }
    if (measurement.getTranslatedCode() != null && !codes.contains(measurement.getTranslatedCode())) {
      codes.add(measurement.getTranslatedCode());
      codedValues.add(new CodedValue(measurement.getTranslatedCode(), measurement.getTranslatedCodeSystem(),
          measurement.getTranslatedDisplayName(), measurement.getTranslatedCodeSystemName()));
    }
  }

  @Override
  protected void buildDataEnterer(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // Not used
  }

  @Override
  protected void buildInformationRecipient(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // Not used
  }

  @Override
  protected void buildParticipants(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // Not used
  }

  @Override
  protected void buildBody(PHMRDocument phmrDocument, XmlStreamBuilder xmlBuilder) throws IOException {
    this.buildStructuredBodySectionStart(xmlBuilder);
    this.buildVitalSignsSection(phmrDocument.getVitalSigns(), phmrDocument.getVitalSignsText(), xmlBuilder);
    this.buildResultsSection(phmrDocument.getResults(), phmrDocument.getResultsText(), xmlBuilder);
    this.buildMedicalEquipmentSection(phmrDocument.getMedicalEquipments(), phmrDocument.getMedicalEquipmentsText(),
        xmlBuilder);
    this.buildStructuredBodySectionEnd(xmlBuilder);
  }
}