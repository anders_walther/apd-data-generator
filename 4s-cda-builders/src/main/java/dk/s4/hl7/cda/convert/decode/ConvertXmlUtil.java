package dk.s4.hl7.cda.convert.decode;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.MissingFormatArgumentException;

import org.apache.commons.lang3.time.FastDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.OrganizationIdentity.OrganizationBuilder;
import dk.s4.hl7.cda.model.Patient.Gender;
import dk.s4.hl7.cda.model.Patient.PatientBuilder;
import dk.s4.hl7.cda.model.Telecom;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.hashing.HashableCharSequence;

public class ConvertXmlUtil {
  private static final String NULL_FLAVOR_VALUE = "NI";
  private static final String UNK_NULL_FLAVOR_VALUE = "UNK";
  private static final String NULL_FLAVOR = "nullFlavor";
  private static final Logger logger = LoggerFactory.getLogger(ConvertXmlUtil.class);

  private static final FastDateFormat BIRTHTIME_FORMATTER = FastDateFormat.getInstance("yyyyMMdd000000+0000");
  private static final FastDateFormat OLD_BIRTHTIME_FORMATTER = FastDateFormat.getInstance("yyyyMMdd");
  private static final FastDateFormat DATETIME_FORMATTER = FastDateFormat.getInstance("yyyyMMddHHmmss");

  public static Telecom createTelecom(XMLElement xmlElement) {
    Use use = ConvertXmlUtil.getUse(xmlElement, Use.HomeAddress);
    return new Telecom(use, getProtocol(xmlElement), getTelecomValue(xmlElement));
  }

  public static void addTelecom(XMLElement xmlElement, PatientBuilder builder) {
    Use use = ConvertXmlUtil.getUse(xmlElement, Use.HomeAddress);
    builder.addTelecom(use, getProtocol(xmlElement), getTelecomValue(xmlElement));
  }

  public static void addTelecom(XMLElement xmlElement, OrganizationBuilder builder) {
    Use use = ConvertXmlUtil.getUse(xmlElement, Use.WorkPlace);
    if (xmlElement.getAttributeValue("value") != null) {
      builder.addTelecom(use, getProtocol(xmlElement), getTelecomValue(xmlElement));
    }
  }

  public static Use getUse(XMLElement xmlElement, Use defaultUse) {
    String use = xmlElement.getAttributeValue("use");
    if (use != null) {
      if (use.equalsIgnoreCase("H")) {
        return AddressData.Use.HomeAddress;
      } else if (use.equalsIgnoreCase("WP")) {
        return AddressData.Use.WorkPlace;
      }
    }
    return defaultUse;
  }

  public static void setGender(PatientBuilder patientBuilder, String gender) {
    if (gender.equalsIgnoreCase("UN")) {
      patientBuilder.setGender(Gender.Undifferentiated);
    } else if (gender.equalsIgnoreCase("F")) {
      patientBuilder.setGender(Gender.Female);
    } else if (gender.equalsIgnoreCase("M")) {
      patientBuilder.setGender(Gender.Male);
    } else {
      logger.warn("Gender contains invalid value - Value is set to 'Undifferentiated'");
      patientBuilder.setGender(Gender.Undifferentiated);
    }
  }

  public static Gender getGender(String gender) {
    if (gender.equalsIgnoreCase("UN")) {
      return Gender.Undifferentiated;
    } else if (gender.equalsIgnoreCase("F")) {
      return Gender.Female;
    } else if (gender.equalsIgnoreCase("M")) {
      return Gender.Male;
    } else {
      logger.warn("Gender contains invalid value - Value is set to 'Undifferentiated'");
      return Gender.Undifferentiated;
    }
  }

  public static boolean isElementPresentOrNullFlavoredUnknown(XMLElement xmlElement,
      HashableCharSequence actualElementName, String targetElementName) {
    if (actualElementName.equalsIgnoreCase(targetElementName)) {
      String nullFlavorValue = xmlElement.getAttributeValue(NULL_FLAVOR);
      return nullFlavorValue == null || isNullFlavorUnknown(nullFlavorValue);
    }
    return false;
  }

  public static boolean isElementPresent(XMLElement xmlElement, String actualElementName, String targetElementName) {
    if (actualElementName.equalsIgnoreCase(targetElementName)) {
      return xmlElement.getAttributeValue(NULL_FLAVOR) == null;
    }
    return false;
  }

  public static boolean isElementPresent(XMLElement xmlElement, HashableCharSequence actualElementName,
      String targetElementName) {
    if (actualElementName.equalsIgnoreCase(targetElementName)) {
      return xmlElement.getAttributeValue(NULL_FLAVOR) == null;
    }
    return false;
  }

  public static boolean isElementValuePresent(XMLElement xmlElement, String actualElementName,
      String targetElementName) {
    if (isElementPresent(xmlElement, actualElementName, targetElementName)) {
      return isTextUseful(xmlElement.getElementValue());
    }
    return false;
  }

  public static boolean isTextUseful(String text) {
    if (text != null) {
      text = text.trim();
      return !text.isEmpty() && !text.equalsIgnoreCase("null") && !text.equalsIgnoreCase(NULL_FLAVOR_VALUE);
    }
    return false;
  }

  public static boolean isNullFlavorUnknown(String text) {
    return text != null && UNK_NULL_FLAVOR_VALUE.equalsIgnoreCase(text.trim());
  }

  public static boolean isNullFlavorUnknown(XMLElement xmlElement) {
    return isNullFlavorUnknown(xmlElement.getAttributeValue(NULL_FLAVOR));
  }

  /**
   * Checks that the expected element is present and that it's not set to
   * nullFlavor. Checks that the expected attributes are present and does not
   * contain a value of 'NI'.
   * 
   * The attributeNames parameter should only include required attributes.
   * Optional attributes should be checked outside of this method.
   * 
   * @param actualElementName
   * @param targetElementName
   * @param attributeNames
   * @return
   */
  public static boolean isAttributePresent(XMLElement xmlElement, String actualElementName, String targetElementName,
      String... attributeNames) {
    if (attributeNames == null || attributeNames.length == 0) {
      throw new MissingFormatArgumentException("Parameter 'attributeNames' must be set!");
    }
    if (isElementPresent(xmlElement, actualElementName, targetElementName)) {
      for (String attributeName : attributeNames) {
        if (!isTextUseful(xmlElement.getAttributeValue(attributeName))) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  public static boolean isAttributePresent(XMLElement xmlElement, HashableCharSequence actualElementName,
      String targetElementName, String... attributeNames) {
    if (attributeNames == null || attributeNames.length == 0) {
      throw new MissingFormatArgumentException("Parameter 'attributeNames' must be set!");
    }
    if (isElementPresent(xmlElement, actualElementName, targetElementName)) {
      for (String attributeName : attributeNames) {
        if (!isTextUseful(xmlElement.getAttributeValue(attributeName))) {
          return false;
        }
      }
      return true;
    }
    return false;
  }

  public static Date getDateFromyyyyMMddhhmmss(String time) {
    if (time != null && !time.trim().isEmpty()) {
      try {
        return DATETIME_FORMATTER.parse(time);
      } catch (ParseException e) {
        logger.warn("Time does not match pattern [" + DATETIME_FORMATTER.getPattern() + "]: " + time);
        return null;
      }
    }
    return null;
  }

  public static String getProtocol(XMLElement xmlElement) {
    String attributeValue = xmlElement.getAttributeValue("value");
    int indexOf = attributeValue.indexOf(":");
    if (indexOf == -1) {
      return null;
    }
    return attributeValue.substring(0, indexOf);
  }

  public static String getTelecomValue(XMLElement xmlElement) {
    String attributeValue = xmlElement.getAttributeValue("value");
    int indexOf = attributeValue.indexOf(":");
    if (indexOf == -1) {
      return null;
    }
    return attributeValue.substring(++indexOf);
  }

  public static void setPersonBirthday(String birthDayText, PatientBuilder patientBuilder) {
    Calendar calendarTime = tryParseBirthTime(birthDayText, BIRTHTIME_FORMATTER);
    if (calendarTime == null) {
      logger.info("Try parsing of birthtime using old pattern");
      calendarTime = tryParseBirthTime(birthDayText, OLD_BIRTHTIME_FORMATTER);
      if (calendarTime == null) {
        return;
      }
    }
    patientBuilder.setBirthTime(calendarTime.get(Calendar.YEAR), calendarTime.get(Calendar.MONTH),
        calendarTime.get(Calendar.DAY_OF_MONTH));
  }

  private static Calendar tryParseBirthTime(String birthDayText, FastDateFormat birthTimeFormatter) {
    try {
      Date birthDay = birthTimeFormatter.parse(birthDayText);
      Calendar calendarTime = Calendar.getInstance();
      calendarTime.setTime(birthDay);
      return calendarTime;
    } catch (ParseException e) {
      logger.warn("birthTime does not match pattern [" + birthTimeFormatter.getPattern() + "]: " + birthDayText);
    }
    return null;
  }

  public static Date getPersonBirthday(String birthDayText) {
    try {
      return BIRTHTIME_FORMATTER.parse(birthDayText);
    } catch (ParseException e) {
      logger.warn("birthTime does not match pattern [" + BIRTHTIME_FORMATTER.getPattern() + "]: " + birthDayText);
    }
    return null;
  }
}
