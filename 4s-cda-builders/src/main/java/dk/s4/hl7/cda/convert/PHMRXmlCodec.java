package dk.s4.hl7.cda.convert;

import java.io.Reader;

import dk.s4.hl7.cda.convert.base.AppendableSerializer;
import dk.s4.hl7.cda.convert.base.Codec;
import dk.s4.hl7.cda.convert.base.ReaderSerializer;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;

/**
 * The implementation of Codec to encode and decode PHMR from objects to XML
 * and XML to objects. After construction the codec is considered thread-safe.
 * 
 * @see https://www.dartlang.org/articles/libraries/converters-and-codecs
 * 
 * @author Frank Jacobsen Systematic
 */
public class PHMRXmlCodec
    implements Codec<PHMRDocument, String>, AppendableSerializer<PHMRDocument>, ReaderSerializer<PHMRDocument> {
  private PHMRXmlConverter phmrXmlConverter;
  private XmlPHMRConverter xmlPhmrConverter;

  public PHMRXmlCodec() {
    this(new XmlPrettyPrinter());
  }

  /**
   * Create PHMRXmlCodec
   * 
   * @param xmlPrettyPrinter May be null
   */
  public PHMRXmlCodec(XmlPrettyPrinter xmlPrettyPrinter) {
    this.phmrXmlConverter = new PHMRXmlConverter(xmlPrettyPrinter);
    this.xmlPhmrConverter = new XmlPHMRConverter();
  }

  public String encode(PHMRDocument source) {
    return phmrXmlConverter.convert(source);
  }

  public PHMRDocument decode(String source) {
    return xmlPhmrConverter.convert(source);
  }

  @Override
  public void serialize(PHMRDocument source, Appendable appendable) {
    if (appendable == null) {
      throw new NullPointerException("Target appendable is null");
    }
    phmrXmlConverter.serialize(source, appendable);
  }

  @Override
  public PHMRDocument deserialize(Reader source) {
    return xmlPhmrConverter.deserialize(source);
  }
}
