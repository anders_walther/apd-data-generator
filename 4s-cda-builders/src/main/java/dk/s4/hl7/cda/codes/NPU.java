package dk.s4.hl7.cda.codes;

import java.util.Date;

import dk.s4.hl7.cda.model.*;
import dk.s4.hl7.cda.model.phmr.Measurement;

//CHECKSTYLE:OFF
public class NPU {
  // Often used codes in NPU for telemedical measurements

  public static final String DISPLAYNAME = "NPU terminologien";
  public static final String CODESYSTEM_OID = "1.2.208.176.2.1";
  public static final String TRANSLATION_SNOMED_CT_NAME = "SNOMED CT";
  public static final String TRANSLATION_SNOMED_CT_OID = "2.16.840.1.113883.6.96";
  public static final String TRANSLATION_MDC_NAME = "MDC Dynamic";
  public static final String TRANSLATION_MDC_OID = "2.16.840.1.113883.6.24";

  public static final String APPOINTMENT_DATE_CODE = "185353001";
  public static final String APPOINTMENT_DATE_DISPLAYNAME = "Aftale dato";

  public static final String DRY_BODY_WEIGHT_CODE = "NPU03804";
  public static final String DRY_BODY_WEIGHT_DISPLAYNAME = "Legeme masse; Pt";

  public static final String BLOOD_PRESURE_SYSTOLIC_ARM_CODE = "DNK05472";
  public static final String BLOOD_PRESURE_SYSTOLIC_ARM_DISPLAYNAME = "Blodtryk systolisk; Arm";

  public static final String BLOOD_PRESURE_DIASTOLIC_ARM_CODE = "DNK05473";
  public static final String BLOOD_PRESURE_DIASTOLIC_ARM_DISPLAYNAME = "Blodtryk diastolisk; Arm";

  public static final String PROTEIN_URINE_CODE = "NPU03958";
  public static final String PROTEIN_URINE_DISPLAYNAME = "Protein;U";

  public static final String SATURATION_CODE = "NPU03011";
  public static final String SATURATION_DISPLAYNAME = "O2 sat.;Hb(aB)";

  public static Measurement createWeight(String weightInKg, Date atTime, ID id) {
    Measurement meas;
    meas = new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setPhysicalQuantity(weightInKg, UCUM.kg, DRY_BODY_WEIGHT_CODE, DRY_BODY_WEIGHT_DISPLAYNAME)
        .setId(id)
        .build();
    return meas;
  }

  public static Measurement createWeight(String weightInKg, Date atTime, DataInputContext context, ID id) {
    Measurement meas;
    meas = new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setPhysicalQuantity(weightInKg, UCUM.kg, DRY_BODY_WEIGHT_CODE, DRY_BODY_WEIGHT_DISPLAYNAME)
        .setContext(context)
        .setId(id)
        .build();
    return meas;
  }

  public static Measurement createBloodPresureSystolic(String systolic, Date atTime, ID id) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setPhysicalQuantity(systolic, UCUM.mmHg, BLOOD_PRESURE_SYSTOLIC_ARM_CODE,
            NPU.BLOOD_PRESURE_SYSTOLIC_ARM_DISPLAYNAME)
        .setId(id)
        .build();
  }

  public static Measurement createBloodPresureSystolic(String systolic, Date atTime, DataInputContext context, ID id) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setPhysicalQuantity(systolic, UCUM.mmHg, BLOOD_PRESURE_SYSTOLIC_ARM_CODE,
            NPU.BLOOD_PRESURE_SYSTOLIC_ARM_DISPLAYNAME)
        .setId(id)
        .setContext(context)
        .build();
  }

  public static Measurement createBloodPresureDiastolic(String diastolic, Date atTime, ID id) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setPhysicalQuantity(diastolic, UCUM.mmHg, BLOOD_PRESURE_DIASTOLIC_ARM_CODE,
            BLOOD_PRESURE_DIASTOLIC_ARM_DISPLAYNAME)
        .setId(id)
        .build();
  }

  public static Measurement createBloodPresureDiastolic(String diastolic, Date atTime, DataInputContext context,
      ID id) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setPhysicalQuantity(diastolic, UCUM.mmHg, BLOOD_PRESURE_DIASTOLIC_ARM_CODE,
            BLOOD_PRESURE_DIASTOLIC_ARM_DISPLAYNAME)
        .setContext(context)
        .setId(id)
        .build();
  }

  public static Measurement createProteinUrine(String protein, Date atTime, ID id) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setPhysicalQuantity(protein, UCUM.gPerL, PROTEIN_URINE_CODE, PROTEIN_URINE_DISPLAYNAME)
        .setId(id)
        .build();
  }

  public static Measurement createProteinUrine(String protein, Date atTime, DataInputContext context, ID id) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setPhysicalQuantity(protein, UCUM.gPerL, PROTEIN_URINE_CODE, PROTEIN_URINE_DISPLAYNAME)
        .setId(id)
        .setContext(context)
        .build();
  }

  public static Measurement createSaturation(String saturation, Date atTime, ID id) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setPhysicalQuantity(saturation, UCUM.NA, SATURATION_CODE, SATURATION_DISPLAYNAME)
        .setId(id)
        .build();
  }

  public static Measurement createSaturation(String saturation, Date atTime, DataInputContext context, ID id) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setPhysicalQuantity(saturation, UCUM.NA, SATURATION_CODE, SATURATION_DISPLAYNAME)
        .setContext(context)
        .setId(id)
        .build();
  }

  public static Measurement createCTG(String id1, String reference, Date atTime, DataInputContext context, ID id) {
    return new Measurement.MeasurementBuilder(atTime, Measurement.Status.COMPLETED)
        .setObservationMediaReference(id1, reference)
        .setContext(context)
        .setId(id)
        .build();
  }
}
