package dk.s4.hl7.cda.convert;

import java.io.IOException;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.convert.encode.ClinicalDocumentConverter;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

/**
 * An implementation of the Converter that can build an XML Document object
 * following the Danish Appointment standard.
 */
public class APDXmlConverter extends ClinicalDocumentConverter<AppointmentDocument> {

  public APDXmlConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    super(xmlPrettyPrinter);
  }

  @Override
  public String convert(AppointmentDocument source) {
    StringBuilder builder = new StringBuilder(10000);
    serialize(source, builder);
    return builder.toString();
  }

  @Override
  protected void buildRootNode(XmlStreamBuilder xmlBuilder) throws IOException {
    // CONF-PHMR-1:
    xmlBuilder.addDefaultPI();
    xmlBuilder
        .element("ClinicalDocument")
        .attribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance")
        .attribute("xmlns", "urn:hl7-org:v3")
        .attribute("xmlns:sdtc", "urn:hl7-org:sdtc")
        .attribute("classCode", "DOCCLIN")
        .attribute("moodCode", "EVN")
        .attribute("xsi:schemaLocation", "urn:hl7-org:v3 ../../PHMR/Schema/CDA_SDTC.xsd");
  }

  @Override
  protected void buildBody(AppointmentDocument appointment, XmlStreamBuilder xmlBuilder) throws IOException {
    this.buildStructuredBodySectionStart(xmlBuilder);
    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIdWithExtension(xmlBuilder, HL7.APD_ID, HL7.APD_ID_EXTENSION);

    BuildUtil.buildCode(Loinc.SECTION_APD_CODE, Loinc.OID, Loinc.SECTION_APD_DISPLAYNAME, Loinc.DISPLAYNAME,
        xmlBuilder);

    xmlBuilder.element("title").value(appointment.getAppointmentTitle()).elementEnd();
    xmlBuilder.element("text").valueNoEscaping(appointment.getAppointmentText()).elementEnd();

    xmlBuilder.element("entry");

    buildEncounter(appointment, xmlBuilder);

    xmlBuilder.elementEnd(); // end entry
    xmlBuilder.elementEnd(); // end section
    xmlBuilder.elementEnd(); // end component
    this.buildStructuredBodySectionEnd(xmlBuilder);
  }

  private void buildEncounter(AppointmentDocument appointment, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("encounter").attribute("moodCode", "APT").attribute("classCode", "ENC");
    BuildUtil.buildTemplateIdWithExtension(xmlBuilder, HL7.APD_ENCOUNTER_ID, HL7.APD_ID_EXTENSION);
    BuildUtil.buildId(MedCom.ROOT_OID, MedCom.ROOT_AUTHORITYNAME, appointment.getAppointmentId(), xmlBuilder);
    BuildUtil.buildCode(NPU.APPOINTMENT_DATE_CODE, NPU.TRANSLATION_SNOMED_CT_OID, NPU.APPOINTMENT_DATE_DISPLAYNAME,
        NPU.TRANSLATION_SNOMED_CT_NAME, xmlBuilder);

    xmlBuilder.element("statusCode").attribute("code", getStatusCode(appointment)).elementShortEnd();

    buildEffectiveTime(appointment, xmlBuilder);

    buildPerformer(appointment.getAppointmentPerformer(), xmlBuilder);
    buildAuthorSection(null, appointment.getAppointmentAuthor(), xmlBuilder);
    buildLocation(appointment.getAppointmentLocation(), xmlBuilder);

    xmlBuilder.element("entryRelationship").attribute("typeCode", "RSON");
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "RQO");

    xmlBuilder
        .element("code")
        .attribute("code", "NI")
        .attribute("displayName", appointment.getIndicationDisplayName())
        .elementShortEnd();

    xmlBuilder.elementEnd(); // end observation
    xmlBuilder.elementEnd(); // end entryRelationship
    xmlBuilder.elementEnd(); // end encounter
  }

  private static String getStatusCode(AppointmentDocument a) {
    switch (a.getAppointmentStatus()) {
    case ACTIVE:
      return "active";
    case CANCELLED:
      return "cancelled";
    default:
      throw new IllegalStateException("Invalid Appointment status" + a.getAppointmentStatus());
    }
  }

  private void buildPerformer(Participant participant, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("performer").attribute("typeCode", "PRF");
    buildAssignedEntity(participant, xmlBuilder);
    xmlBuilder.elementEnd(); // end performer
  }

  private void buildLocation(OrganizationIdentity organizationIdentity, XmlStreamBuilder xmlBuilder)
      throws IOException {
    xmlBuilder.element("participant").attribute("typeCode", "LOC");
    xmlBuilder.element("participantRole").attribute("classCode", "SDLOC");

    BuildUtil.buildTemplateIdWithExtension(xmlBuilder, HL7.APD_LOCATION_ID, HL7.APD_ID_EXTENSION);
    buildSORId(organizationIdentity, xmlBuilder);
    buildAddress(organizationIdentity.getAddress(), xmlBuilder);
    buildTelecom(organizationIdentity.getTelecomList(), xmlBuilder);

    xmlBuilder.element("playingEntity").attribute("classCode", "PLC");

    xmlBuilder.element("name").value(organizationIdentity.getOrgName()).elementEnd();

    xmlBuilder.elementEnd(); // end playingEntity

    xmlBuilder.elementEnd(); // end participantRole
    xmlBuilder.elementEnd(); // end participant
  }

  @Override
  protected void buildDocumentationOf(AppointmentDocument source, XmlStreamBuilder xmlBuilder) throws IOException {

    xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
    xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");

    buildEffectiveTime(source, xmlBuilder);

    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf
  }

  private void buildEffectiveTime(AppointmentDocument source, XmlStreamBuilder xmlBuilder) throws IOException {
    xmlBuilder.element("effectiveTime");

    if (source.getServiceStartTime() != null) {
      xmlBuilder
          .element("low")
          .attribute("value", dateTimeformatter.format(source.getServiceStartTime()))
          .elementShortEnd();
    }
    if (source.getServiceStopTime() != null) {
      xmlBuilder
          .element("high")
          .attribute("value", dateTimeformatter.format(source.getServiceStopTime()))
          .elementShortEnd();
    }
    xmlBuilder.elementEnd(); // end effectiveTime
  }

  @Override
  protected void buildDataEnterer(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // Not used
  }

  @Override
  protected void buildInformationRecipient(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // Not used
  }

  @Override
  protected void buildParticipants(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // Not used
  }

  @Override
  protected void buildLegalAuthenticatorSection(ClinicalDocument document, XmlStreamBuilder xmlBuilder)
      throws IOException {
    // Not used	  
  }

}
