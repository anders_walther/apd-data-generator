package dk.s4.hl7.cda.convert.decode.apd;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.OrganizationHandler;
import dk.s4.hl7.cda.convert.decode.header.ParticipantHandler;
import dk.s4.hl7.cda.convert.decode.header.ParticipantHandler.ParticipantType;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.cda.model.apd.AppointmentDocument.Status;
import dk.s4.hl7.util.xml.RawTextHandler;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class SectionHandler implements CDAXmlHandler<AppointmentDocument> {

  public static final String COMPONENT_SECTION = "/ClinicalDocument/component/structuredBody/component/section";

  public static final String ENCOUNTER_SECTION = "/ClinicalDocument/component/structuredBody/component/section/entry/encounter";

  private String title;

  private Status status;

  private String id;

  private String indicationDisplayName;

  private RawTextHandler textHandler;

  private ParticipantHandler authorHandler;

  private ParticipantHandler performerHandler;

  private OrganizationHandler locationHandler;

  public SectionHandler() {
    textHandler = new RawTextHandler(COMPONENT_SECTION + "/text", "text");
    authorHandler = new ParticipantHandler(ParticipantType.PARTICIPANT, ENCOUNTER_SECTION + "/author/assignedAuthor",
        ENCOUNTER_SECTION + "/author/time", "assignedPerson", "/representedOrganization");
    performerHandler = new ParticipantHandler(ParticipantType.PARTICIPANT,
        ENCOUNTER_SECTION + "/performer/assignedEntity", ENCOUNTER_SECTION + "/performer/time", "assignedPerson",
        "/representedOrganization");
    locationHandler = new OrganizationHandler(ENCOUNTER_SECTION + "/participant/participantRole",
        "/playingEntity/name");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "title")) {
      title = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "statusCode", "code")) {
      status = Status.valueOf(xmlElement.getAttributeValue("code").toUpperCase());
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "extension")) {
      id = xmlElement.getAttributeValue("extension");
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "displayName")) {
      indicationDisplayName = xmlElement.getAttributeValue("displayName");
    } else {

    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    //ignore
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    textHandler.addHandlerToMap(xmlMapping);
    authorHandler.addHandlerToMap(xmlMapping);
    performerHandler.addHandlerToMap(xmlMapping);
    locationHandler.addHandlerToMap(xmlMapping);
    xmlMapping.add(COMPONENT_SECTION, this);
    xmlMapping.add(COMPONENT_SECTION + "/title", this);
    xmlMapping.add(COMPONENT_SECTION + "/entry/encounter/statusCode", this);
    xmlMapping.add(COMPONENT_SECTION + "/entry/encounter/id", this);
    xmlMapping.add(COMPONENT_SECTION + "/entry/encounter/entryRelationship/observation/code", this);

  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    textHandler.removeHandlerFromMap(xmlMapping);
    authorHandler.removeHandlerFromMap(xmlMapping);
    performerHandler.removeHandlerFromMap(xmlMapping);
    locationHandler.removeHandlerFromMap(xmlMapping);
    xmlMapping.remove(COMPONENT_SECTION);
    xmlMapping.remove(COMPONENT_SECTION + "/title");
    xmlMapping.remove(COMPONENT_SECTION + "/entry/encounter/statusCode");
    xmlMapping.remove(COMPONENT_SECTION + "/entry/encounter/id");
    xmlMapping.remove(COMPONENT_SECTION + "/entry/encounter/entryRelationship/observation/code");

  }

  @Override
  public void addDataToDocument(AppointmentDocument clinicalDocument) {
    clinicalDocument.setAppointmentTitle(title);
    clinicalDocument.setAppointmentId(id);
    clinicalDocument.setAppointmentStatus(status);
    clinicalDocument.setIndicationDisplayName(indicationDisplayName);

    clinicalDocument.setAppointmentText(textHandler.getRawText());
    clinicalDocument.setAppointmentAuthor(authorHandler.getParticipants().get(0));
    clinicalDocument.setAppointmentPerformer(performerHandler.getParticipants().get(0));
    clinicalDocument.setAppointmentLocation(locationHandler.getOrganization());
  }

}
