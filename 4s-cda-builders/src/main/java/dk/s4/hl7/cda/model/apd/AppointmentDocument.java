package dk.s4.hl7.cda.model.apd;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant;
import dk.s4.hl7.cda.model.core.BaseClinicalDocument;

/**
 * An implementation of SimpleClinicalDocument that only supports the data of
 * the Danish Appointment.
 * 
 */
public final class AppointmentDocument extends BaseClinicalDocument {

  public enum Status {
    ACTIVE,
    CANCELLED
  }

  private Status appointmentStatus;

  private String appointmentText;

  private String appointmentTitle;

  private String appointmentId;

  private String indicationDisplayName;

  private Participant appointmentAuthor;

  private Participant appointmentPerformer;

  private OrganizationIdentity appointmentLocation;

  public AppointmentDocument(ID id) {
    super(HL7.CDA_TYPEID_ROOT, HL7.CDA_TYPEID_EXTENSION, id, Loinc.APD_CODE, Loinc.APD_DISPLAYNAME,
        new String[] { MedCom.DK_APD_ROOT_OID }, HL7.REALM_CODE_DK);
  }

  public String getAppointmentText() {
    return appointmentText;
  }

  public void setAppointmentText(String appointmentText) {
    this.appointmentText = appointmentText;
  }

  public String getAppointmentTitle() {
    return appointmentTitle;
  }

  public void setAppointmentTitle(String appointmentTitle) {
    this.appointmentTitle = appointmentTitle;
  }

  public String getAppointmentId() {
    return appointmentId;
  }

  public void setAppointmentId(String id) {
    this.appointmentId = id;
  }

  public Status getAppointmentStatus() {
    return appointmentStatus;
  }

  public void setAppointmentStatus(Status appointmentStatus) {
    this.appointmentStatus = appointmentStatus;
  }

  public String getIndicationDisplayName() {
    return indicationDisplayName;
  }

  public void setIndicationDisplayName(String indicationDisplayName) {
    this.indicationDisplayName = indicationDisplayName;
  }

  public Participant getAppointmentAuthor() {
    return appointmentAuthor;
  }

  public void setAppointmentAuthor(Participant appointmentAuthor) {
    this.appointmentAuthor = appointmentAuthor;
  }

  public Participant getAppointmentPerformer() {
    return appointmentPerformer;
  }

  public void setAppointmentPerformer(Participant appointmentPerformer) {
    this.appointmentPerformer = appointmentPerformer;
  }

  public OrganizationIdentity getAppointmentLocation() {
    return appointmentLocation;
  }

  public void setAppointmentLocation(OrganizationIdentity appointmentLocation) {
    this.appointmentLocation = appointmentLocation;
  }

}
