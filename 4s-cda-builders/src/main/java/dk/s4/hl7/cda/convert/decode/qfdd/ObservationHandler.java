package dk.s4.hl7.cda.convert.decode.qfdd;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.IdHandler;
import dk.s4.hl7.cda.convert.decode.general.QuestionOptionsPatternHandler;
import dk.s4.hl7.cda.convert.decode.general.ReferenceHandler;
import dk.s4.hl7.cda.convert.decode.general.ResponseReferenceRangePatternHandler;
import dk.s4.hl7.cda.convert.decode.general.ValueCollection;
import dk.s4.hl7.cda.convert.decode.general.ValueCollection.CE_INDEX;
import dk.s4.hl7.cda.convert.decode.qfdd.precondition.PreconditionHandler;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDAnalogSliderQuestion.QFDDAnalogSliderQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDDiscreteSliderQuestion.QFDDDiscreteSliderQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDMultipleChoiceQuestion.QFDDMultipleChoiceQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDNumericQuestion.QFDDNumericQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion;
import dk.s4.hl7.cda.model.qfdd.QFDDQuestion.BaseQFDDQuestionBuilder;
import dk.s4.hl7.cda.model.qfdd.QFDDTextQuestion.QFDDTextQuestionBuilder;
import dk.s4.hl7.util.xml.ElementAttributeFinder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class ObservationHandler extends BaseXmlHandler {
  public static final String OBSERVATION_SECTION = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation";
  private List<QFDDQuestion> questions;

  /**
   * There may be multiple template ids for the pattern. There's generally only
   * one template id but in the case of analog and discrete slider there are
   * two: analog or discrete slider oid and multiple choice oid
   */
  private Set<String> templates;
  private CodedValue questionCode;
  private String question;
  private ValueCollection valueCollection;

  private IdHandler idHandler;
  private ReferenceHandler referenceHandler;
  private QuestionOptionsPatternHandler questionOptionsPatternHandler;
  private ResponseReferenceRangePatternHandler responseReferenceRangePatternhandler;
  private FeedbackHandler feedbackHandler;
  private HelpTextHandler helpTextHandler;
  private ElementAttributeFinder elementAttributeFinder;
  private PreconditionHandler preconditionHandler;

  public ObservationHandler() {
    this.idHandler = new IdHandler(OBSERVATION_SECTION);
    this.referenceHandler = new ReferenceHandler();
    this.feedbackHandler = new FeedbackHandler();
    this.helpTextHandler = new HelpTextHandler();
    this.questionOptionsPatternHandler = new QuestionOptionsPatternHandler();
    this.responseReferenceRangePatternhandler = new ResponseReferenceRangePatternHandler();
    this.elementAttributeFinder = createEntryRelationshipFinder();
    this.preconditionHandler = new PreconditionHandler(OBSERVATION_SECTION + "/precondition");
    this.templates = new HashSet<String>();
    this.valueCollection = new ValueCollection();
    this.questions = new ArrayList<QFDDQuestion>();

    addPath(OBSERVATION_SECTION);
    addPath(OBSERVATION_SECTION + "/value");
    addPath(OBSERVATION_SECTION + "/templateId");
    addPath(OBSERVATION_SECTION + "/code");
    addPath(OBSERVATION_SECTION + "/code/originalText");
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "templateId", "root")) {
      templates.add(xmlElement.getAttributeValue("root"));
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code",
        "codeSystem")) {
      questionCode = new CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    } else if (ConvertXmlUtil.isElementValuePresent(xmlElement, xmlElement.getElementName(), "originalText")) {
      question = xmlElement.getElementValue();
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "value", "type")) {
      valueCollection.addValue(xmlElement);
    }
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "observation")) {
      String responseOid = determineObservationType();
      if (responseOid.equalsIgnoreCase(HL7.QFDD_ANALOG_SLIDER_PATTERN_OID)) {
        createAnalogSliderQuestion();
      } else if (responseOid.equalsIgnoreCase(HL7.QFDD_DISCRETE_SLIDER_PATTERN_OID)) {
        createDiscreteSliderQuestion();
      } else if (responseOid.equalsIgnoreCase(HL7.QFDD_NUMERIC_PATTERN_OID)) {
        createNumericQuestion();
      } else if (responseOid.equalsIgnoreCase(HL7.QFDD_TEXT_QUESTION_PATTERN_OID)) {
        createTextQuestion();
      } else if (responseOid.equalsIgnoreCase(HL7.QFDD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID)) {
        createMultipleChoiceQuestion();
      }
      localClear();
    }
  }

  private String determineObservationType() {
    String result = "";
    if (templates.size() == 1) {
      return templates.iterator().next();
    } else if (templates.size() > 1) {
      Iterator<String> itr = templates.iterator();
      while (itr.hasNext()) {
        String oid = itr.next();
        if (oid.equalsIgnoreCase(HL7.QFDD_ANALOG_SLIDER_PATTERN_OID)
            || oid.equalsIgnoreCase(HL7.QFDD_DISCRETE_SLIDER_PATTERN_OID)) {
          result = oid;
          break; // Analog slider and discrete slider overrules other oids
        }
        result = oid;
      }
    }
    return result;
  }

  private void createAnalogSliderQuestion() {
    QFDDAnalogSliderQuestionBuilder builder = new QFDDAnalogSliderQuestionBuilder();
    builder.setCodeValue(questionCode).setId(idHandler.getId()).setQuestion(question);
    if (responseReferenceRangePatternhandler.hasValues() && !responseReferenceRangePatternhandler.isNumericResponse()) {
      builder.setInterval(responseReferenceRangePatternhandler.getMinimum(),
          responseReferenceRangePatternhandler.getMaximum(), responseReferenceRangePatternhandler.getIncrement());
    }
    builder.setFeedback(feedbackHandler.getFeedback());
    builder.setHelpText(helpTextHandler.getHelpText());
    addPreconditions(builder);
    addReferences(builder);
    questions.add(builder.build());
  }

  private void createDiscreteSliderQuestion() {
    QFDDDiscreteSliderQuestionBuilder builder = new QFDDDiscreteSliderQuestionBuilder();
    builder.setCodeValue(questionCode).setId(idHandler.getId()).setQuestion(question);
    if (questionOptionsPatternHandler.hasValues()) {
      builder.setMinimum(questionOptionsPatternHandler.getMinimum());
    }
    List<List<String>> values = valueCollection.getValues();
    for (List<String> value : values) {
      if (value != null && !value.isEmpty()) {
        builder.addAnswerOption(value.get(CE_INDEX.CODE.ordinal()), value.get(CE_INDEX.CODE_SYSTEM.ordinal()),
            value.get(CE_INDEX.DISPLAY_NAME.ordinal()), value.get(CE_INDEX.CODE_SYSTEM_NAME.ordinal()));
      }
    }
    builder.setFeedback(feedbackHandler.getFeedback());
    builder.setHelpText(helpTextHandler.getHelpText());
    addPreconditions(builder);
    addReferences(builder);
    questions.add(builder.build());
  }

  private void createNumericQuestion() {
    QFDDNumericQuestionBuilder builder = new QFDDNumericQuestionBuilder()
        .setCodeValue(questionCode)
        .setId(idHandler.getId())
        .setQuestion(question);
    if (responseReferenceRangePatternhandler.hasValues()) {
      builder.setInterval(responseReferenceRangePatternhandler.getMinimum(),
          responseReferenceRangePatternhandler.getMaximum(), responseReferenceRangePatternhandler.getIntervalType());
    }
    builder.setFeedback(feedbackHandler.getFeedback());
    builder.setHelpText(helpTextHandler.getHelpText());
    addPreconditions(builder);
    addReferences(builder);
    questions.add(builder.build());
  }

  private void createMultipleChoiceQuestion() {
    QFDDMultipleChoiceQuestionBuilder builder = new QFDDMultipleChoiceQuestionBuilder();
    builder.setCodeValue(questionCode).setId(idHandler.getId()).setQuestion(question);
    if (questionOptionsPatternHandler.hasValues()) {
      builder.setInterval(questionOptionsPatternHandler.getMinimum(), questionOptionsPatternHandler.getMaximum());
    }
    List<List<String>> values = valueCollection.getValues();
    for (List<String> value : values) {
      if (value != null && !value.isEmpty()) {
        builder.addAnswerOption(value.get(CE_INDEX.CODE.ordinal()), value.get(CE_INDEX.CODE_SYSTEM.ordinal()),
            value.get(CE_INDEX.DISPLAY_NAME.ordinal()), value.get(CE_INDEX.CODE_SYSTEM_NAME.ordinal()));
      }
    }
    builder.setFeedback(feedbackHandler.getFeedback());
    builder.setHelpText(helpTextHandler.getHelpText());
    addPreconditions(builder);
    addReferences(builder);
    questions.add(builder.build());
  }

  private void createTextQuestion() {
    QFDDTextQuestionBuilder builder = new QFDDTextQuestionBuilder();
    builder.setCodeValue(questionCode).setId(idHandler.getId()).setQuestion(question);
    builder.setFeedback(feedbackHandler.getFeedback());
    builder.setHelpText(helpTextHandler.getHelpText());
    addPreconditions(builder);
    addReferences(builder);
    questions.add(builder.build());
  }

  private void addReferences(BaseQFDDQuestionBuilder<?, ?> builder) {
    // Currently no references for QFDD - Maybe in future DK profile
  }

  private void addPreconditions(BaseQFDDQuestionBuilder<?, ?> builder) {
    for (QFDDPrecondition precondition : preconditionHandler.getPreconditions()) {
      builder.addPrecondition(precondition);
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    idHandler.addHandlerToMap(xmlMapping);
    referenceHandler.addHandlerToMap(xmlMapping);
    responseReferenceRangePatternhandler.addHandlerToMap(xmlMapping);
    elementAttributeFinder.addHandlerToMap(xmlMapping);
    preconditionHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    idHandler.removeHandlerFromMap(xmlMapping);
    referenceHandler.removeHandlerFromMap(xmlMapping);
    responseReferenceRangePatternhandler.removeHandlerFromMap(xmlMapping);
    elementAttributeFinder.removeHandlerFromMap(xmlMapping);
    preconditionHandler.removeHandlerFromMap(xmlMapping);
  }

  private ElementAttributeFinder createEntryRelationshipFinder() {
    ElementAttributeFinder elementAttributeFinder = new ElementAttributeFinder(
        OBSERVATION_SECTION + "/entryRelationship/observation/templateId", "templateId");
    elementAttributeFinder.addAttributeMatchers("root", HL7.QFDD_QUESTION_FEEDBACK_PATTERN_OID, feedbackHandler);
    elementAttributeFinder.addAttributeMatchers("root", HL7.QFDD_QUESTION_HELP_TEXT_PATTERN_OID, helpTextHandler);
    elementAttributeFinder.addAttributeMatchers("root", HL7.QUESTION_OPTIONS_PATTERN_OBSERVATION_OID,
        questionOptionsPatternHandler);
    return elementAttributeFinder;
  }

  private void localClear() {
    templates.clear();
    questionCode = null;
    question = null;
    valueCollection.clear();
    idHandler.clear();
    referenceHandler.clear();
    questionOptionsPatternHandler.clear();
    responseReferenceRangePatternhandler.clear();
    feedbackHandler.clear();
    helpTextHandler.clear();
    preconditionHandler.clear();
  }

  public List<QFDDQuestion> getQuestions() {
    return questions;
  }

  public void clear() {
    localClear();
    questions.clear();
  }
}
