package dk.s4.hl7.cda.model;

/**
 * Generic class representing an interval
 */
public class Interval {
  private String lowValue;
  private boolean lowValueInclusive;
  private boolean lowValueUnknown;
  private String highValue;
  private boolean highValueInclusive;
  private boolean highValueUnknown;

  public String getLowValue() {
    return lowValue;
  }

  public boolean isLowValueInclusive() {
    return lowValueInclusive;
  }

  public String getHighValue() {
    return highValue;
  }

  public boolean isHighValueInclusive() {
    return highValueInclusive;
  }

  public boolean isSameValue() {
    if (lowValue != null) {
      return lowValue.equals(highValue);
    } else {
      return highValue == null;
    }
  }

  public boolean isBothUnknown() {
    return isLowValueUnknown() && isHighValueUnknown();
  }

  public boolean isLowValueUnknown() {
    return (lowValue == null) && lowValueUnknown;
  }

  public boolean isHighValueUnknown() {
    return (highValue == null) && highValueUnknown;
  }

  public static class IntervalBuilder {
    private String lowValue;
    private boolean lowValueInclusive;
    private boolean lowValueUnknown = true;
    private String highValue;
    private boolean highValueInclusive;
    private boolean highValueUnknown = true;

    public Interval build() {
      return new Interval(this);
    }

    public IntervalBuilder setLowValue(String lowValue, boolean inclusive) {
      if (lowValue == null) {
        throw new IllegalArgumentException("lowValue cannot be null");
      }
      this.lowValue = lowValue;
      this.lowValueInclusive = inclusive;
      this.lowValueUnknown = false;
      return this;
    }

    public IntervalBuilder setLowValueUnknown() {
      this.lowValue = null;
      this.lowValueInclusive = false;
      this.lowValueUnknown = true;
      return this;
    }

    public IntervalBuilder setHighValue(String highValue, boolean inclusive) {
      if (highValue == null) {
        throw new IllegalArgumentException("highValue cannot be null");
      }
      this.highValue = highValue;
      this.highValueInclusive = inclusive;
      this.highValueUnknown = false;
      return this;
    }

    public IntervalBuilder setHighValueUnknown() {
      this.highValue = null;
      this.highValueInclusive = false;
      this.highValueUnknown = true;
      return this;
    }
  }

  protected Interval(IntervalBuilder builder) {
    super();
    this.lowValue = builder.lowValue;
    this.lowValueInclusive = builder.lowValueInclusive;
    this.lowValueUnknown = builder.lowValueUnknown;
    this.highValue = builder.highValue;
    this.highValueInclusive = builder.highValueInclusive;
    this.highValueUnknown = builder.highValueUnknown;
  }

}
