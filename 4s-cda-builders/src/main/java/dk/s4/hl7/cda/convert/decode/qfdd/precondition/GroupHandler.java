package dk.s4.hl7.cda.convert.decode.qfdd.precondition;

import dk.s4.hl7.cda.convert.decode.general.BaseXmlHandler;
import dk.s4.hl7.cda.convert.decode.general.IdHandler;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup.GroupType;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup.QFDDPreconditionGroupBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlMapping;

public class GroupHandler extends BaseXmlHandler {
  private QFDDPreconditionGroupBuilder groupBuilder;
  private PreconditionHandler preconditionHandler;
  private IdHandler idHandler;
  private GroupType groupType;

  public GroupHandler(String baseXPath, GroupType groupType) {
    this.groupBuilder = new QFDDPreconditionGroupBuilder();
    groupBuilder.setGroupType(groupType);
    this.groupType = groupType;
    String groupXPathBase = baseXPath + "/" + groupType.toXmlElementName();
    this.idHandler = new IdHandler(groupXPathBase);
    this.preconditionHandler = new PreconditionHandler(groupXPathBase + "/precondition");
  }

  public GroupType getGroupType() {
    return groupType;
  }

  @Override
  public boolean includeChildren() {
    return false;
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Do nothing
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    // Do nothing
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    super.addHandlerToMap(xmlMapping);
    preconditionHandler.addHandlerToMap(xmlMapping);
    idHandler.addHandlerToMap(xmlMapping);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    super.removeHandlerFromMap(xmlMapping);
    preconditionHandler.removeHandlerFromMap(xmlMapping);
    idHandler.removeHandlerFromMap(xmlMapping);
  }

  public QFDDPreconditionGroup getPreconditionGroup() {
    groupBuilder.setId(idHandler.getId());
    for (QFDDPrecondition precondition : preconditionHandler.getPreconditions()) {
      groupBuilder.addPrecondition(precondition);
    }
    preconditionHandler.clear();
    return groupBuilder.build();
  }
}
