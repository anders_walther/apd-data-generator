package dk.s4.hl7.cda.codes;

import dk.s4.hl7.cda.convert.decode.CdaBuilderException;

public enum IntervalType {
  IVL_INT,
  IVL_REAL,
  IVL_TS,
  GLIST_PQ;

  public static IntervalType findMatchingIntervalType(String type) {
    for (IntervalType intervalType : IntervalType.values()) {
      if (intervalType.name().equalsIgnoreCase(type)) {
        return intervalType;
      }
    }
    throw new CdaBuilderException("Unknown value interval type: " + type);
  }
}
