package dk.s4.hl7.cda.model;

import dk.s4.hl7.cda.model.util.ModelUtil;

/**
 * Generic class representing the base information for both QFDD Questions and QRD Responses
 */
public class QuestionnaireEntity {
  private String question;
  private CodedValue codeValue;
  private ID id;

  public static abstract class QuestionnaireEntityBuilder<R extends QuestionnaireEntity, T extends QuestionnaireEntityBuilder<R, T>> {
    protected String question;
    protected CodedValue codeValue;
    protected ID id;

    public QuestionnaireEntityBuilder() {

    }

    public T setQuestion(String question) {
      this.question = question;
      return getThis();
    }

    public T setCodeValue(CodedValue codeValue) {
      this.codeValue = codeValue;
      return getThis();
    }

    public T setId(ID id) {
      this.id = id;
      return getThis();
    }

    /**
     * This fixes the need for type casting
     * 
     * @return T
     */
    public abstract T getThis();

    public R build() {
      ModelUtil.checkNull(question, "The question is mandatory");
      ModelUtil.checkNull(codeValue, "The code is mandatory");
      ModelUtil.checkNull(id, "The response Id is mandatory");
      return null;
    }

  }

  protected QuestionnaireEntity(QuestionnaireEntityBuilder<?, ?> builder) {
    this.question = builder.question;
    this.codeValue = builder.codeValue;
    this.id = builder.id;
  }

  public String getQuestion() {
    return question;
  }

  public CodedValue getCode() {
    if (codeValue == null) {
      return null;
    }
    return codeValue;
  }

  public ID getId() {
    return id;
  }
}
