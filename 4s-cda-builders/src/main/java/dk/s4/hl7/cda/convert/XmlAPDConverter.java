package dk.s4.hl7.cda.convert;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import dk.s4.hl7.cda.convert.decode.CDAXmlHandler;
import dk.s4.hl7.cda.convert.decode.ClinicalDocumentXmlConverter;
import dk.s4.hl7.cda.convert.decode.apd.SectionHandler;
import dk.s4.hl7.cda.convert.decode.header.CDAHeaderHandler;
import dk.s4.hl7.cda.convert.decode.header.CustodianHandler;
import dk.s4.hl7.cda.convert.decode.header.DocumentationOfHandler;
import dk.s4.hl7.cda.convert.decode.header.PatientHandler;
import dk.s4.hl7.cda.model.apd.AppointmentDocument;
import dk.s4.hl7.cda.model.core.ClinicalDocument;

/**
 * Parse Appointment xml to GreenCDA model
 */
public class XmlAPDConverter extends ClinicalDocumentXmlConverter<AppointmentDocument> {

  @Override
  public AppointmentDocument convert(String source) {
    return deserialize(new StringReader(source));
  }

  @Override
  protected List<CDAXmlHandler<ClinicalDocument>> createParticipantHandlers() {
    List<CDAXmlHandler<ClinicalDocument>> list = new ArrayList<CDAXmlHandler<ClinicalDocument>>();
    list.add(new PatientHandler());
    list.add(createAuthorHandler());
    list.add(new CustodianHandler());
    list.add(new DocumentationOfHandler());
    return list;
  }

  @Override
  protected List<CDAXmlHandler<AppointmentDocument>> createSectionHandlers() {
    List<CDAXmlHandler<AppointmentDocument>> list = new ArrayList<CDAXmlHandler<AppointmentDocument>>();
    list.add(new SectionHandler());
    return list;
  }

  @Override
  protected AppointmentDocument createNewDocument(CDAHeaderHandler headerHandler) {
    return new AppointmentDocument(headerHandler.getDocumentId());
  }

}
