package dk.s4.hl7.cda.convert.decode.general;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.convert.decode.ConvertXmlUtil;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.ID.IDBuilder;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.Reference.ReferenceBuilder;
import dk.s4.hl7.util.xml.XMLElement;
import dk.s4.hl7.util.xml.XmlHandler;
import dk.s4.hl7.util.xml.XmlMapping;

public class ReferenceHandler implements XmlHandler {
  private static Logger logger = LoggerFactory.getLogger(ReferenceHandler.class);
  public static final String REFERENCE = "/ClinicalDocument/component/structuredBody/component/section/entry/organizer/component/observation/reference";

  private List<Reference> references;
  private ReferenceType referenceType;
  private ID referenceDocumentId;
  private ID referenceObservationId;
  private String documentIdReferencesUse;
  private CodedValue codeValue;

  private enum ReferenceType {
    UNKNOWN,
    EXTERNAL_DOCUMENT,
    EXTERNAL_OBSERVATION
  }

  public ReferenceHandler() {
    this.references = new ArrayList<Reference>();
    this.referenceType = ReferenceType.UNKNOWN;
  }

  public void clear() {
    references.clear();
  }

  @Override
  public void handleElementStart(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "externalDocument")) {
      referenceType = ReferenceType.EXTERNAL_DOCUMENT;
    } else if (ConvertXmlUtil.isElementPresent(xmlElement, xmlElement.getElementName(), "externalObservation")) {
      referenceType = ReferenceType.EXTERNAL_OBSERVATION;
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "id", "root", "extension")) {
      readID(xmlElement);
    } else if (ConvertXmlUtil.isAttributePresent(xmlElement, xmlElement.getElementName(), "code", "code",
        "codeSystem")) {
      codeValue = new CodedValueBuilder()
          .setCode(xmlElement.getAttributeValue("code"))
          .setCodeSystem(xmlElement.getAttributeValue("codeSystem"))
          .setDisplayName(xmlElement.getAttributeValue("displayName"))
          .setCodeSystemName(xmlElement.getAttributeValue("codeSystemName"))
          .build();
    }
  }

  protected void readID(XMLElement xmlElement) {
    String idRoot = xmlElement.getAttributeValue("root");
    if (idRoot.equalsIgnoreCase(MedCom.DK_REFERENCE_EXTERNAL_DOCUMENT)) {
      documentIdReferencesUse = xmlElement.getAttributeValue("extension");
    } else if (referenceDocumentId == null) {
      referenceDocumentId = buildID(xmlElement, idRoot);
    } else if (referenceObservationId == null) {
      referenceObservationId = buildID(xmlElement, idRoot);
    } else {
      logger.warn("Found unknown ID in a reference element: " + xmlElement.getAttributeValue("extension"));
    }
  }

  protected ID buildID(XMLElement xmlElement, String idRoot) {
    return new IDBuilder()
        .setRoot(idRoot)
        .setExtension(xmlElement.getAttributeValue("extension"))
        .setAuthorityName(xmlElement.getAttributeValue("assigningAuthorityName"))
        .build();
  }

  @Override
  public void handleElementEnd(XmlMapping xmlMapping, XMLElement xmlElement) {
    if (xmlElement.getElementName().equalsIgnoreCase("reference")) {
      if (referenceType == ReferenceType.EXTERNAL_DOCUMENT) {
        if (documentIdReferencesUse != null && !documentIdReferencesUse.trim().isEmpty() && referenceDocumentId != null
            && codeValue != null) {
          references.add(new ReferenceBuilder(documentIdReferencesUse, referenceDocumentId, codeValue).build());
        } else {
          logger.warn("Incomplete external document reference not added to model");
        }
      } else if (referenceType == ReferenceType.EXTERNAL_OBSERVATION) {
        if (documentIdReferencesUse != null && !documentIdReferencesUse.trim().isEmpty() && referenceDocumentId != null
            && codeValue != null && referenceObservationId != null) {
          references.add(new ReferenceBuilder(documentIdReferencesUse, referenceDocumentId, codeValue)
              .externalObservation(referenceObservationId)
              .build());
        } else {
          logger.warn("Incomplete external observation reference not added to model");
        }
      } else {
        logger.warn("Invalid reference in document");
      }
      referenceType = ReferenceType.UNKNOWN;
      referenceDocumentId = null;
      referenceObservationId = null;
      documentIdReferencesUse = null;
      codeValue = null;
    }
  }

  @Override
  public void addHandlerToMap(XmlMapping xmlMapping) {
    xmlMapping.add(REFERENCE, this);
  }

  @Override
  public void removeHandlerFromMap(XmlMapping xmlMapping) {
    xmlMapping.remove(REFERENCE);
  }

  public List<Reference> getReferences() {
    return references;
  }

  @Override
  public boolean includeChildren() {
    return true;
  }
}
