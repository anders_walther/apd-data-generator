package dk.s4.hl7.cda.convert.encode.pattern;

import java.io.IOException;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.qfdd.QFDDCriterion;
import dk.s4.hl7.cda.model.qfdd.QFDDPrecondition;
import dk.s4.hl7.cda.model.qfdd.QFDDPreconditionGroup;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

public class PreconditionPattern {

  public void build(XmlStreamBuilder xmlBuilder, List<QFDDPrecondition> preconditions) throws IOException {
    QFDDPrecondition[] temppreconditions = new QFDDPrecondition[preconditions.size()];
    preconditions.toArray(temppreconditions);
    build(xmlBuilder, temppreconditions);
  }

  public void build(XmlStreamBuilder xmlBuilder, QFDDPrecondition... preconditions) throws IOException {
    final boolean allowSimple = true;
    build(xmlBuilder, allowSimple, preconditions);
  }

  private void build(XmlStreamBuilder xmlBuilder, boolean allowSimple, QFDDPrecondition... preconditions)
      throws IOException {
    if (preconditions != null) {
      for (int i = 0; i < preconditions.length; i++) {
        QFDDPrecondition precondition = preconditions[i];
        if (allowSimple && precondition.isSimple()) {
          buildSimplePreconditionWithCriterion(xmlBuilder, precondition);
        } else if (precondition.getCriterion() != null) {
          buildSDTCPreconditionWithCriterion(xmlBuilder, precondition);
        } else {
          buildSDTCPreconditionGroup(xmlBuilder, precondition);
        }
      }
    }
  }

  private void buildSDTCPreconditionWithCriterion(XmlStreamBuilder xmlBuilder, QFDDPrecondition precondition)
      throws IOException {
    QFDDCriterion criterion = precondition.getCriterion();
    if (criterion.getValueType() != null) {
      xmlBuilder.element("sdtc:precondition").attribute("typeCode", "PRCN");
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_SDTC_PRECONDITION_PATTERN_OID);
      BuildUtil.buildNamedCodedValued(xmlBuilder, "conjunctionCode", precondition.getConjunctionCode());
      xmlBuilder.element("criterion").attribute("classCode", "OBS").attribute("moodCode", "EVN.CRT");
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_CRITERION_PATTERN_OID);
      BuildUtil.buildCode(criterion.getCodeValue(), xmlBuilder);
      // This is a min/max precondition
      if (criterion.getMinimum() != null && criterion.getMinimum().trim().length() > 0) {
        xmlBuilder.element("value").attribute("xsi:type", criterion.getValueType());
        xmlBuilder.element("low").attribute("value", criterion.getMinimum()).elementShortEnd();
        xmlBuilder.element("high").attribute("value", criterion.getMaximum()).elementShortEnd();
        xmlBuilder.elementEnd(); // end value
      } else if (criterion.getAnswer() != null) {
        CodedValue answer = criterion.getAnswer();
        xmlBuilder.element("value").attribute("xsi:type", criterion.getValueType());
        BuildUtil.buildCodedValue(xmlBuilder, answer);
        xmlBuilder.elementShortEnd();
      }
      xmlBuilder.elementEnd(); // end criterion
      if (precondition.isNegationInd()) {
        xmlBuilder.element("negationInd").attribute("value", "true").elementShortEnd();
      }
      xmlBuilder.elementEnd(); // end sdtc:precondition
    }
  }

  private void buildSimplePreconditionWithCriterion(XmlStreamBuilder xmlBuilder, QFDDPrecondition precondition)
      throws IOException {
    QFDDCriterion criterion = precondition.getCriterion();
    if (criterion.getValueType() != null) {
      xmlBuilder.element("precondition").attribute("typeCode", "PRCN");
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_PRECONDITION_PATTERN_OID);
      xmlBuilder.element("criterion").attribute("classCode", "OBS").attribute("moodCode", "EVN.CRT");
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_CRITERION_PATTERN_OID);
      BuildUtil.buildCode(criterion.getCodeValue(), xmlBuilder);
      // This is a min/max precondition
      if (criterion.getMinimum() != null && criterion.getMinimum().trim().length() > 0) {
        xmlBuilder.element("value").attribute("xsi:type", criterion.getValueType());
        xmlBuilder.element("low").attribute("value", criterion.getMinimum()).elementShortEnd();
        xmlBuilder.element("high").attribute("value", criterion.getMaximum()).elementShortEnd();
        xmlBuilder.elementEnd(); // end value
      } else if (criterion.getAnswer() != null) {
        CodedValue answer = criterion.getAnswer();
        xmlBuilder.element("value").attribute("xsi:type", criterion.getValueType());
        BuildUtil.buildCodedValue(xmlBuilder, answer);
        xmlBuilder.elementShortEnd();
      }
      xmlBuilder.elementEnd(); // end criterion
      xmlBuilder.elementEnd(); // end precondition
    }
  }

  private void buildSDTCPreconditionGroup(XmlStreamBuilder xmlBuilder, QFDDPrecondition precondition)
      throws IOException {
    final boolean allowSimple = false;
    QFDDPreconditionGroup group = precondition.getPreconditionGroup();
    xmlBuilder.element("sdtc:precondition").attribute("typeCode", "PRCN");
    BuildUtil.buildTemplateIds(xmlBuilder, HL7.QFDD_SDTC_PRECONDITION_PATTERN_OID);
    BuildUtil.buildNamedCodedValued(xmlBuilder, "conjunctionCode", precondition.getConjunctionCode());
    xmlBuilder.element(group.getGroupType().toXmlElementName());
    BuildUtil.buildTemplateIds(xmlBuilder, group.getGroupType().getTemplateId());
    BuildUtil.buildId(group.getId(), xmlBuilder);
    build(xmlBuilder, allowSimple, group.getPreconditions());
    xmlBuilder.elementEnd(); // End group
    if (precondition.isNegationInd()) {
      xmlBuilder.element("negationInd").attribute("value", "true").elementShortEnd();
    }
    xmlBuilder.elementEnd(); // end sdtc:precondition
  }
}
