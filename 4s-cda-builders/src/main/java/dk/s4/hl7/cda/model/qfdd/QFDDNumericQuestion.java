package dk.s4.hl7.cda.model.qfdd;

import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.model.BuilderException;
import dk.s4.hl7.cda.model.util.ModelUtil;

/**
 * Numeric question where the user should provide an inside a given numeric
 * range.
 * 
 * The range is given by the minimum and maximum values.
 * The type of the answer value is given by intervalType
 */
public class QFDDNumericQuestion extends QFDDQuestion {
  private String minimum; // low
  private String maximum; // high
  private IntervalType intervalType;

  /**
   * "Effective Java" Builder for constructing QFDDNumericQuestion.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QFDDNumericQuestionBuilder
      extends QFDDQuestion.BaseQFDDQuestionBuilder<QFDDNumericQuestion, QFDDNumericQuestionBuilder> {

    public QFDDNumericQuestionBuilder() {
    }

    private String minimum; // head value
    private String maximum; // denominator
    private IntervalType intervalType;

    public QFDDNumericQuestionBuilder setInterval(String minimum, String maximum, IntervalType intervalType) {
      ModelUtil.checkNull(minimum, "Minimum value is mandatory");
      ModelUtil.checkNull(maximum, "Maximum value is mandatory");
      ModelUtil.checkNull(intervalType, "Interval type is mandatory");
      this.maximum = maximum;
      this.minimum = minimum;
      this.intervalType = intervalType;
      replaceCommaWithDots();
      return this;
    }

    private void replaceCommaWithDots() {
      if (intervalType == IntervalType.IVL_REAL) {
        minimum = minimum.replace(',', '.');
        maximum = maximum.replace(',', '.');
      }
    }

    public QFDDNumericQuestion build() {
      if (intervalType == null) {
        throw new BuilderException("The mandatory answer range interval has not been set");
      }
      return new QFDDNumericQuestion(this);
    }

    @Override
    public QFDDNumericQuestionBuilder getThis() {
      return this;
    }
  }

  private QFDDNumericQuestion(QFDDNumericQuestionBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    maximum = builder.maximum;
    intervalType = builder.intervalType;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }

  public IntervalType getIntervalType() {
    return intervalType;
  }

  public String getIntervalTypeAsString() {
    if (intervalType != null) {
      return intervalType.name();
    }
    return null;
  }
}
