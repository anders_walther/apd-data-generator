package dk.s4.hl7.cda.model.qfdd;

import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.convert.decode.CdaBuilderException;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.util.ModelUtil;

public class QFDDCriterion {
  private CodedValue codeValue;
  private String valueType;

  // min and max only set for numeric range
  private final String minimum;
  private final String maximum;

  // CodedValue is set for non numeric range
  private final CodedValue answer;

  private QFDDCriterion(QFDDCriterionBuilder builder) {
    this.codeValue = builder.codeValue;
    this.minimum = builder.minimum;
    this.maximum = builder.maximum;
    this.answer = builder.answer;
    this.valueType = builder.valueType;
  }

  public CodedValue getCodeValue() {
    return codeValue;
  }

  public String getMinimum() {
    return minimum;
  }

  public String getMaximum() {
    return maximum;
  }

  public String getValueType() {
    return valueType;
  }

  public CodedValue getAnswer() {
    return answer;
  }

  public static class QFDDCriterionBuilder {
    private CodedValue codeValue;
    private String minimum;
    private String maximum;
    private CodedValue answer;
    private String valueType;

    public QFDDCriterionBuilder(CodedValue codeValue) {
      this.codeValue = codeValue;
    }

    public QFDDCriterionBuilder setMinimum(String minimum) {
      ModelUtil.checkNull(minimum, "Minimum must have a value");
      this.minimum = minimum;
      return this;
    }

    public QFDDCriterionBuilder setMaximum(String maximum) {
      ModelUtil.checkNull(maximum, "Maximum must have a value");
      this.maximum = maximum;
      return this;
    }

    public QFDDCriterionBuilder setAnswer(CodedValue answer) {
      ModelUtil.checkNull(answer, "Answer code must have a value");
      this.valueType = "CE";
      this.answer = answer;
      return this;
    }

    public QFDDCriterion build() {
      if ((minimum == null || maximum == null) && answer == null) {
        throw new CdaBuilderException("Precondition must have an interval or an answer");
      }
      return new QFDDCriterion(this);
    }

    public QFDDCriterionBuilder setValueType(String valueType) {
      this.valueType = valueType;
      return this;
    }

    public QFDDCriterionBuilder setValueType(IntervalType valueType) {
      this.valueType = valueType.toString();
      return this;
    }
  }
}
