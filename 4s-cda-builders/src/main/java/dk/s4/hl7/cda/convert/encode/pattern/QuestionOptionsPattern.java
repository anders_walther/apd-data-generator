package dk.s4.hl7.cda.convert.encode.pattern;

import java.io.IOException;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

public class QuestionOptionsPattern {

  public void build(int minimum, int maximum, XmlStreamBuilder xmlBuilder) throws IOException {
    if (minimum < 0 && maximum < 0) {
      return;
    }
    xmlBuilder.element("entryRelationship").attribute("typeCode", "SUBJ").attribute("contextConductionInd", "true");
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");

    BuildUtil.buildTemplateIds(xmlBuilder, HL7.QUESTION_OPTIONS_PATTERN_OBSERVATION_OID);
    BuildUtil.buildCode(Loinc.createQuestionOptionsCode(), xmlBuilder);

    xmlBuilder.element("value").attribute("xsi:type", "IVL_INT");
    if (minimum > -1) {
      xmlBuilder.element("low").attribute("value", Integer.toString(minimum)).elementShortEnd();
    }
    if (maximum > -1) {
      xmlBuilder.element("high").attribute("value", Integer.toString(maximum)).elementShortEnd();
    }
    xmlBuilder.elementEnd(); // end value
    xmlBuilder.elementEnd(); // end observation
    xmlBuilder.elementEnd(); // end entryRelationship
  }

}
