package dk.s4.hl7.cda.convert;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import dk.s4.hl7.cda.codes.HL7;
import dk.s4.hl7.cda.codes.IntervalType;
import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.convert.encode.BuildUtil;
import dk.s4.hl7.cda.convert.encode.ClinicalDocumentConverter;
import dk.s4.hl7.cda.convert.encode.pattern.ReferenceRangePattern;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.Section;
import dk.s4.hl7.cda.model.core.ClinicalDocument;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import dk.s4.hl7.cda.model.qrd.QRDAnalogSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDDiscreteSliderResponse;
import dk.s4.hl7.cda.model.qrd.QRDDocument;
import dk.s4.hl7.cda.model.qrd.QRDMultipleChoiceResponse;
import dk.s4.hl7.cda.model.qrd.QRDNumericResponse;
import dk.s4.hl7.cda.model.qrd.QRDResponse;
import dk.s4.hl7.cda.model.qrd.QRDTextResponse;
import dk.s4.hl7.util.xml.XmlPrettyPrinter;
import dk.s4.hl7.util.xml.XmlStreamBuilder;

/**
 * An implementation of the Converter that can build an XML Document object
 * following the Danish QRD standard.
 * 
 * @author Frank Jacobsen Systematic A/S
 */
public class QRDXmlConverter extends ClinicalDocumentConverter<QRDDocument> {
  private static final ReferenceRangePattern responseReferenceRangePattern = new ReferenceRangePattern();

  public QRDXmlConverter(XmlPrettyPrinter xmlPrettyPrinter) {
    super(xmlPrettyPrinter);
  }

  public QRDXmlConverter() {
    this(new XmlPrettyPrinter());
  }

  private void buildQRDSections(List<Section<QRDResponse>> qrdSections, XmlStreamBuilder xmlBuilder)
      throws IOException {

    if (qrdSections == null || qrdSections.isEmpty()) {
      logger.warn("Creating a document which has no sections");
      xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
      xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");
      BuildUtil.buildTemplateIds(xmlBuilder, HL7.QRD_SECTION_ID);

      BuildUtil.buildCode(Loinc.QRD_CODE, Loinc.OID, null, "LOINC", xmlBuilder);

      xmlBuilder.element("title").value("Empty section").elementEnd();
      // Text may be embedded HTML at this point
      xmlBuilder.element("text").valueNoEscaping("Empty section").elementEnd();
      xmlBuilder.elementEnd(); // End section
      xmlBuilder.elementEnd(); // End component
    }
    for (Section<QRDResponse> qrdSection : qrdSections) {
      List<QRDResponse> qrdResponseList = qrdSection.getQuestionnaireEntities();
      if (!qrdResponseList.isEmpty()) {
        xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
        xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");
        BuildUtil.buildTemplateIds(xmlBuilder, HL7.QRD_SECTION_ID);

        BuildUtil.buildCode(Loinc.QRD_CODE, Loinc.OID, null, "LOINC", xmlBuilder);

        xmlBuilder.element("title").value(qrdSection.getTitle()).elementEnd();
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping(qrdSection.getText()).elementEnd();

        xmlBuilder.element("entry").attribute("typeCode", "DRIV").attribute("contextConductionInd", "true").addElement(
            xmlBuilder.element("organizer").attribute("classCode", "BATTERY").attribute("moodCode", "EVN"));

        BuildUtil.buildTemplateIds(xmlBuilder, HL7.QRD_RESPONSE_PATTERN_OID);
        xmlBuilder.element("statusCode").attribute("code", "COMPLETED").elementShortEnd();
        buildResponses(xmlBuilder, qrdResponseList);
        xmlBuilder.elementEnd(); // End organizer
        xmlBuilder.elementEnd(); // End entry

        xmlBuilder.elementEnd(); // End section
        xmlBuilder.elementEnd(); // End component
      } else if (qrdSection.getSectionInformation() != null) {
        xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
        xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");
        BuildUtil.buildTemplateIds(xmlBuilder, HL7.QRD_SECTION_ID);

        xmlBuilder.element("title").value(qrdSection.getTitle()).elementEnd();
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping(qrdSection.getText()).elementEnd();
        if (qrdSection.getLanguage() != null) {
          xmlBuilder.element("languageCode").attribute("code", qrdSection.getLanguage()).elementShortEnd();
        }
        xmlBuilder.elementEnd(); // End section
        xmlBuilder.elementEnd(); // End component
      } else {
        logger.warn("Creating a section which has no responses");
        xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
        xmlBuilder.element("section").attribute("classCode", "DOCSECT").attribute("moodCode", "EVN");
        BuildUtil.buildTemplateIds(xmlBuilder, HL7.QRD_SECTION_ID);

        BuildUtil.buildCode(Loinc.QRD_CODE, Loinc.OID, null, "LOINC", xmlBuilder);

        xmlBuilder.element("title").value("Empty section").elementEnd();
        // Text may be embedded HTML at this point
        xmlBuilder.element("text").valueNoEscaping("Empty section").elementEnd();
        xmlBuilder.elementEnd(); // End section
        xmlBuilder.elementEnd(); // End component
      }
    }
  }

  private void buildResponses(XmlStreamBuilder xmlBuilder, List<QRDResponse> qrdResponseList) throws IOException {
    int sequenceNumber = 0;
    for (QRDResponse qRDResponse : qrdResponseList) {
      buildOneResponse(qRDResponse, xmlBuilder, ++sequenceNumber);
    }
  }

  private static void buildOneResponse(QRDResponse qRDResponse, XmlStreamBuilder xmlBuilder, int sequenceNumber)
      throws IOException {

    xmlBuilder.element("component").attribute("typeCode", "COMP").attribute("contextConductionInd", "true");
    xmlBuilder.element("sequenceNumber").attribute("value", String.valueOf(sequenceNumber)).elementShortEnd();
    xmlBuilder.element("observation").attribute("classCode", "OBS").attribute("moodCode", "EVN");

    if (qRDResponse instanceof QRDNumericResponse) {
      buildNumericResponse(qRDResponse, xmlBuilder);
    } else if (qRDResponse instanceof QRDDiscreteSliderResponse) {
      buildDiscreteSliderResponse(qRDResponse, xmlBuilder);
    } else if (qRDResponse instanceof QRDAnalogSliderResponse) {
      buildAnalogSliderResponse(qRDResponse, xmlBuilder);
    } else if (qRDResponse instanceof QRDTextResponse) {
      buildTextResponse(qRDResponse, xmlBuilder);
    } else if (qRDResponse instanceof QRDMultipleChoiceResponse) {
      buildMultipleChoiceResponse(qRDResponse, xmlBuilder);
    }

    xmlBuilder.elementEnd(); // end observation
    xmlBuilder.elementEnd(); // end component
  }

  private static void buildMultipleChoiceResponse(QRDResponse qRDResponse, XmlStreamBuilder xmlBuilder)
      throws IOException {

    QRDMultipleChoiceResponse qRDMultipleChoiceResponse = (QRDMultipleChoiceResponse) qRDResponse;
    BuildUtil.buildTemplateIds(xmlBuilder, HL7.QRD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID);

    BuildUtil.buildId(qRDResponse.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCode(qRDResponse, xmlBuilder);

    for (CodedValue codeValue : qRDMultipleChoiceResponse.getAnswers()) {
      xmlBuilder
          .element("value")
          .attribute("xsi:type", "CE")
          .attribute("code", codeValue.getCode())
          .attribute("codeSystem", codeValue.getCodeSystem())
          .attribute("displayName", codeValue.getDisplayName())
          .attribute("codeSystemName", codeValue.getCodeSystemName())
          .elementShortEnd();
    }

    questionOptionsPattern.build(qRDMultipleChoiceResponse.getMinimum(), qRDMultipleChoiceResponse.getMaximum(),
        xmlBuilder);
    externalReferencePattern.build(qRDResponse.getReferences(), xmlBuilder);
  }

  private static void buildTextResponse(QRDResponse qRDResponse, XmlStreamBuilder xmlBuilder) throws IOException {
    QRDTextResponse qRDTextResponse = (QRDTextResponse) qRDResponse;
    BuildUtil.buildTemplateIds(xmlBuilder, HL7.QRD_TEXT_QUESTION_PATTERN_OID);

    BuildUtil.buildId(qRDResponse.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCode(qRDResponse, xmlBuilder);

    xmlBuilder.element("value").attribute("xsi:type", "ST").value(qRDTextResponse.getText());
    xmlBuilder.elementEnd(); // end value

    externalReferencePattern.build(qRDResponse.getReferences(), xmlBuilder);
  }

  private static void buildAnalogSliderResponse(QRDResponse qRDResponse, XmlStreamBuilder xmlBuilder)
      throws IOException {
    QRDAnalogSliderResponse qRDAnalogSliderResponse = (QRDAnalogSliderResponse) qRDResponse;
    BuildUtil.buildTemplateIds(xmlBuilder, HL7.QRD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID,
        HL7.QRD_ANALOG_SLIDER_PATTERN_OID);

    BuildUtil.buildId(qRDResponse.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCode(qRDResponse, xmlBuilder);

    xmlBuilder
        .element("value")
        .attribute("xsi:type", qRDAnalogSliderResponse.getUnit())
        .attribute("value", qRDAnalogSliderResponse.getValue())
        .elementShortEnd();

    externalReferencePattern.build(qRDResponse.getReferences(), xmlBuilder);

    xmlBuilder.element("referenceRange").attribute("typeCode", "REFV");
    xmlBuilder.element("observationRange");

    xmlBuilder.element("value").attribute("xsi:type", IntervalType.GLIST_PQ.name()).attribute("denominator",
        qRDAnalogSliderResponse.getMaximum());

    xmlBuilder.element("head").attribute("value", qRDAnalogSliderResponse.getMinimum()).elementShortEnd();
    xmlBuilder.element("increment").attribute("value", qRDAnalogSliderResponse.getIncrement()).elementShortEnd();
    xmlBuilder.elementEnd(); // end value
    xmlBuilder.elementEnd(); // end observationRange
    xmlBuilder.elementEnd(); // end referenceRange
  }

  private static void buildDiscreteSliderResponse(QRDResponse qRDResponse, XmlStreamBuilder xmlBuilder)
      throws IOException {
    QRDDiscreteSliderResponse qRDDiscreteSliderResponse = (QRDDiscreteSliderResponse) qRDResponse;
    BuildUtil.buildTemplateIds(xmlBuilder, HL7.QRD_MULTIPLE_CHOICE_QUESTION_PATTERN_OID,
        HL7.QRD_DISCRETE_SLIDER_PATTERN_OID);
    BuildUtil.buildId(qRDResponse.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCode(qRDResponse, xmlBuilder);

    if (qRDDiscreteSliderResponse.getAnswer() != null) {
      CodedValue answer = qRDDiscreteSliderResponse.getAnswer();
      xmlBuilder
          .element("value")
          .attribute("xsi:type", "CE")
          .attribute("code", answer.getCode())
          .attribute("codeSystem", answer.getCodeSystem())
          .attribute("displayName", answer.getDisplayName())
          .attribute("codeSystemName", answer.getCodeSystemName())
          .elementShortEnd();
    }

    questionOptionsPattern.build(qRDDiscreteSliderResponse.getMinimum(), qRDDiscreteSliderResponse.getMaximum(),
        xmlBuilder);
    externalReferencePattern.build(qRDResponse.getReferences(), xmlBuilder);
  }

  private static void buildNumericResponse(QRDResponse qRDResponse, XmlStreamBuilder xmlBuilder) throws IOException {
    QRDNumericResponse qRDNumericResponse = (QRDNumericResponse) qRDResponse;
    BuildUtil.buildTemplateIds(xmlBuilder, HL7.QRD_NUMERIC_PATTERN_OID);

    BuildUtil.buildId(qRDResponse.getId(), xmlBuilder);
    BuildUtil.buildQuestionnaireEntityCode(qRDResponse, xmlBuilder);

    xmlBuilder
        .element("value")
        .attribute("xsi:type", qRDNumericResponse.getTypeAsString())
        .attribute("value", qRDNumericResponse.getValue())
        .elementShortEnd();

    externalReferencePattern.build(qRDResponse.getReferences(), xmlBuilder);
    responseReferenceRangePattern.build(qRDNumericResponse, xmlBuilder);
  }

  public String convert(QRDDocument source) {
    int nofObservations = source.getInformationRecipients().size() + source.getSections().size();
    for (Section<QRDResponse> section : source.getSections()) {
      nofObservations += section.getQuestionnaireEntities().size();
    }
    StringBuilder builder = new StringBuilder(8000 + (nofObservations * 1500));
    serialize(source, builder);
    return builder.toString();
  }

  @Override
  protected void buildContext(ClinicalDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // CONF-PHMR-15
    xmlBuilder.element("title").value(document.getTitle()).elementEnd();

    // CONF-PHMR-16 / CONF-DK PHMR-17
    xmlBuilder
        .element("effectiveTime")
        .attribute("value", dateTimeformatter.format(document.getEffectiveTime()))
        .elementShortEnd();

    xmlBuilder
        .element("confidentialityCode")
        .attribute("code", "N")
        .attribute("codeSystem", HL7.CONFIDENTIALITY_OID)
        .elementShortEnd();

    // CONF-PHMR-17-20
    xmlBuilder.element("languageCode").attribute("code", document.getLanguageCode()).elementShortEnd();

    if (document.getVersionNumber() != null) {
      xmlBuilder.element("versionNumber").attribute("value", document.getVersionNumber().toString()).elementShortEnd();
    }
  }

  @Override
  protected void buildDocumentationOf(QRDDocument document, XmlStreamBuilder xmlBuilder) throws IOException {
    // Period-of-time that questionnaire response covers 
    xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
    xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");
    xmlBuilder.element("effectiveTime");
    if (document.getServiceStartTime() != null) {
      xmlBuilder
          .element("low")
          .attribute("value", dateTimeformatter.format(document.getServiceStartTime()))
          .elementShortEnd();
    }
    if (document.getServiceStopTime() != null) {
      xmlBuilder
          .element("high")
          .attribute("value", dateTimeformatter.format(document.getServiceStopTime()))
          .elementShortEnd();
    }
    xmlBuilder.elementEnd(); // end effectiveTime
    xmlBuilder.elementEnd(); // end serviceEvent
    xmlBuilder.elementEnd(); // end documentationOf

    buildDocumentationOfQuestionnaireType(document, xmlBuilder);
  }

  private void buildDocumentationOfQuestionnaireType(QRDDocument document, XmlStreamBuilder xmlBuilder)
      throws IOException {
    CodedValue questionnaireType = document.getQuestionnaireType();
    if (questionnaireType != null) { // Type of questionnaire
      xmlBuilder.element("documentationOf").attribute("typeCode", "DOC");
      xmlBuilder.element("serviceEvent").attribute("classCode", "MPROT").attribute("moodCode", "EVN");
      BuildUtil.buildCode(questionnaireType.getCode(), questionnaireType.getCodeSystem(),
          questionnaireType.getDisplayName(), questionnaireType.getCodeSystemName(), xmlBuilder);
      xmlBuilder.elementEnd(); // end serviceEvent
      xmlBuilder.elementEnd(); // end documentationOf
    }
  }

  @Override
  protected void buildLegalAuthenticatorSection(ClinicalDocument document, XmlStreamBuilder xmlBuilder)
      throws IOException {
    // NOT USED
  }

  @Override
  protected void buildBody(QRDDocument clinicalDocument, XmlStreamBuilder xmlBuilder) throws IOException {
    this.buildStructuredBodySectionStart(xmlBuilder);
    this.buildQRDSections(clinicalDocument.getSections(), xmlBuilder);
    this.buildStructuredBodySectionEnd(xmlBuilder);
  }
}
