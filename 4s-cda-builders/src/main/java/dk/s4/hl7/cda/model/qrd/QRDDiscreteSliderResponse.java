package dk.s4.hl7.cda.model.qrd;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import dk.s4.hl7.cda.convert.decode.CdaBuilderException;
import dk.s4.hl7.cda.model.CodedValue;

/**
 * Multiple choice response where the user may only provide at maximum one
 * answer. The answer is optional by the use of minimum which may be 0 or 1.
 * 
 * The answer is given as a coded value
 */
public final class QRDDiscreteSliderResponse extends QRDResponse {
  private static final Logger logger = LoggerFactory.getLogger(QRDDiscreteSliderResponseBuilder.class);
  private CodedValue answer;
  private int minimum;

  /**
   * "Effective Java" Builder for constructing QRDDiscreteSliderResponse.
   *
   * @author Frank Jacobsen, Systematic
   *
   */
  public static class QRDDiscreteSliderResponseBuilder
      extends QRDResponse.BaseQRDResponseBuilder<QRDDiscreteSliderResponse, QRDDiscreteSliderResponseBuilder> {

    private CodedValue answer;
    private int minimum;

    public QRDDiscreteSliderResponseBuilder() {
      this.minimum = -1;
    }

    public QRDDiscreteSliderResponseBuilder setMinimum(int minimum) {
      this.minimum = minimum;
      return this;
    }

    private void validateMinimum() {
      if (minimum != 0 && minimum != 1) {
        logger.warn("The minimum in the Discrete Slider must be 1 or 0 but was: " + minimum + ". Defaulting to 0");
        minimum = 0;
      }
    }

    @Override
    public QRDDiscreteSliderResponse build() {
      validateMinimum();
      if (minimum == 1 && answer == null) {
        throw new CdaBuilderException("An answer is required but no answer is given");
      }
      return new QRDDiscreteSliderResponse(this);
    }

    @Override
    public QRDDiscreteSliderResponseBuilder getThis() {
      return this;
    }

    public QRDDiscreteSliderResponseBuilder setAnswer(String code, String codeSystem, String displayName,
        String codeSystemName) {
      answer = new CodedValue(code, codeSystem, displayName, codeSystemName);
      return this;
    }
  }

  private QRDDiscreteSliderResponse(QRDDiscreteSliderResponseBuilder builder) {
    super(builder);
    minimum = builder.minimum;
    answer = builder.answer;
  }

  public void setAnswer(String code, String codeSystem, String displayName, String codeSystemName) {
    answer = new CodedValue(code, codeSystem, displayName, codeSystemName);
  }

  public CodedValue getAnswer() {
    return answer;
  }

  public int getMinimum() {
    return minimum;
  }

  public int getMaximum() {
    return 1;
  }
}
