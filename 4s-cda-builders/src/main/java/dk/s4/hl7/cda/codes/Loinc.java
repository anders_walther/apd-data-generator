package dk.s4.hl7.cda.codes;

import dk.s4.hl7.cda.model.CodedValue;

//CHECKSTYLE:OFF
public class Loinc {
  public static final String OID = "2.16.840.1.113883.6.1";
  public static final String DISPLAYNAME = "LOINC";

  public static final String PHMR_CODE = "53576-5";
  public static final String PMHR_DISPLAYNAME = "Personal Health Monitoring Report";

  public static final String APD_CODE = "39289-4";
  public static final String APD_DISPLAYNAME = "Dato og tidspunkt for møde mellem patient og sundhedsperson";

  public static final String COMMENT = "48767-8";
  public static final String COMMENT_DISPLAYNAME = "Kommentar til måling";

  public static final String QFD_CODE = "74468-0";
  public static final String QFD_DISPLAYNAME = "Questionnaire Form Definition Document";

  public static final String QRD_CODE = "74465-6";
  public static final String QRD_DISPLAYNAME = "Questionnaire Response Document";

  public static final String FEEDBACK_CODE = "4466-4";
  public static final String FEEDBACK_DISPLAYNAME = "Feedback to user post question response Question";

  public static final String HELPTEXT_CODE = "48767-8";
  public static final String HELPTEXT_DISPLAYNAME = "Annotation Comment";

  public static final String QUESTION_OPTIONS_PATTERN_CODE = "74467-2";

  public static final String SECTION_VITAL_SIGNS_CODE = "8716-3";
  public static final String SECTION_RESULTS_CODE = "30954-2";
  public static final String SECTION_MEDICAL_CODE = "46264-8";
  public static final String SECTION_QRD_CODE = QRD_CODE;

  public static final String SECTION_APD_CODE = "18776-5";
  public static final String SECTION_APD_DISPLAYNAME = "Plan of care note";

  //TODO: These should be static types
  public static final CodedValue createQRDTypeCode() {
    return new CodedValue(QRD_CODE, OID, QRD_DISPLAYNAME, DISPLAYNAME);
  }

  public static final CodedValue createQFDDTypeCode() {
    return new CodedValue(QFD_CODE, OID, QFD_DISPLAYNAME, DISPLAYNAME);
  }

  public static final CodedValue createPHMRTypeCode() {
    return new CodedValue(PHMR_CODE, OID, PMHR_DISPLAYNAME, DISPLAYNAME);
  }

  public static final CodedValue createQuestionOptionsCode() {
    return new CodedValue(QUESTION_OPTIONS_PATTERN_CODE, OID, null, DISPLAYNAME);
  }

  public static final CodedValue COMMENT_CODEDVALUE = new CodedValue(COMMENT, OID, COMMENT_DISPLAYNAME, DISPLAYNAME);
}