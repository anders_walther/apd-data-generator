package dk.s4.hl7.cda.datacreation;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Converts QRD specific data and header information csv files, to new csv file,
 * with all the information.
 * 
 */
public class QrdDataGenerator extends DataGeneratorBase {
  public static void main(String[] args) {
    Logger logger = LoggerFactory.getLogger(PhmrDataGenerator.class);
    QrdDataGenerator qrdDataGenerator = new QrdDataGenerator();
    try {
      qrdDataGenerator.generateQrqDocuments = true;
      qrdDataGenerator.readProperty(args);

      // first timestamp from properties file
      qrdDataGenerator.cronDateTimeGenerator = new CronDateTimeGenerator(qrdDataGenerator.qrdCron);

      qrdDataGenerator.parseCsvFile();
    } catch (IOException | URISyntaxException e) {
      logger.error("Parsing the input csv file faild", e);
    } catch (ParseException e) {
      logger.error("The cron expression for generating QRD test document is invalid: " + qrdDataGenerator.qrdCron, e);
      logger.error("Please correct the cron expression in the config.properties files");
    }
  }

  @Override
  protected void parseCsvFile() throws IOException {
    String outputFileName = generatedQrdDataFilesPath + "QrdDataFileFor";
    final boolean isQrd = true;
    parseCsvFile(qrdFilesPath, outputFileName, isQrd);
  }

  /**
   * The QRD specific data is computed when this method is called.
   * 
   * @param filesDataRecord
   *          , contains the specific QRD data.
   * @param docId
   * @return String that contains the new specific QRD data as csv string.
   * 
   */
  protected List<String> manipulateData(CSVRecord filesDataRecord, String docId, String qrdTid) {
    String qrdDocTitel = filesDataRecord.get(CsvColumnNames.QRD_DOK_TITEL.getColumnName());
    String qrdSkabelon = filesDataRecord.get(CsvColumnNames.QRD_SKABELON.getColumnName());
    String qrdSpoergsmaalId = filesDataRecord.get(CsvColumnNames.QRD_SPOERGSMAAL_ID.getColumnName());
    String qrdType = filesDataRecord.get(CsvColumnNames.QRD_TYPE.getColumnName());
    String qrdSvar = filesDataRecord.get(CsvColumnNames.QRD_SVAR.getColumnName());
    String qrdTitel = filesDataRecord.get(CsvColumnNames.QRD_TITEL.getColumnName());
    String qrdVejledendeText = filesDataRecord.get(CsvColumnNames.QRD_VEJLEDENDE_TEKST.getColumnName());
    String qrdSpoegsmaalText = filesDataRecord.get(CsvColumnNames.QRD_SPOERGSMAAL_TEXT.getColumnName());
    String qrdAntal = filesDataRecord.get(CsvColumnNames.QRD_ANTAL.getColumnName());

    String[] split = qrdSvar.split("#");
    List<String> listSplit = new ArrayList<String>(Arrays.asList(split));

    // the multiple, discrete and text split up in # , and if the qrdAntal
    // is less then the text numbers, then select random on the text
    if (qrdType.equalsIgnoreCase("multiple") || qrdType.equalsIgnoreCase("discrete")
        || qrdType.equalsIgnoreCase("text")) {
      int sizeQrdAntal = Integer.parseInt(qrdAntal);
      for (; sizeQrdAntal < listSplit.size();) {
        int randomValue = (int) ((Math.random() * (listSplit.size() - 0)) + 1);
        listSplit.remove(--randomValue);
      }
      qrdSvar = "";
      for (String svar : listSplit) {
        qrdSvar = qrdSvar + svar + "#";
      }
      qrdSvar = removeLastCharacter(qrdSvar);
    }

    String objId = UUID.randomUUID().toString();

    StringBuilder builder = new StringBuilder(1024);
    builder.append(qrdDocTitel).append(';');
    builder.append(qrdSkabelon).append(';');
    builder.append(qrdSpoergsmaalId).append(';');
    builder.append(qrdType).append(';');
    builder.append(qrdSvar).append(';');
    builder.append(qrdTitel).append(';');
    builder.append(qrdVejledendeText).append(';');
    builder.append(qrdSpoegsmaalText).append(';');
    builder.append(qrdAntal).append(';');
    builder.append(qrdTid).append(';');
    builder.append(docId).append(';');
    builder.append(objId).append(';');
    return Arrays.asList(builder.toString());

  }

  private String removeLastCharacter(String qrdSvar) {
    // removes the last #
    int qrdSvarLength = qrdSvar.length();
    return qrdSvar.substring(0, --qrdSvarLength);
  }
}
