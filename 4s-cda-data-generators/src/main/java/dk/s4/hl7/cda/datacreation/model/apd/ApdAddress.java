package dk.s4.hl7.cda.datacreation.model.apd;

public class ApdAddress extends ApdCsvObject {
  private String streetNames;
  private String postalCode;
  private String city;
  private String country;
  private String addressUse;

  public String getStreetNames() {
    return streetNames;
  }

  public void setStreetNames(String streetNames) {
    this.streetNames = streetNames;
  }

  public String getPostalCode() {
    return postalCode;
  }

  public void setPostalCode(String postalCode) {
    this.postalCode = postalCode;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getAddressUse() {
    return addressUse;
  }

  public void setAddressUse(String addressUse) {
    this.addressUse = addressUse;
  }

  @Override
  public String toCsv() {
    return this.streetNames + ";" + this.postalCode + ";" + this.city + ";" + this.country + ";";
  }

  @Override
  public boolean isObjectEmpty() {
    return isNullOrEmpty(this.streetNames) && isNullOrEmpty(this.postalCode) && isNullOrEmpty(this.city)
        && isNullOrEmpty(this.country) && isNullOrEmpty(this.addressUse);
  }

}
