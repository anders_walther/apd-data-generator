package dk.s4.hl7.cda.datacreation.model.apd;

public class ApdOrganization extends ApdCsvObject {
  private String organizationId;
  private String name;
  private String phoneNumbers;
  private String emails;
  private ApdAddress address;

  public String getOrganizationId() {
    return organizationId;
  }

  public void setOrganizationId(String organizationId) {
    this.organizationId = organizationId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setPhoneNumbers(String phoneNumbers) {
    this.phoneNumbers = phoneNumbers;
  }

  public String getPhoneNumbers() {
    return phoneNumbers;
  }

  public String getEmails() {
    return emails;
  }

  public void setEmails(String emails) {
    this.emails = emails;
  }

  public void setAddress(ApdAddress address) {
    this.address = address;
  }

  public ApdAddress getAddress() {
    return address;
  }

  public String toCsv() {
    return this.organizationId + ";" + this.name + ";" + this.phoneNumbers + ";" + this.emails + ";"
        + this.address.toCsv();
  }

  public boolean isObjectEmpty() {
    return isNullOrEmpty(this.organizationId) && isNullOrEmpty(this.name) && isNullOrEmpty(this.phoneNumbers)
        && isNullOrEmpty(this.emails) && this.address.isObjectEmpty();
  }
}
