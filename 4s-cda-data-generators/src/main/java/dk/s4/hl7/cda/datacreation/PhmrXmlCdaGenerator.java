package dk.s4.hl7.cda.datacreation;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import org.apache.commons.csv.CSVRecord;
import org.joda.time.DateTime;

import dk.s4.hl7.cda.codes.Loinc;
import dk.s4.hl7.cda.codes.MedCom;
import dk.s4.hl7.cda.codes.NPU;
import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.datacreation.model.ReferenceTimeWrapper;
import dk.s4.hl7.cda.model.AddressData;
import dk.s4.hl7.cda.model.AddressData.Use;
import dk.s4.hl7.cda.model.CodedValue;
import dk.s4.hl7.cda.model.CodedValue.CodedValueBuilder;
import dk.s4.hl7.cda.model.DataInputContext;
import dk.s4.hl7.cda.model.ID;
import dk.s4.hl7.cda.model.OrganizationIdentity;
import dk.s4.hl7.cda.model.Participant.ParticipantBuilder;
import dk.s4.hl7.cda.model.Patient;
import dk.s4.hl7.cda.model.PersonIdentity;
import dk.s4.hl7.cda.model.PersonIdentity.PersonBuilder;
import dk.s4.hl7.cda.model.Reference;
import dk.s4.hl7.cda.model.phmr.Measurement;
import dk.s4.hl7.cda.model.phmr.Measurement.Status;
import dk.s4.hl7.cda.model.phmr.MedicalEquipment;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;

/**
 * Convert PHMR object model to xml.
 */
public class PhmrXmlCdaGenerator extends DataCreationBase {
  private static final ReferenceTimeComparator REFERENCE_TIME_COMPARATOR = new ReferenceTimeComparator();
  private PHMRXmlCodec phmrCodec = new PHMRXmlCodec();

  // Contains a list of all QRD references.
  private HashMap<String, List<ReferenceTimeWrapper>> qrdReferences = null;

  public static void main(String[] args) throws Exception {
    PhmrXmlCdaGenerator phmrCdaGenerator = new PhmrXmlCdaGenerator();
    phmrCdaGenerator.readProperty(args);
    phmrCdaGenerator.createReferenceMappingToQRDS();
    phmrCdaGenerator.parseCsvFile();
  }

  private void createReferenceMappingToQRDS() throws IOException {
    qrdReferences = new HashMap<String, List<ReferenceTimeWrapper>>();
    CodedValue codedValue = new CodedValueBuilder()
        .setCode(Loinc.QRD_CODE)
        .setCodeSystem(Loinc.OID)
        .setDisplayName(Loinc.DISPLAYNAME)
        .build();
    QrdXmlCdaGenerator qrdXmlGenerator = new QrdXmlCdaGenerator();
    for (File qrdCsvFile : listFilesFromDirectory(generatedQrdDataFilesPath)) {
      List<CSVRecord> generatedQRdFilesDateRecords = qrdXmlGenerator.getCsvRecords(qrdCsvFile.getAbsolutePath());
      for (int recordIndex = 1; recordIndex < generatedQRdFilesDateRecords.size(); recordIndex++) {
        CSVRecord currentRecord = generatedQRdFilesDateRecords.get(recordIndex);
        ID idDocRef = new ID.IDBuilder()
            .setRoot(MedCom.ROOT_OID)
            .setExtension(currentRecord.get(CsvColumnNames.DOKUMENTATION_ID.getColumnName()))
            .build();
        Reference reference = new Reference.ReferenceBuilder(
            Reference.DocumentIdReferencesUse.XDS_UNIQUE_ID_REFERENCE.getReferencesUse(), idDocRef, codedValue).build();
        putReferenceInMap(currentRecord.get(CsvColumnNames.PATIENT_CPR.getColumnName()),
            currentRecord.get(CsvColumnNames.QRD_TID.getColumnName()), reference);
      }
    }
    sortReferenceLists();
  }

  private void sortReferenceLists() {
    for (List<ReferenceTimeWrapper> list : qrdReferences.values()) {
      Collections.sort(list, REFERENCE_TIME_COMPARATOR);
    }
  }

  @Override
  protected void parseCsvFile() throws IOException {
    File[] phmrCsvFiles = listFilesFromDirectory(generatedPhmrDataFilesPath);
    for (File phmrCsvFile : phmrCsvFiles) {
      List<CSVRecord> generatedPhmrFilesDateRecords = getCsvRecords(phmrCsvFile.getAbsolutePath());
      PHMRDocument pHMRClinicalDocument = null;
      // Create phmr document with header from first row
      if (!generatedPhmrFilesDateRecords.isEmpty()) {
        pHMRClinicalDocument = buildHeader(generatedPhmrFilesDateRecords.get(1));
      }
      // Add measurements
      for (int recordIndex = 1; recordIndex < generatedPhmrFilesDateRecords.size(); recordIndex++) {
        pHMRClinicalDocument = buildPhmrData(generatedPhmrFilesDateRecords.get(recordIndex), pHMRClinicalDocument);
      }
      writeXmlFile(createPhmrFromCDA(pHMRClinicalDocument), phmrCsvFile.getName());
    }
  }

  private PHMRDocument buildHeader(CSVRecord phmrFilesDataRecord) {
    // Organisation
    String organisationAfdelingStednavn = phmrFilesDataRecord
        .get(CsvColumnNames.ORGANISATION_AFDELING_STEDNAVN.getColumnName());
    String organisationAfdelingVejnavn = phmrFilesDataRecord
        .get(CsvColumnNames.ORGANISATION_AFDELING_VEJNAVN.getColumnName());
    String organisationAfdelingPostnummer = phmrFilesDataRecord
        .get(CsvColumnNames.ORGANISATION_AFDELING_POSTNUMMER.getColumnName());
    String organisationAfdelingBy = phmrFilesDataRecord.get(CsvColumnNames.ORGANISATION_AFDELING_BY.getColumnName());
    String organisationAnsvarligFornavn = phmrFilesDataRecord
        .get(CsvColumnNames.ORGANISATION_ANSVARLIG_FORNAVN.getColumnName());
    String organisationAnsvarligEfternavn = phmrFilesDataRecord
        .get(CsvColumnNames.ORGANISATION_ANSVARLIG_EFTERNAVN.getColumnName());
    String organisationNavn = phmrFilesDataRecord.get(CsvColumnNames.ORGANISATION_NAVN.getColumnName());
    String organisationAfdelingSor = phmrFilesDataRecord.get(CsvColumnNames.ORGANISATION_AFDELING_SOR.getColumnName());
    String organisationSor = phmrFilesDataRecord.get(CsvColumnNames.ORGANISATION_SOR.getColumnName());

    // Legal Authenticator
    String legalAuthenticatorAfdelingStednavn = phmrFilesDataRecord
        .get(CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_STEDNAVN.getColumnName());
    String legalAuthenticatorAfdelingVejnavn = phmrFilesDataRecord
        .get(CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_VEJNAVN.getColumnName());
    String legalAuthenticatorAfdelingPostnummer = phmrFilesDataRecord
        .get(CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_POSTNUMMER.getColumnName());
    String legalAuthenticatorAfdelingBy = phmrFilesDataRecord
        .get(CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_BY.getColumnName());
    String legalAuthenticatorAnsvarligFornavn = phmrFilesDataRecord
        .get(CsvColumnNames.LEGAL_AUTHENTICATOR_ANSVARLIG_FORNAVN.getColumnName());
    String legalAuthenticatorAnsvarligEfternavn = phmrFilesDataRecord
        .get(CsvColumnNames.LEGAL_AUTHENTICATOR_ANSVARLIG_EFTERNAVN.getColumnName());
    String legalAuthenticatorNavn = phmrFilesDataRecord.get(CsvColumnNames.LEGAL_AUTHENTICATOR_NAVN.getColumnName());
    String legalAuthenticatoAfdelingSor = phmrFilesDataRecord
        .get(CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_SOR.getColumnName());
    String legalAuthenticatoSor = phmrFilesDataRecord.get(CsvColumnNames.LEGAL_AUTHENTICATOR_SOR.getColumnName());

    // Cusodian
    String custodianSor = phmrFilesDataRecord.get(CsvColumnNames.CUSTODIAN_SOR.getColumnName());
    String custodianStednavn = phmrFilesDataRecord.get(CsvColumnNames.CUSTODIAN_STEDNAVN.getColumnName());
    String custodianAfdelingNavn = phmrFilesDataRecord.get(CsvColumnNames.CUSTODIAN_AFDELING_NAVN.getColumnName());
    String custodianVejnavn = phmrFilesDataRecord.get(CsvColumnNames.CUSTODIAN_VEJ_NAVN.getColumnName());
    String custodianPostnummer = phmrFilesDataRecord.get(CsvColumnNames.CUSTODIAN_POSTNUMMER.getColumnName());
    String custodianBy = phmrFilesDataRecord.get(CsvColumnNames.CUSTODIAN_BY.getColumnName());

    Date documentCreationTime = new DateTime().toDate();

    // Patient
    Patient person = definePatientPersonIdentity(phmrFilesDataRecord);
    String patientCpr = phmrFilesDataRecord.get(CsvColumnNames.PATIENT_CPR.getColumnName());

    // only create pHMRClinicalDocument first time.
    String dokumentationId = phmrFilesDataRecord.get("dokumentation_id");
    ID idHeader = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension(dokumentationId)
        .setRoot(MedCom.ROOT_OID)
        .build();
    PHMRDocument pHMRClinicalDocument = new PHMRDocument(idHeader);
    pHMRClinicalDocument.setLanguageCode("da-DK");
    pHMRClinicalDocument.setTitle("Hjemmemonitorering for " + patientCpr);

    // Use the document_id from the csv file.
    String phmrTime = phmrFilesDataRecord.get(CsvColumnNames.PHMR_TIDSPUNKT.getColumnName());
    DateTime phmrTimeDateTime = DateTime.parse(phmrTime);

    pHMRClinicalDocument.setEffectiveTime(phmrTimeDateTime.toDate());

    // 1.2 Populate the document with patient information

    pHMRClinicalDocument.setPatient(person);

    // Populate Organisation
    AddressData organisationAdress = new AddressData.AddressBuilder(organisationAfdelingPostnummer,
        organisationAfdelingBy)
            .addAddressLine(organisationAfdelingStednavn)
            .addAddressLine(organisationAfdelingVejnavn)
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build();

    OrganizationIdentity organization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR(organisationSor)
        .setName(organisationNavn)
        .setAddress(organisationAdress)
        .build();

    pHMRClinicalDocument.setAuthor(new ParticipantBuilder()
        .setSOR(organisationAfdelingSor)
        .setAddress(organization.getAddress())
        .setPersonIdentity(new PersonBuilder()
            .addFamilyName(organisationAnsvarligEfternavn)
            .addGivenName(organisationAnsvarligFornavn)
            .build())
        .setTime(documentCreationTime)
        .setOrganizationIdentity(organization)
        .build());

    AddressData custodianAdress = new AddressData.AddressBuilder(custodianPostnummer, custodianBy)
        .addAddressLine(custodianAfdelingNavn)
        .addAddressLine(custodianVejnavn)
        .setCountry("Danmark")
        .setUse(AddressData.Use.WorkPlace)
        .build();

    OrganizationIdentity custodianOrganization = new OrganizationIdentity.OrganizationBuilder()
        .setSOR(custodianSor)
        .setName(custodianStednavn)
        .setAddress(custodianAdress)
        .addTelecom(Use.WorkPlace, "tel", "65223344")
        .build();

    pHMRClinicalDocument.setCustodian(custodianOrganization);

    // Populate LegalAuthenticator
    AddressData legalorganisationAdress = new AddressData.AddressBuilder(legalAuthenticatorAfdelingPostnummer,
        legalAuthenticatorAfdelingBy)
            .addAddressLine(legalAuthenticatorAfdelingStednavn)
            .addAddressLine(legalAuthenticatorAfdelingVejnavn)
            .setCountry("Danmark")
            .setUse(AddressData.Use.WorkPlace)
            .build();

    OrganizationIdentity legalorganization = new OrganizationIdentity.OrganizationBuilder()
        .setName(legalAuthenticatorNavn)
        .setSOR(legalAuthenticatoSor)
        .build();

    PersonIdentity legalauthor = new PersonIdentity.PersonBuilder(legalAuthenticatorAnsvarligEfternavn)
        .addGivenName(legalAuthenticatorAnsvarligFornavn)
        .build();

    pHMRClinicalDocument.setLegalAuthenticator(new ParticipantBuilder()
        .setAddress(legalorganisationAdress)
        .setSOR(legalAuthenticatoAfdelingSor)
        .setPersonIdentity(legalauthor)
        .setOrganizationIdentity(legalorganization)
        .setTime(documentCreationTime)
        .build());

    DateTime dateTo = phmrTimeDateTime.plusDays(1);
    dateTo = dateTo.minusMillis(1);

    pHMRClinicalDocument.setDocumentationTimeInterval(phmrTimeDateTime.toDate(), dateTo.toDate());

    return pHMRClinicalDocument;
  }

  private PHMRDocument buildPhmrData(CSVRecord phmrFilesDataRecord, PHMRDocument pHMRClinicalDocument)
      throws IOException {
    String patientCpr = phmrFilesDataRecord.get(CsvColumnNames.PATIENT_CPR.getColumnName());
    String shouldBuildReferencer = phmrFilesDataRecord.get(CsvColumnNames.REFERENCER.getColumnName());
    String phmrTime = phmrFilesDataRecord.get(CsvColumnNames.PHMR_TIDSPUNKT.getColumnName());
    String phmrType = phmrFilesDataRecord.get(CsvColumnNames.PHMR_TYPE.getColumnName());
    String phmrVærdi = phmrFilesDataRecord.get(CsvColumnNames.PHMR_VAERDI.getColumnName());
    String medicalDeviceDisplayName = phmrFilesDataRecord
        .get(CsvColumnNames.MEDICAL_DEVICE_DISPLAYNAME.getColumnName());
    String medicalDeviceCode = phmrFilesDataRecord.get(CsvColumnNames.MEDICAL_DEVICE_CODE.getColumnName());
    String manufacturerModelName = phmrFilesDataRecord.get(CsvColumnNames.MANUFACTURER_MODEL_NAME.getColumnName());

    DateTime fromTime = DateTime.parse(phmrTime);

    MedicalEquipment equipment = new MedicalEquipment.MedicalEquipmentBuilder()
        .setMedicalDeviceCode(medicalDeviceCode)
        .setMedicalDeviceDisplayName(medicalDeviceDisplayName)
        .setManufacturerModelName(manufacturerModelName)
        .setSoftwareName("SerialNr: N/I / SW Rev. N/I")
        .build();
    pHMRClinicalDocument.addMedicalEquipment(equipment);

    ID measurementId = new ID.IDBuilder()
        .setAuthorityName(MedCom.ROOT_AUTHORITYNAME)
        .setExtension(UUID.randomUUID().toString())
        .setRoot(MedCom.ROOT_OID)
        .build();

    DataInputContext context = new DataInputContext(DataInputContext.ProvisionMethod.TypedByHealthcareProfessional,
        DataInputContext.PerformerType.HealthcareProfessional);
    if (phmrType.equalsIgnoreCase(NPU.DRY_BODY_WEIGHT_CODE.toString())) {
      context = new DataInputContext(DataInputContext.ProvisionMethod.Electronically,
          DataInputContext.PerformerType.Citizen);
      Measurement weight = NPU.createWeight(phmrVærdi, fromTime.toDate(), context, measurementId);
      addReference(shouldBuildReferencer, patientCpr, weight, pHMRClinicalDocument, fromTime);
      pHMRClinicalDocument.addResult(weight);
    }
    if (phmrType.equalsIgnoreCase(NPU.BLOOD_PRESURE_DIASTOLIC_ARM_CODE.toString())) {
      Measurement diastolic = NPU.createBloodPresureDiastolic(phmrVærdi, fromTime.toDate(), context, measurementId);
      addReference(shouldBuildReferencer, patientCpr, diastolic, pHMRClinicalDocument, fromTime);
      diastolic.setStatus(Status.COMPLETED);
      pHMRClinicalDocument.addVitalSign(diastolic);
    }
    if (phmrType.equalsIgnoreCase(NPU.BLOOD_PRESURE_SYSTOLIC_ARM_CODE.toString())) {
      Measurement systolic = NPU.createBloodPresureSystolic(phmrVærdi, fromTime.toDate(), context, measurementId);
      addReference(shouldBuildReferencer, patientCpr, systolic, pHMRClinicalDocument, fromTime);
      systolic.setStatus(Status.COMPLETED);
      pHMRClinicalDocument.addVitalSign(systolic);
    }
    if (phmrType.equalsIgnoreCase(NPU.PROTEIN_URINE_CODE.toString())) {
      Measurement protein = NPU.createProteinUrine(phmrVærdi, fromTime.toDate(), context, measurementId);
      addReference(shouldBuildReferencer, patientCpr, protein, pHMRClinicalDocument, fromTime);
      protein.setStatus(Status.COMPLETED);
      pHMRClinicalDocument.addVitalSign(protein);
    }
    if (phmrType.equalsIgnoreCase(NPU.SATURATION_CODE.toString())) {
      Measurement saturation = NPU.createSaturation(phmrVærdi, fromTime.toDate(), context, measurementId);
      addReference(shouldBuildReferencer, patientCpr, saturation, pHMRClinicalDocument, fromTime);
      saturation.setStatus(Status.COMPLETED);
      pHMRClinicalDocument.addVitalSign(saturation);
    }

    return pHMRClinicalDocument;

  }

  private void addReference(String referencerBoolean, String patientCpr, Measurement measurement,
      PHMRDocument pHMRClinicalDocument, DateTime phmrTime) throws IOException {
    if (!referencerBoolean.equalsIgnoreCase("JA")) {
      return;
    }
    Reference reference = findNearestMatchingQRDReference(patientCpr, phmrTime);
    if (reference != null) {
      measurement.addReference(reference);
    }
  }

  private void putReferenceInMap(String cpr, String qrdTid, Reference reference) {
    List<ReferenceTimeWrapper> referenceList = qrdReferences.get(cpr);
    if (referenceList == null) {
      List<ReferenceTimeWrapper> newReferenceList = new ArrayList<ReferenceTimeWrapper>();
      newReferenceList.add(new ReferenceTimeWrapper(DateTime.parse(qrdTid), reference));
      qrdReferences.put(cpr, newReferenceList);
    } else {
      referenceList.add(new ReferenceTimeWrapper(DateTime.parse(qrdTid), reference));
    }
  }

  /**
   * Finds the nearest QRD document for a PHMR document where phmrTime =<
   * qrdTime.
   * 
   * @param patientCpr
   * @param phmrTime
   * @return
   */
  private Reference findNearestMatchingQRDReference(String patientCpr, DateTime phmrTime) {
    List<ReferenceTimeWrapper> referenceList = qrdReferences.get(patientCpr);
    if (referenceList != null) {
      for (int i = 0; i < referenceList.size(); i++) {
        ReferenceTimeWrapper referenceCandidate = referenceList.get(i);
        if (phmrTime.isEqual(referenceCandidate.getTime()) || phmrTime.isBefore(referenceCandidate.getTime())) {
          return referenceCandidate.getReference();
        }
      }
    }
    return null;
  }

  public List<CSVRecord> getCsvRecords(String fileName) throws IOException {

    return getCsvRecords(fileName, "UTF-8", CsvColumnNames.ORGANISATION_AFDELING_SOR.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_STEDNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_VEJNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_POSTNUMMER.getColumnName(),
        CsvColumnNames.ORGANISATION_AFDELING_BY.getColumnName(),
        CsvColumnNames.ORGANISATION_ANSVARLIG_FORNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_ANSVARLIG_EFTERNAVN.getColumnName(),
        CsvColumnNames.ORGANISATION_SOR.getColumnName(), CsvColumnNames.ORGANISATION_NAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_SOR.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_STEDNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_VEJNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_POSTNUMMER.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_AFDELING_BY.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_ANSVARLIG_FORNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_ANSVARLIG_EFTERNAVN.getColumnName(),
        CsvColumnNames.LEGAL_AUTHENTICATOR_SOR.getColumnName(), CsvColumnNames.LEGAL_AUTHENTICATOR_NAVN.getColumnName(),
        CsvColumnNames.CUSTODIAN_SOR.getColumnName(), CsvColumnNames.CUSTODIAN_STEDNAVN.getColumnName(),
        CsvColumnNames.CUSTODIAN_AFDELING_NAVN.getColumnName(), CsvColumnNames.CUSTODIAN_VEJ_NAVN.getColumnName(),
        CsvColumnNames.CUSTODIAN_POSTNUMMER.getColumnName(), CsvColumnNames.CUSTODIAN_BY.getColumnName(),
        CsvColumnNames.PATIENT_CPR.getColumnName(), CsvColumnNames.PATIENT_FORNANVN.getColumnName(),
        CsvColumnNames.PATIENT_EFTERNAVN.getColumnName(), CsvColumnNames.PATIENT_VEJNAVN.getColumnName(),
        CsvColumnNames.PATIENT_POSTNUMMER.getColumnName(), CsvColumnNames.PATIENT_BY.getColumnName(),
        CsvColumnNames.PATIENT_TLF.getColumnName(), CsvColumnNames.PATIENT_MAIL.getColumnName(),
        CsvColumnNames.REFERENCER.getColumnName(), CsvColumnNames.PHMR_TIDSPUNKT.getColumnName(),
        CsvColumnNames.PHMR_TYPE.getColumnName(), CsvColumnNames.PHMR_ENHED.getColumnName(),
        CsvColumnNames.PHMR_VAERDI.getColumnName(), CsvColumnNames.MEDICAL_DEVICE_CODE.getColumnName(),
        CsvColumnNames.MEDICAL_DEVICE_DISPLAYNAME.getColumnName(),
        CsvColumnNames.MANUFACTURER_MODEL_NAME.getColumnName(), CsvColumnNames.PHMR_MAALINGER_PATIENT.getColumnName(),
        CsvColumnNames.DOKUMENTATION_ID.getColumnName(), CsvColumnNames.OBJEKT_ID.getColumnName());
  }

  private void writeXmlFile(String createQRDFromCDA, String fileName) throws IOException {
    String xmlFileName = fileName.replace("csv", "xml");
    String outputFileName = generatedPhmrXmlFilesPath + xmlFileName;
    new File(outputFileName).getParentFile().mkdirs();
    Writer out = new OutputStreamWriter(new FileOutputStream(outputFileName), "UTF-8");
    out.write(createQRDFromCDA);
    out.flush();
    out.close();
  }

  public String createPhmrFromCDA(PHMRDocument cda) {
    return phmrCodec.encode(cda);
  }

  private static class ReferenceTimeComparator implements Comparator<ReferenceTimeWrapper> {
    @Override
    public int compare(ReferenceTimeWrapper o1, ReferenceTimeWrapper o2) {
      return o1.getTime().compareTo(o2.getTime());
    }
  }
}
