package dk.s4.hl7.cda.download;

import java.io.File;
import java.net.URI;
import java.util.Properties;

public class DownloaderProperties {
  private Properties properties;

  public DownloaderProperties(Properties properties) {
    this.properties = properties;
  }

  public Properties getProperties() {
    return properties;
  }

  public File getOutputFolder() {
    String outputText = properties.getProperty("output.folder");
    File outputFolder = new File(outputText);
    checkOutputFolder(outputFolder);
    return outputFolder;
  }

  public URI getRegistryUrl() {
    return getURIProperty("xds.registryUrl");
  }

  public URI getRepositoryUrl() {
    return getURIProperty("xds.repositoryUrl");
  }

  public String getRepositoryId() {
    return getTextProperty("xds.repositoryId");
  }

  public String getHomeCommunityId() {
    return getTextProperty("xds.homeCommunityId");
  }

  public String getEncoding() {
    return getTextProperty("output.encoding");
  }

  private String getTextProperty(String propertyName) {
    String property = properties.getProperty(propertyName);
    isTextUseful(property, propertyName);
    return property;
  }

  private URI getURIProperty(String propertyName) {
    String url = getTextProperty(propertyName);
    return URI.create(url);
  }

  private void isTextUseful(String text, String propertyName) {
    if (text == null) {
      throw new RuntimeException(String.format("The property %s is undefined", propertyName));
    }
    String textTemp = text.trim();
    if (textTemp.isEmpty()) {
      throw new RuntimeException(String.format("The property %s is empty", propertyName));
    }
    if (textTemp.equalsIgnoreCase("null")) {
      throw new RuntimeException(String.format("The property %s contains 'null'", propertyName));
    }
  }

  private void checkOutputFolder(File file) {
    if (!file.exists()) {
      throw new RuntimeException("Output folder does not exist: " + file.getAbsolutePath());
    }
    if (!file.isDirectory()) {
      throw new RuntimeException("Output folder is not a folder: " + file.getAbsolutePath());
    }
    if (!file.canWrite()) {
      throw new RuntimeException("Unable to write to output folder: " + file.getAbsolutePath());
    }
  }
}
