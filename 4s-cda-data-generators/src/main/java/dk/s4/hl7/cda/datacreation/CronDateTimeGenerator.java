package dk.s4.hl7.cda.datacreation;

import java.text.ParseException;
import java.util.Date;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.quartz.CronExpression;

public class CronDateTimeGenerator implements Iterator<DateTime> {
  private CronExpression cron;
  private Date currentDate;
  private boolean dateHasBeenRead;

  public CronDateTimeGenerator(String cronExpression) throws ParseException {
    this.cron = new CronExpression(cronExpression);
    reset();
  }

  @Override
  public boolean hasNext() {
    if (dateHasBeenRead && currentDate != null) {
      currentDate = cron.getNextValidTimeAfter(currentDate);
      dateHasBeenRead = false;
    }
    return currentDate != null;
  }

  @Override
  public DateTime next() {
    dateHasBeenRead = true;
    if (currentDate != null) {
      return new DateTime(currentDate);
    }
    return null;
  }

  public void reset() {
    this.currentDate = new DateTime(1980, 1, 1, 0, 0).toDate();
    this.dateHasBeenRead = true;
  }

  @Override
  public void remove() {
  }
}
