package dk.s4.hl7.cda.download;

public class ArgumentsToQueryMapper {

  public CDAFindDocumentsQueryBuilder map(ArgumentHandler argumentHandler) {
    CDAFindDocumentsQueryBuilder queryBuilder = new CDAFindDocumentsQueryBuilder();
    queryBuilder.setPatientCpr(argumentHandler.getPatientCpr());
    if (argumentHandler.isApproved()) {
      queryBuilder.setApproved();
    }
    if (argumentHandler.isDeprecated()) {
      queryBuilder.setDeprecated();
    }
    if (argumentHandler.isStable()) {
      queryBuilder.setStable();
    }
    if (argumentHandler.isOnDemand()) {
      queryBuilder.setOnDemand();
    }
    queryBuilder.setServiceStartTimeInterval(argumentHandler.getServiceStartTime());
    queryBuilder.setServiceStopTimeInterval(argumentHandler.getServiceStopTime());
    queryBuilder.setCreatenTimeInterval(argumentHandler.getCreationTime());
    queryBuilder.addAuthorPerson(argumentHandler.getAuthor());
    queryBuilder.addClassCodes(argumentHandler.getClassCodes());
    queryBuilder.addConfidentialityCodes(argumentHandler.getConfidentialityCodes());
    queryBuilder.addEventCodes(argumentHandler.getEventCodes());
    queryBuilder.addFormatCodes(argumentHandler.getFormatCodes());
    queryBuilder.addHealthcareFacilityCodes(argumentHandler.getHealthcareFacilityCodes());
    queryBuilder.addPracticeSettingCodes(argumentHandler.getPracticeSettingCodes());
    queryBuilder.addTypeCodes(argumentHandler.getTypeCodes());
    return queryBuilder;
  }
}
