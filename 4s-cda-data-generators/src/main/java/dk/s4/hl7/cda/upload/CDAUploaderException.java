package dk.s4.hl7.cda.upload;

public class CDAUploaderException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public CDAUploaderException(String message) {
    super(message);
  }

  public CDAUploaderException(Exception e) {
    super(e);
  }
}
