package dk.s4.hl7.cda.upload;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.net4care.xdsconnector.IRepositoryConnector;
import org.net4care.xdsconnector.SendRequestException;
import org.net4care.xdsconnector.Utilities.CodedValue;
import org.net4care.xdsconnector.Utilities.PrepareRequestException;
import org.net4care.xdsconnector.Utilities.RetrieveDocumentSetRequestTypeBuilder;
import org.net4care.xdsconnector.service.RegistryResponseType;
import org.net4care.xdsconnector.service.RetrieveDocumentSetResponseType;
import org.w3c.dom.Document;

public class SleepyRepositoryDecoratorTest {
  // Because of spikes when garbage collecting the value cannot be below 20 ms
  private static final int MAX_DIFFERENCE = 20;

  @Test
  public void singleDocumentTest() throws Exception {
    // Arrange
    int nofDocumentsBeforeSleep = 2;
    int callsBeforeSleep = nofDocumentsBeforeSleep;
    int sleepDuration = 100;
    List<String> documents = Arrays.asList("");
    RepositoryConnectorDurationCollector connector = createRepositoryConnectorChain(nofDocumentsBeforeSleep,
        sleepDuration);

    // Act / assert
    callAndAssertOneSleepyRoundTrip(documents, callsBeforeSleep, sleepDuration, connector);
    connector.getDurations().clear();
    // first call is already done. So callsBeforeSleep - 1
    callAndAssertOneSleepyRoundTrip(documents, callsBeforeSleep - 1, sleepDuration, connector);
  }

  @Test
  public void multipleDocumentsTest() throws Exception {
    // Arrange
    int nofDocumentsBeforeSleep = 2;
    int callsBeforeSleep = 1;
    int sleepDuration = 100;
    List<String> documents = Arrays.asList("", ""); // Two documents
    RepositoryConnectorDurationCollector connector = createRepositoryConnectorChain(nofDocumentsBeforeSleep,
        sleepDuration);

    // Act / assert
    callAndAssertOneSleepyRoundTrip(documents, callsBeforeSleep, sleepDuration, connector);
    connector.getDurations().clear();
    // first call is already done. So callsBeforeSleep - 1
    callAndAssertOneSleepyRoundTrip(documents, callsBeforeSleep - 1, sleepDuration, connector);
  }

  private RepositoryConnectorDurationCollector createRepositoryConnectorChain(int nofDocumentsBeforeSleep,
      int sleepDuration) {
    RepositoryConnectorDummy dummyConnector = new RepositoryConnectorDummy();
    IRepositoryConnector sleepyConnector = new SleepyRespositoryDecorator(nofDocumentsBeforeSleep, sleepDuration,
        dummyConnector);
    return new RepositoryConnectorDurationCollector(sleepyConnector);
  }

  private void callAndAssertOneSleepyRoundTrip(List<String> documents, int callsBeforeSleep, int sleepDuration,
      RepositoryConnectorDurationCollector connector) throws PrepareRequestException, SendRequestException {
    for (int i = 0; i < callsBeforeSleep; i++) {
      connector.provideAndRegisterCDADocuments(documents, null, null);
      Assert.assertTrue(isInsideLimits(connector.getDurations().get(i), 0, MAX_DIFFERENCE));
    }
    // Expecting a sleep call
    connector.provideAndRegisterCDADocuments(documents, null, null);
    Assert.assertTrue(isInsideLimits(connector.getDurations().get(connector.getDurations().size() - 1), sleepDuration,
        MAX_DIFFERENCE));
  }

  private boolean isInsideLimits(long duration, long expectedDuration, long maxDifference) {
    long difference = Math.abs(duration - expectedDuration);
    return (maxDifference - difference) >= 0;
  }

  private class RepositoryConnectorDurationCollector implements IRepositoryConnector {
    private IRepositoryConnector nextIRepositoryConnector;
    private List<Long> durations;

    public RepositoryConnectorDurationCollector(IRepositoryConnector nextIRepositoryConnector) {
      this.durations = new ArrayList<Long>();
      this.nextIRepositoryConnector = nextIRepositoryConnector;
    }

    public List<Long> getDurations() {
      return durations;
    }

    @Override
    public RetrieveDocumentSetResponseType retrieveDocumentSet(String docId) throws SendRequestException {
      long start = System.currentTimeMillis();
      RetrieveDocumentSetResponseType response = nextIRepositoryConnector.retrieveDocumentSet(docId);
      durations.add(System.currentTimeMillis() - start);
      return response;
    }

    @Override
    public RetrieveDocumentSetResponseType retrieveDocumentSet(List<String> docId) throws SendRequestException {
      long start = System.currentTimeMillis();
      RetrieveDocumentSetResponseType response = nextIRepositoryConnector.retrieveDocumentSet(docId);
      durations.add(System.currentTimeMillis() - start);
      return response;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocument(Document cda, CodedValue healthcareFacilityType,
        CodedValue practiceSettingsCode) throws PrepareRequestException, SendRequestException {
      long start = System.currentTimeMillis();
      RegistryResponseType response = nextIRepositoryConnector.provideAndRegisterCDADocument(cda,
          healthcareFacilityType, practiceSettingsCode);
      durations.add(System.currentTimeMillis() - start);
      return response;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocument(String cda, CodedValue healthcareFacilityType,
        CodedValue practiceSettingsCode) throws PrepareRequestException, SendRequestException {
      long start = System.currentTimeMillis();
      RegistryResponseType response = nextIRepositoryConnector.provideAndRegisterCDADocument(cda,
          healthcareFacilityType, practiceSettingsCode);
      durations.add(System.currentTimeMillis() - start);
      return response;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocuments(List<String> cdas, CodedValue healthcareFacilityType,
        CodedValue practiceSettingsCode) throws PrepareRequestException, SendRequestException {
      long start = System.currentTimeMillis();
      RegistryResponseType response = nextIRepositoryConnector.provideAndRegisterCDADocuments(cdas,
          healthcareFacilityType, practiceSettingsCode);
      durations.add(System.currentTimeMillis() - start);
      return response;
    }

    @Override
    public RetrieveDocumentSetResponseType retrieveDocumentSet(RetrieveDocumentSetRequestTypeBuilder builder)
        throws SendRequestException {
      // TODO Auto-generated method stub
      return null;
    }
  }

  private class RepositoryConnectorDummy implements IRepositoryConnector {
    @Override
    public RetrieveDocumentSetResponseType retrieveDocumentSet(String docId) {
      return null;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocument(Document cda, CodedValue healthcareFacilityType,
        CodedValue practiceSettingsCode) {
      return null;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocument(String cda, CodedValue healthcareFacilityType,
        CodedValue practiceSettingsCode) {
      return null;
    }

    @Override
    public RegistryResponseType provideAndRegisterCDADocuments(List<String> cdas, CodedValue healthcareFacilityType,
        CodedValue practiceSettingsCode) {
      return createSuccessResponse();
    }

    private RegistryResponseType createSuccessResponse() {
      RegistryResponseType responseType = new RegistryResponseType();
      responseType.setStatus("urn:oasis:names:tc:ebxml-regrep:ResponseStatusType:Success");
      return responseType;
    }

    @Override
    public RetrieveDocumentSetResponseType retrieveDocumentSet(List<String> docId) {
      return null;
    }

    @Override
    public RetrieveDocumentSetResponseType retrieveDocumentSet(RetrieveDocumentSetRequestTypeBuilder builder)
        throws SendRequestException {
      return null;
    }
  }
}
