import dk.s4.hl7.util.PerfTest;

public class PerformanceTest {

  public static void main(String[] args) throws Exception {
    PerfTest test = new PerfTest();
    for (int i = 0; i < 1; i++) {
      test.JAXBTest();
      test.phmrCodec();
      test.writeJAXBTest();
      test.writePhmrCodec();
    }
  }
}
