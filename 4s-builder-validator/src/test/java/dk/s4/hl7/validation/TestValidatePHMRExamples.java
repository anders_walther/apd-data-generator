package dk.s4.hl7.validation;

import org.junit.Ignore;
import org.junit.Test;

import dk.s4.hl7.cda.convert.PHMRXmlCodec;
import dk.s4.hl7.cda.convert.encode.phmr.SetupMedcomExample1;
import dk.s4.hl7.cda.convert.encode.phmr.SetupMedcomExample2;
import dk.s4.hl7.cda.convert.encode.phmr.SetupMedcomExample4;
import dk.s4.hl7.cda.convert.encode.phmr.SetupMedcomKOLExample1;
import dk.s4.hl7.cda.model.phmr.PHMRDocument;
import generated.CdaType;

public class TestValidatePHMRExamples extends BaseTest<PHMRDocument> {

  public TestValidatePHMRExamples() {
    super(new PHMRXmlCodec());
  }

  @Test
  public void testMedComExample1() throws Exception {
    validateDocument(SetupMedcomExample1.defineAsCDA(), CdaType.PHMR);
  }

  @Test
  @Ignore
  public void testMedComExample2() throws Exception {
    validateDocument(SetupMedcomExample2.defineAsCDA(), CdaType.PHMR);
  }

  @Test
  public void testMedComExample4() throws Exception {
    validateDocument(SetupMedcomExample4.defineAsCDA(), CdaType.PHMR);
  }

  @Test
  public void testMedComKOLExample1() throws Exception {
    validateDocument(SetupMedcomKOLExample1.defineAsCDA(), CdaType.PHMR);
  }
}