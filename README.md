# Net4Care CDA Library

This component allows

   - A *GreenCDA* approach for constructing a data object representing
     PHMR, QRD, QFDD and APD documents: *ClinicalDocument*
   - A Builder which can produce a XML document representing a valid
     Danish PHMR, QRD, QFDD or APD (following the MedCom profile) document from the
     *ClinicalDocument* object.

## Usage

### Builders

For code examples please take a look at the following example tests. Also be sure to have a look at the DK CDA profiles at MedCom:

PHMR:

 - [dk.s4.hl7.cda.convert.SetupMedcomKOLExample1](4s-cda-builders/src/test/java/dk/s4/hl7/cda/convert/SetupMedcomKOLExample1.java)
 - http://svn.medcom.dk/svn/drafts/Standarder/HL7/PHMR/

QRD:
 
 - [dk.s4.hl7.cda.model.qrd.SetupQrdKolExample1](4s-cda-builders/src/test/java/dk/s4/hl7/cda/model/qrd/SetupQrdKolExample1.java)
 - http://svn.medcom.dk/svn/drafts/Standarder/HL7/PRO/QRD/

QFDD: 

 - [dk.s4.hl7.cda.model.qfdd.SetupQFDDMultibleChoiseExample](4s-cda-builders/src/test/java/dk/s4/hl7/cda/model/qfdd/SetupQFDDMultibleChoiseExample.java)
 - http://svn.medcom.dk/svn/drafts/Standarder/HL7/PRO/QFDD/
 
APD: 

 - [dk.s4.hl7.cda.convert.encode.apd.SetupMedcomExample](4s-cda-builders/src/test/java/dk/s4/hl7/cda/convert/encode/apd/SetupMedcomExample.java)
 - http://svn.medcom.dk/svn/drafts/Standarder/HL7/Appointment/

### Data generators
This tool is used for generating PHMR and QRD documents based on template csv files as input.
Having the xml files the CDAUploader may be used to upload the documents to an XDS Respository by using the "Provide and Register" request.

Please have a look at the 'User guide for CDA tools.docx' document for thorough explanation and examples of this set of tools.

This package has dependencies to XDS Connector 0.2.0

## Prerequisites
 - Git 
 - Java version 1.6, or newer (data generators require 1.7 or newer)
 - Maven version 3.0, or newer

## Build
 - Clone project from Bitbucket:
   `git clone https://bitbucket.org/4s/4s-cda-builder.git `
 - Compile and run test for CDA builders: `<git-root-folder>/4s-cda-builders/mvn install`
 - Compile and run test for Data generators: `<git-root-folder>/4s-cda-data-generators/mvn install`

## Develop

### Eclipse

- Prepare eclipse project files:  
  `mvn eclipse:eclipse`
- If the M2_REPO variable is not set in eclipse then execute:  
  `mvn -Declipse.workspace=<path to eclipse workspace>; eclipse:configure-workspace`
- Open eclipse and import as existing project, see [eclipse help](http://help.eclipse.org/kepler/index.jsp?topic=/org.eclipse.platform.doc.user/tasks/tasks-importproject.htm)

## Deploy

Only maintainers of the repository should do this, provided here for reference

 - Add artifactory credentials to *$HOME/.m2/settings.xml*
 - Verify that the pom.xml has the proper version number (No SNAPSHOT) and
   that changes.md is updated
 - Execute: `mvn deploy -P {CS|4S}`

Use profile `CS` for [AU CS's Twiga repository](http://twiga.cs.au.dk:8081/artifactory/simple/libs-release-net4care/) 
and `4S` for [4S' repository](http://artifactory.4s-online.dk/artifactory/libs-snapshot-local)